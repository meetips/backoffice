(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/account/account.component.html":
/*!************************************************!*\
  !*** ./src/app/account/account.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"account-container\" class=\"d-block w-100 h-100\">\n  <app-menu (openSideNav)=\"accountSideNav.toggle()\"></app-menu>\n  <mat-drawer-container class=\"w-100\" [hasBackdrop]=\"false\">\n    <mat-drawer [mode]=\"'side'\" #accountSideNav class=\"\" position=\"start\">\n      <mat-list class=\"w-100\">\n        <mat-list-item>\n          <a routerLink=\"dashboard\" class=\"d-block w-100\">dashboard</a>\n        </mat-list-item>\n        <mat-divider></mat-divider>\n        <mat-list-item>\n          <a routerLink=\"password\" class=\"d-block w-100\">password</a>\n        </mat-list-item>\n        <mat-divider></mat-divider>\n        <mat-list-item>\n          <a routerLink=\"profile\" class=\"d-block w-100\">profile</a>\n        </mat-list-item>\n        <mat-divider></mat-divider>\n        <mat-list-item>\n          <a routerLink=\"social-media\" class=\"d-block w-100\">social-media</a>\n        </mat-list-item>\n        <mat-divider></mat-divider>\n        <mat-list-item>\n          <a routerLink=\"payout\" class=\"d-block w-100\">payout</a>\n        </mat-list-item>\n      </mat-list>\n\n      <mat-list>\n        <mat-list-item>\n          <h4>\n            <a class=\"d-block\" routerLink=\"campaign\" (click)=\"selectionCampaign()\">Creer une nouvelles campagne</a>\n          </h4>\n        </mat-list-item>\n        <mat-list-item *ngFor=\"let campaign of myCampaigns\">\n          <a class=\"d-block w-100\" routerLink=\"campaign\" (click)=\"selectionCampaign(campaign)\">{{campaign.name}}</a>\n          <mat-divider></mat-divider>\n        </mat-list-item>\n      </mat-list>\n    </mat-drawer>\n\n    <div id=\"router-container\" class=\"h-100\">\n      <router-outlet></router-outlet>\n    </div>\n\n  </mat-drawer-container>\n  <!--<div class=\"col-4\">\n\n  <mat-card >\n      \n  </mat-card>\n  <mat-accordion>\n      <mat-expansion-panel>\n    <mat-expansion-panel-header>\n    <mat-panel-title>\n      Mes campagnes\n    </mat-panel-title>\n  </mat-expansion-panel-header>\n  <p>\n    <a class=\"d-block\" routerLink=\"campaign\">Creer une nouvelles campagne</a>\n    <a class=\"d-block\" *ngFor=\"let campaign of myCampaigns\" routerLink=\"campaign\">{{campaign.name}}</a>\n  </p>\n</mat-expansion-panel>\n  </mat-accordion>\n</div>\n\n\n\n  <div class=\"col-8\">\n    <router-outlet></router-outlet>\n  </div>\n-->\n\n\n</div>\n"

/***/ }),

/***/ "./src/app/account/account.component.less":
/*!************************************************!*\
  !*** ./src/app/account/account.component.less ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#account-container {\n  background-color: #80DEEA;\n}\nmat-drawer-container {\n  position: absolute;\n  top: 48px;\n  bottom: 0;\n  height: auto;\n}\n"

/***/ }),

/***/ "./src/app/account/account.component.ts":
/*!**********************************************!*\
  !*** ./src/app/account/account.component.ts ***!
  \**********************************************/
/*! exports provided: AccountComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountComponent", function() { return AccountComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var _account_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./account.service */ "./src/app/account/account.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AccountComponent = /** @class */ (function () {
    function AccountComponent(authService, accountService) {
        this.authService = authService;
        this.accountService = accountService;
    }
    AccountComponent.prototype.ngOnInit = function () {
        this.getMyCampaigns();
    };
    AccountComponent.prototype.getMyCampaigns = function () {
        var _this = this;
        this.accountService.getCampaigns().subscribe(function (res) {
            console.log(res);
            _this.myCampaigns = res;
        }, function (err) {
            console.log(err);
        });
    };
    AccountComponent.prototype.selectionCampaign = function (current) {
        if (current === void 0) { current = null; }
        console.log(current);
        console.log('is emmited');
        this.accountService.currentCampaign.emit(current);
    };
    AccountComponent.prototype.contentWidth = function () {
        if (this.accountSideNav.opened) {
            return true;
        }
        return false;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('accountSideNav'),
        __metadata("design:type", Object)
    ], AccountComponent.prototype, "accountSideNav", void 0);
    AccountComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-account',
            template: __webpack_require__(/*! ./account.component.html */ "./src/app/account/account.component.html"),
            styles: [__webpack_require__(/*! ./account.component.less */ "./src/app/account/account.component.less")]
        }),
        __metadata("design:paramtypes", [_auth_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"], _account_service__WEBPACK_IMPORTED_MODULE_2__["AccountService"]])
    ], AccountComponent);
    return AccountComponent;
}());



/***/ }),

/***/ "./src/app/account/account.service.ts":
/*!********************************************!*\
  !*** ./src/app/account/account.service.ts ***!
  \********************************************/
/*! exports provided: AccountService, ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountService", function() { return AccountService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var angular2_cookie_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular2-cookie/core */ "./node_modules/angular2-cookie/core.js");
/* harmony import */ var angular2_cookie_core__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(angular2_cookie_core__WEBPACK_IMPORTED_MODULE_2__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var AccountService = /** @class */ (function () {
    function AccountService(http, cookieService) {
        this.http = http;
        this.cookieService = cookieService;
        this.apiUrl = 'http://localhost:8888';
        this.userEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.currentCampaign = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    AccountService.prototype.createAccount = function (model) {
        return this.http.post(this.apiUrl + '/api/users/', model, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' }),
            responseType: 'json',
        });
        // return this.http.get<any>('http://localhost:8888/api/users/?format=json');
    };
    AccountService.prototype.login = function (model) {
        var user = this.http.post('http://localhost:8888/api/users/login/', model, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' }),
            responseType: 'json',
        });
        this.sendEvent(user);
        return user;
        // return 'Authorization: Token edrokg654fz654gr34';
    };
    AccountService.prototype.sendEvent = function (model) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _b = (_a = this.userEvent).emit;
                        return [4 /*yield*/, model];
                    case 1:
                        _b.apply(_a, [_c.sent()]);
                        return [2 /*return*/];
                }
            });
        });
    };
    AccountService.prototype.updateProfile = function (model) {
        return this.http.put('http://localhost:8888/api/users/' + model.id, model, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' }),
            responseType: 'json',
        });
    };
    AccountService.prototype.getCurrent = function () {
        return this.http.get('http://localhost:8888/api/users/current', {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' }),
            responseType: 'json',
        });
    };
    AccountService.prototype.createCampaign = function (model) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]();
        var options = {
            headers: headers
        };
        // 'Content-Type', 'multipart/form-data'
        return this.http.post('http://localhost:8888/api/campaigns/', model, options);
    };
    AccountService.prototype.getCampaigns = function () {
        return this.http.get(this.apiUrl + '/api/users/campaigns');
    };
    AccountService.prototype.logout = function () {
        localStorage.removeItem('accessToken');
    };
    AccountService.prototype.getCampaign = function (id) {
        return this.http.get(this.apiUrl + '/api/campaigns/' + id);
    };
    AccountService.prototype.updateCampaign = function (model) {
        return this.http.put(this.apiUrl + '/api/campaigns/' + model.get('id') + '/', model);
    };
    AccountService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            angular2_cookie_core__WEBPACK_IMPORTED_MODULE_2__["CookieService"]])
    ], AccountService);
    return AccountService;
}());

var ApiService = /** @class */ (function () {
    function ApiService(http) {
        this.http = http;
    }
    ApiService.prototype.post = function (url, model) {
        this.http.post(url, model, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' }),
            responseType: 'json',
        });
    };
    return ApiService;
}());



/***/ }),

/***/ "./src/app/account/campaign/campaign.component.html":
/*!**********************************************************!*\
  !*** ./src/app/account/campaign/campaign.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"account-campaign-container\">\n  <form>\n\n    <mat-card>\n      <mat-card-title>Votre campagne</mat-card-title>\n      <mat-card-content>\n        <mat-form-field class=\"w-100\" appearance=\"outline\">\n          <mat-label>Donne un nom a ta campagne</mat-label>\n          <input matInput [(ngModel)]=\"model.name\" name=\"name\" />\n        </mat-form-field>\n\n        <mat-form-field class=\"w-100\" appearance=\"outline\">\n            <mat-label>Une courte description pour cet campagne</mat-label>\n          <input matInput [(ngModel)]=\"model.description\" name=\"description\"\n          />\n        </mat-form-field>\n        <mat-form-field class=\"w-100\" appearance=\"outline\">\n            <mat-label>Message de remerciement</mat-label>\n          <input matInput [(ngModel)]=\"model.thanks_message\" name=\"thanks_message\" />\n        </mat-form-field>\n        <div class=\"d-flex image-container\">\n\n        <div class=\"w-50 d-flex\">\n          <div class=\"image-parent w-50\"  *ngIf=\"bannerPreviewLoaded\" >\n            <img [src]=\"bannerPreview\"/>\n          </div>\n          <label class=\"w-50\">\n            Importe ta propre banniere\n            <input class=\"w-100\" type=\"file\" #bannerInput (change)=\"fileEvent($event, 'banner')\" name=\"banner\" accept=\"image/*\" />\n          </label>\n        </div>\n        <div class=\"w-50 d-flex\">\n          <div class=\"image-parent w-50\"  *ngIf=\"thumbnailPreviewLoaded\">\n            <img [src]=\"thumbnailPreview\"/>\n          </div>\n          <label class=\"w-50\">\n            importe un image de profile\n            <input class=\"w-100\" type=\"file\" #thumbnailInput name=\"thumbnail\" (change)=\"fileEvent($event, 'thumbnail')\" accept=\"image/*\"/>\n          </label>\n        </div>\n      </div>\n\n        <mat-form-field class=\"w-100\" appearance=\"outline\">\n            <mat-label>Décris ton objectif avec cette campagne</mat-label>\n          <input matInput [(ngModel)]=\"model.objective\" name=\"objective\" />\n        </mat-form-field>\n        <mat-form-field class=\"w-100\" appearance=\"outline\">\n            <mat-label>Décris ton objectif avec cette campagne</mat-label>\n          <input matInput [(ngModel)]=\"model.objective_amount\" name=\"objective_amount\"/>\n        </mat-form-field>\n        <mat-slide-toggle [(ngModel)]=\"model.is_active\" name=\"is_active\">activer la campagne ?</mat-slide-toggle>\n      </mat-card-content>\n    </mat-card>\n\n    <mat-card class=\"mt-3\">\n      <mat-card-title>\n        parametre visuel lié a ta campagne\n      </mat-card-title>\n      <mat-card-content>\n        <mat-radio-group class=\"example-radio-group w-100\" [(ngModel)]=\"model.show_tip_count\" name=\"show_tip_count\">\n          <mat-radio-button class=\"example-radio-button w-100\" [value]=\"true\">\n            activer l'affichage de l'objectif de la campagne\n          </mat-radio-button>\n          <mat-radio-button class=\"example-radio-button w-100\" [value]=\"false\">\n            desactiver l'affichage de l'objectif de la campagne\n          </mat-radio-button>\n        </mat-radio-group>\n        <mat-radio-group class=\"example-radio-group w-100\" [(ngModel)]=\"model.show_objective_amount\" name=\"show_objective_amount\">\n          <mat-radio-button class=\"example-radio-button w-100\" [value]=\"true\">\n            activer l'affichage du montant de l'objectif de la campagne\n          </mat-radio-button>\n          <mat-radio-button class=\"example-radio-button w-100\" [value]=\"false\">\n            desactiver l'affichage du montant de l'objectif de la campagne\n          </mat-radio-button>\n        </mat-radio-group>\n\n      </mat-card-content>\n    </mat-card>\n\n    <mat-card class=\"mt-3\">\n      <button mat-raised-button class=\"w-100\" color=\"primary\" (click)=\"save()\" *ngIf=\"!isUpdate\">Enregistrer</button>\n      <button mat-raised-button class=\"w-100\" color=\"primary\" (click)=\"update()\" *ngIf=\"isUpdate\">Mettre à jour</button>\n    </mat-card>\n  </form>\n\n</div>\n"

/***/ }),

/***/ "./src/app/account/campaign/campaign.component.less":
/*!**********************************************************!*\
  !*** ./src/app/account/campaign/campaign.component.less ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".image-container .image-parent {\n  max-height: 15vh;\n  max-width: 25vh;\n}\n.image-container .image-parent img {\n  max-height: 15vh;\n  max-width: 25vh;\n}\n"

/***/ }),

/***/ "./src/app/account/campaign/campaign.component.ts":
/*!********************************************************!*\
  !*** ./src/app/account/campaign/campaign.component.ts ***!
  \********************************************************/
/*! exports provided: AccountCampaignComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountCampaignComponent", function() { return AccountCampaignComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _account_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../account.service */ "./src/app/account/account.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! util */ "./node_modules/util/util.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(util__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AccountCampaignComponent = /** @class */ (function () {
    function AccountCampaignComponent(accountService, activatedRoute) {
        this.accountService = accountService;
        this.activatedRoute = activatedRoute;
        this.model = {
            id: null,
            name: null,
            personnal_url: null,
            show_balance: false,
            description: null,
            thanks_message: null,
            objective: null,
            objective_amount: 0,
            show_tip_count: false,
            show_objective_amount: false,
        };
        this.successCreated = null;
        this.saveErrors = {};
        this.bannerPreviewLoaded = false;
        this.thumbnailPreviewLoaded = false;
        this.isUpdate = false;
    }
    AccountCampaignComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.accountService.currentCampaign.subscribe(function (e) {
            if (e === null) {
                _this.isUpdate = false;
                _this.model = {};
                _this.bannerSrc = undefined;
                _this.bannerPreviewLoaded = false;
                _this.bannerPreview = undefined;
                _this.thumbnailSrc = undefined;
                _this.thumbnailPreviewLoaded = false;
                _this.thumbnailPreview = undefined;
                return;
            }
            else {
                _this.isUpdate = true;
            }
            _this.bannerPreviewLoaded = false;
            _this.thumbnailPreviewLoaded = false;
            if (!Object(util__WEBPACK_IMPORTED_MODULE_4__["isNullOrUndefined"])(e.banner)) {
                _this.bannerSrc = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].backendApi + e.banner;
                _this.bannerPreviewLoaded = true;
                _this.bannerPreview = _this.bannerSrc;
            }
            if (!Object(util__WEBPACK_IMPORTED_MODULE_4__["isNullOrUndefined"])(e.thumbnail)) {
                _this.thumbnailSrc = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].backendApi + e.thumbnail;
                _this.thumbnailPreviewLoaded = true;
                _this.thumbnailPreview = _this.thumbnailSrc;
            }
            _this.model = e;
            _this.model = Object.assign({}, e, { banner: undefined, thumbnail: undefined });
        }, function (e) {
            console.log(e);
        });
    };
    AccountCampaignComponent.prototype.save = function () {
        var _this = this;
        var toSend = this.model;
        var formData = this.getFormData(toSend);
        // formData = this.addImages(formData);
        // formData.append('banner', this.bannerFile, this.bannerFile.name);
        this.accountService.createCampaign(formData).subscribe(function (res) {
            console.log(res);
            _this.successCreated = true;
        }, function (err) {
            console.log(err);
            _this.successCreated = false;
            _this.saveErrors = err;
        });
    };
    AccountCampaignComponent.prototype.getFormData = function (toSend) {
        var formData = new FormData();
        for (var _i = 0, _a = Object.keys(toSend); _i < _a.length; _i++) {
            var property = _a[_i];
            console.log(property);
            console.log(toSend[property]);
            var value = toSend[property];
            if (!Object(util__WEBPACK_IMPORTED_MODULE_4__["isNullOrUndefined"])(value)) {
                formData.append(property, value);
            }
        }
        return formData;
    };
    AccountCampaignComponent.prototype.addImages = function (formData) {
        if (!Object(util__WEBPACK_IMPORTED_MODULE_4__["isNullOrUndefined"])(this.bannerFile)) {
            formData.append('banner', this.bannerPreview);
        }
        if (!Object(util__WEBPACK_IMPORTED_MODULE_4__["isNullOrUndefined"])(this.thumbnailFile)) {
            formData.append('thumbnail', this.thumbnailPreview);
        }
        return formData;
    };
    AccountCampaignComponent.prototype.update = function () {
        var _this = this;
        var toSend = this.model;
        var formData = this.getFormData(toSend);
        this.addImages(formData);
        this.accountService.updateCampaign(formData).subscribe(function (res) {
            _this.successCreated = true;
        }, function (err) {
            console.log(err);
            _this.successCreated = false;
            _this.saveErrors = err;
        });
    };
    AccountCampaignComponent.prototype.fileEvent = function (event, typeImage) {
        var _this = this;
        if (event.target.files.length === 0) {
            this[typeImage + 'Preview'] = '';
            this[typeImage + 'PreviewLoaded'] = false;
            return;
        }
        var reader = new FileReader();
        reader.onload = function (e) {
            _this[typeImage + 'Preview'] = e.target.result;
            _this[typeImage + 'PreviewLoaded'] = true;
        };
        reader.readAsDataURL(event.target.files[0]);
        this.model[typeImage] = event.target.files[0];
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('bannerInput'),
        __metadata("design:type", HTMLInputElement)
    ], AccountCampaignComponent.prototype, "bannerInput", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('thumbnailInput'),
        __metadata("design:type", HTMLInputElement)
    ], AccountCampaignComponent.prototype, "thumbnail", void 0);
    AccountCampaignComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-account-campaign',
            template: __webpack_require__(/*! ./campaign.component.html */ "./src/app/account/campaign/campaign.component.html"),
            styles: [__webpack_require__(/*! ./campaign.component.less */ "./src/app/account/campaign/campaign.component.less")]
        }),
        __metadata("design:paramtypes", [_account_service__WEBPACK_IMPORTED_MODULE_1__["AccountService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], AccountCampaignComponent);
    return AccountCampaignComponent;
}());



/***/ }),

/***/ "./src/app/account/dashboard/dashboard.component.css":
/*!***********************************************************!*\
  !*** ./src/app/account/dashboard/dashboard.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#dashboard-container {\n    position: absolute;\n    flex-wrap: wrap;\n}"

/***/ }),

/***/ "./src/app/account/dashboard/dashboard.component.html":
/*!************************************************************!*\
  !*** ./src/app/account/dashboard/dashboard.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"dashboard-container\" class=\"d-flex w-100 p-2\">\n  <div class=\"col-6 p-2\">\n  <mat-card >\n    <mat-card-title>\n      gerer vos differentes campagne ici\n    </mat-card-title>\n    <mat-card-content>\n          <table mat-table [dataSource]=\"transactions\" class=\"mat-elevation-z8\">\n              <!-- Item Column -->\n              <ng-container matColumnDef=\"item\">\n                <th mat-header-cell *matHeaderCellDef> Item </th>\n                <td mat-cell *matCellDef=\"let transaction\"> {{transaction.item}} </td>\n                <td mat-footer-cell *matFooterCellDef> Total </td>\n              </ng-container>\n            \n              <!-- Cost Column -->\n              <ng-container matColumnDef=\"cost\">\n                <th mat-header-cell *matHeaderCellDef> Cost </th>\n                <td mat-cell *matCellDef=\"let transaction\"> {{transaction.cost | currency}} </td>\n                <td mat-footer-cell *matFooterCellDef> {{getTotalCost() | currency}} </td>\n              </ng-container>\n            \n              <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n              <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n          </table>\n    </mat-card-content>\n  </mat-card>\n  \n</div>\n    <div class=\"col-6 p-2\">\n  <mat-card class=\"\">\n      <mat-card-title>\n        gerer mon profile\n      </mat-card-title>\n      <mat-card-content>\n        campaign 1\n      </mat-card-content>\n    </mat-card>\n  </div>\n  \n  <div class=\"col-6 p-2\">\n    <mat-card class=\"\">\n        <mat-card-title>\n          gerer mes reseaux sociaux\n        </mat-card-title>\n        <mat-card-content>\n          campaign 1\n        </mat-card-content>\n      </mat-card>\n\n</div>\n</div>"

/***/ }),

/***/ "./src/app/account/dashboard/dashboard.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/account/dashboard/dashboard.component.ts ***!
  \**********************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DashboardComponent = /** @class */ (function () {
    function DashboardComponent() {
        this.displayedColumns = ['item', 'cost'];
        this.transactions = [
            { item: 'Beach ball', cost: 4 },
            { item: 'Towel', cost: 5 },
            { item: 'Frisbee', cost: 2 },
            { item: 'Sunscreen', cost: 4 },
            { item: 'Cooler', cost: 25 },
            { item: 'Swim suit', cost: 15 },
        ];
    }
    DashboardComponent.prototype.ngOnInit = function () {
    };
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/account/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.css */ "./src/app/account/dashboard/dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/account/password/password.component.css":
/*!*********************************************************!*\
  !*** ./src/app/account/password/password.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/account/password/password.component.html":
/*!**********************************************************!*\
  !*** ./src/app/account/password/password.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"password-container\">\n<mat-card>\n<mat-card-title>\n  votre password\n</mat-card-title>\n    <mat-form-field class=\"w-100\">\n        <input matInput placeholder=\"Mot de passe actuel\" [(ngModel)]='model.current_password' name=\"current_password\">\n      </mat-form-field>\n  <mat-form-field class=\"w-100\">\n    <input matInput placeholder=\"Nouveau mot de passe\"  [(ngModel)]='model.new_password' name=\"new_password\"/>\n  </mat-form-field>\n  <mat-form-field class=\"w-100\">\n    <input matInput placeholder=\"Confirmer votre nouveau mot de passe\"  [(ngModel)]='model.password_confirm' name=\"password_confirm\">\n  </mat-form-field>\n  <button mat-raised-button>Confirmer</button>\n  <button mat-raised-button>Annuler</button>\n</mat-card>\n\n</div>\n"

/***/ }),

/***/ "./src/app/account/password/password.component.ts":
/*!********************************************************!*\
  !*** ./src/app/account/password/password.component.ts ***!
  \********************************************************/
/*! exports provided: PasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PasswordComponent", function() { return PasswordComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PasswordComponent = /** @class */ (function () {
    function PasswordComponent() {
        this.model = {
            currect_password: '',
            new_password: '',
            password_confirm: ''
        };
    }
    PasswordComponent.prototype.ngOnInit = function () {
    };
    PasswordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-password',
            template: __webpack_require__(/*! ./password.component.html */ "./src/app/account/password/password.component.html"),
            styles: [__webpack_require__(/*! ./password.component.css */ "./src/app/account/password/password.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PasswordComponent);
    return PasswordComponent;
}());



/***/ }),

/***/ "./src/app/account/payout/payout.component.css":
/*!*****************************************************!*\
  !*** ./src/app/account/payout/payout.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/account/payout/payout.component.html":
/*!******************************************************!*\
  !*** ./src/app/account/payout/payout.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"payout-container\">\n  <mat-card>\n      <mat-card-title>\n          vos mode de reception de paiements\n        </mat-card-title>\n  <p>\n      <mat-slide-toggle (change)=\"enableChange()\">Modifier votre Profile ?</mat-slide-toggle> \n      Renseigner ici vos information pour recevoir vos paiements\n  </p>\n  <mat-form-field class=\"w-100\">\n    <input matInput placeholder=\"iban\" disabled=\"{{!isEnable}}\" [(ngModel)]=\"model.iban\" name=\"iban\">\n  </mat-form-field>\n  <mat-form-field class=\"w-100\">\n    <input matInput placeholder=\"bic\" disabled=\"{{!isEnable}}\"  [(ngModel)]=\"model.bic\" name=\"bic\">\n  </mat-form-field>\n  <mat-form-field class=\"w-100\">\n    <input matInput placeholder=\"paypal account\" disabled=\"{{!isEnable}}\"  [(ngModel)]=\"model.paypal_account\" name=\"paypal_account\">\n  </mat-form-field>\n  <button mat-raised-button>Confirmer</button>\n  <button mat-raised-button>Annuler</button>\n</mat-card>\n</div>\n"

/***/ }),

/***/ "./src/app/account/payout/payout.component.ts":
/*!****************************************************!*\
  !*** ./src/app/account/payout/payout.component.ts ***!
  \****************************************************/
/*! exports provided: PayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayoutComponent", function() { return PayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PayoutComponent = /** @class */ (function () {
    function PayoutComponent() {
        this.isEnable = false;
        this.model = {
            iban: '',
            bic: '',
            paypal_account: ''
        };
    }
    PayoutComponent.prototype.ngOnInit = function () {
    };
    PayoutComponent.prototype.enableChange = function (event) {
        this.isEnable = !this.isEnable;
    };
    PayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-payout',
            template: __webpack_require__(/*! ./payout.component.html */ "./src/app/account/payout/payout.component.html"),
            styles: [__webpack_require__(/*! ./payout.component.css */ "./src/app/account/payout/payout.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PayoutComponent);
    return PayoutComponent;
}());



/***/ }),

/***/ "./src/app/account/profile/profile.component.css":
/*!*******************************************************!*\
  !*** ./src/app/account/profile/profile.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/account/profile/profile.component.html":
/*!********************************************************!*\
  !*** ./src/app/account/profile/profile.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"profile-container\">\n  <mat-card>\n    <mat-card-title>\n      votre profil\n    </mat-card-title>\n    <mat-card-content>\n      <div class=\"alert alert-warning\" role=\"alert\">\n        Vous ne pouvez pas encore modifier votre profil, veuillez activer votre compte grace au lien envoyé a votre adresse mail.<br>\n        Si vous ne l'avez pas reçu cliquer <a> ici </a>\n      </div>\n\n      <p *ngIf=\"isUserActive()\">\n        <mat-slide-toggle (change)=\"enableChange()\">Modifier votre Profile ?</mat-slide-toggle>\n      </p>\n      <mat-form-field class=\"w-100\">\n        <input class=\"w-100\" matInput placeholder=\"Nom\" [(ngModel)]=\"model.lastname\" name=\"lastname\" disabled=\"{{!isEnable}}\">\n      </mat-form-field>\n      <mat-form-field class=\"w-100\">\n        <input matInput placeholder=\"Prenom\" [(ngModel)]=\"model.firstname\" name=\"firstname\" disabled=\"{{!isEnable}}\">\n      </mat-form-field>\n      <mat-form-field class=\"w-100\">\n        <input matInput placeholder=\"Date de naissance\" [(ngModel)]=\"model.email\" name=\"email\" disabled=\"{{!isEnable}}\">\n      </mat-form-field>\n      <mat-form-field class=\"w-100\">\n        <input matInput placeholder=\"address\" [(ngModel)]=\"model.address\" name=\"address\" disabled=\"{{!isEnable}}\">\n      </mat-form-field>\n      <mat-form-field class=\"w-100\">\n        <input matInput placeholder=\"town\" [(ngModel)]=\"model.town\" name=\"town\" disabled=\"{{!isEnable}}\">\n      </mat-form-field>\n      <mat-form-field class=\"w-100\">\n        <input matInput placeholder=\"birthdate\" [(ngModel)]=\"model.birthdate\" name=\"birthdate\" disabled=\"{{!isEnable}}\">\n      </mat-form-field>\n      <mat-form-field class=\"w-100\">\n        <input matInput placeholder=\"Code postal\" [(ngModel)]=\"model.postal_code\" name=\"postal_code\" disabled=\"{{!isEnable}}\">\n      </mat-form-field>\n      <button mat-raised-button>Enregistrer</button>\n      <button mat-raised-button>Annuler</button>\n    </mat-card-content>\n\n  </mat-card>\n\n</div>\n"

/***/ }),

/***/ "./src/app/account/profile/profile.component.ts":
/*!******************************************************!*\
  !*** ./src/app/account/profile/profile.component.ts ***!
  \******************************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _account_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../account.service */ "./src/app/account/account.service.ts");
/* harmony import */ var angular2_cookie_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular2-cookie/core */ "./node_modules/angular2-cookie/core.js");
/* harmony import */ var angular2_cookie_core__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(angular2_cookie_core__WEBPACK_IMPORTED_MODULE_2__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(accountService, cookieService) {
        this.accountService = accountService;
        this.cookieService = cookieService;
        this.model = {
            id: '',
            firstname: '',
            lastname: '',
            postal_code: '',
            city: '',
            birthdate: '',
            address: '',
            country: '',
        };
        this.isEnable = false;
    }
    ProfileComponent.prototype.ngOnInit = function () {
        this.getCurrent();
    };
    ProfileComponent.prototype.enableChange = function (event) {
        this.isEnable = !this.isEnable;
    };
    ProfileComponent.prototype.isUserActive = function () {
        if (this.model.is_active) {
            return true;
        }
        return false;
    };
    ProfileComponent.prototype.getCurrent = function () {
        var _this = this;
        this.accountService.getCurrent().subscribe(function (res) {
            _this.model = res;
        }, function (e) {
            console.log(e);
        });
    };
    ProfileComponent.prototype.updateProfile = function () {
        this.accountService.updateProfile(this.model).subscribe(function (res) {
            console.log(res);
        }, function (e) {
            console.log(e);
        });
    };
    ProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__(/*! ./profile.component.html */ "./src/app/account/profile/profile.component.html"),
            styles: [__webpack_require__(/*! ./profile.component.css */ "./src/app/account/profile/profile.component.css")]
        }),
        __metadata("design:paramtypes", [_account_service__WEBPACK_IMPORTED_MODULE_1__["AccountService"],
            angular2_cookie_core__WEBPACK_IMPORTED_MODULE_2__["CookieService"]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "./src/app/account/social-media/social-media.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/account/social-media/social-media.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/account/social-media/social-media.component.html":
/*!******************************************************************!*\
  !*** ./src/app/account/social-media/social-media.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"social-media-container\">\n  \n  <mat-card>\n    <mat-card-title>\n      Retrouver ici vos réseau sociaux\n    </mat-card-title>\n    <mat-card-content>\n      <div class=\"row\"> \n        \n            <div class=\"col social-item d-flex\" *ngFor=\"let social of listSocialMedia\" (click)=\"selectSocialMedia(social)\" [ngClass]=\"{'selected': isSelected(social)}\">\n              <img src=\"{{social.url}}\" height=\"35px\"/>\n            \n      </div>\n  \n  \n        <div class=\"row\">\n          <div class=\"col-md-6 social-input-container mt-2\" *ngFor=\"let social of selectedSocialMedia\">\n            <div class=\"row\">\n              <div class=\"col-4 image-container m-auto\">\n                  <img src=\"{{social.url}}\" class=\"w-100 m-auto d-flex\" align=\"middle\"/>\n                \n              </div>\n              <div class=\"col-8\">\n                <mat-form-field class=\"w-100\">\n                  <input matInput placeholder=\"Pseudo\" [(ngModel)]=\"model.social_media[social.name]['pseudo']\" name=\"{{social.name + '_pseudo'}}\">\n                </mat-form-field>\n                <mat-form-field class=\"w-100\">\n                  <input matInput placeholder=\"Url de votre page\" [(ngModel)]=\"model.social_media[social.name]['url']\" name=\"{{social.name + '_url'}}\">\n                </mat-form-field>\n              </div>\n            </div>\n          </div>\n        </div>\n        </div>\n    </mat-card-content>\n  </mat-card>\n\n</div>"

/***/ }),

/***/ "./src/app/account/social-media/social-media.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/account/social-media/social-media.component.ts ***!
  \****************************************************************/
/*! exports provided: SocialMediaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SocialMediaComponent", function() { return SocialMediaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SocialMediaComponent = /** @class */ (function () {
    function SocialMediaComponent() {
        this.listSocialMedia = [
            { name: 'youtube', url: '/assets/statics/youtube3.png' },
            { name: 'facebook', url: '/assets/statics/facebook.png' },
            { name: 'instagram', url: '/assets/statics/instagram.png' },
            { name: 'snapchat', url: '/assets/statics/snapchat.png' },
            { name: 'dailymotion', url: '/assets/statics/dailymotion.png' },
            { name: 'twitch', url: '/assets/statics/twitch.png' },
            { name: 'twitter', url: '/assets/statics/twitter.png' },
            { name: 'tumblr', url: '/assets/statics/tumblr.png' },
        ];
        this.selectedSocialMedia = [];
        this.model = {
            social_media: {},
        };
    }
    SocialMediaComponent.prototype.ngOnInit = function () {
    };
    SocialMediaComponent.prototype.selectSocialMedia = function (socialMedia) {
        var index = this.selectedSocialMedia.indexOf(socialMedia);
        if (index > -1) {
            this.selectedSocialMedia.splice(index, 1);
            delete this.model.socialMedia[socialMedia.name];
        }
        else {
            this.selectedSocialMedia.push(socialMedia);
            this.model.social_media[socialMedia.name] = { pseudo: '', url: '' };
        }
    };
    SocialMediaComponent.prototype.selectIt = function (social_media) {
        this.model.social_media.push(social_media);
    };
    SocialMediaComponent.prototype.isSelected = function (socialMedia) {
        if (this.selectedSocialMedia.includes(socialMedia)) {
            return true;
        }
        return false;
    };
    SocialMediaComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-social-media',
            template: __webpack_require__(/*! ./social-media.component.html */ "./src/app/account/social-media/social-media.component.html"),
            styles: [__webpack_require__(/*! ./social-media.component.css */ "./src/app/account/social-media/social-media.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SocialMediaComponent);
    return SocialMediaComponent;
}());



/***/ }),

/***/ "./src/app/announcer/announcer.component.css":
/*!***************************************************!*\
  !*** ./src/app/announcer/announcer.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/announcer/announcer.component.html":
/*!****************************************************!*\
  !*** ./src/app/announcer/announcer.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"announcer-container\">\n  <mat-card>\n    If you are an announcer and want to diffuse content video here to a attempt public.\n  </mat-card>\n  <form class=\"col-lg-10 offset-lg-1 col-xl-7 m-auto pt-5 pb-5\">\n    <!-- <div class=\"row\"> -->\n\n    <mat-card>\n      <mat-card-title>\n        Créez rapidement votre compte avec ces quelques informations\n      </mat-card-title>\n\n      <mat-card-content>\n\n        <mat-form-field class=\"w-100\">\n          <input class=\"w-100\" matInput placeholder=\"Nom\" [(ngModel)]=\"model.lastname\" name=\"lastname\">\n        </mat-form-field>\n        <mat-form-field class=\"w-100\">\n          <input matInput placeholder=\"Prenom\" [(ngModel)]=\"model.firstname\" name=\"firstname\">\n        </mat-form-field>\n        <mat-form-field class=\"w-100\">\n          <input matInput placeholder=\"Email\" [(ngModel)]=\"model.email\" name=\"email\">\n        </mat-form-field>\n        <mat-form-field class=\"w-100\">\n          <input matInput placeholder=\"Company name\" [(ngModel)]=\"model.companyName\" name=\"companyName\">\n        </mat-form-field>\n        <mat-form-field class=\"w-100\">\n          <input matInput placeholder=\"Mot de passe\" [(ngModel)]=\"model.password\" name=\"password\">\n        </mat-form-field>\n\n        <mat-form-field class=\"w-100\">\n          <input matInput placeholder=\"Confirmer votre mot de passe\" [(ngModel)]=\"model.password_confirm\" name=\"password_confirm\">\n        </mat-form-field>\n      </mat-card-content>\n    </mat-card>\n    </form>\n\n</div>"

/***/ }),

/***/ "./src/app/announcer/announcer.component.ts":
/*!**************************************************!*\
  !*** ./src/app/announcer/announcer.component.ts ***!
  \**************************************************/
/*! exports provided: AnnouncerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AnnouncerComponent", function() { return AnnouncerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AnnouncerComponent = /** @class */ (function () {
    function AnnouncerComponent() {
        this.model = {
            firstname: '',
            lastname: '',
            email: '',
            password: '',
            company_name: '',
            password_confirm: '',
            is_announcer_user: true
        };
    }
    AnnouncerComponent.prototype.ngOnInit = function () {
    };
    AnnouncerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-announcer',
            template: __webpack_require__(/*! ./announcer.component.html */ "./src/app/announcer/announcer.component.html"),
            styles: [__webpack_require__(/*! ./announcer.component.css */ "./src/app/announcer/announcer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AnnouncerComponent);
    return AnnouncerComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_tip_tip_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/tip/tip.component */ "./src/app/tip/tip.component.ts");
/* harmony import */ var src_app_account_account_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/account/account.component */ "./src/app/account/account.component.ts");
/* harmony import */ var src_app_login_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var src_app_home_home_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var src_app_search_search_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/search/search.component */ "./src/app/search/search.component.ts");
/* harmony import */ var src_app_campaign_campaign_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/campaign/campaign.component */ "./src/app/campaign/campaign.component.ts");
/* harmony import */ var src_app_announcer_announcer_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/announcer/announcer.component */ "./src/app/announcer/announcer.component.ts");
/* harmony import */ var src_app_creator_creator_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/creator/creator.component */ "./src/app/creator/creator.component.ts");
/* harmony import */ var _account_profile_profile_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./account/profile/profile.component */ "./src/app/account/profile/profile.component.ts");
/* harmony import */ var _account_password_password_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./account/password/password.component */ "./src/app/account/password/password.component.ts");
/* harmony import */ var _account_payout_payout_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./account/payout/payout.component */ "./src/app/account/payout/payout.component.ts");
/* harmony import */ var _account_social_media_social_media_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./account/social-media/social-media.component */ "./src/app/account/social-media/social-media.component.ts");
/* harmony import */ var _account_campaign_campaign_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./account/campaign/campaign.component */ "./src/app/account/campaign/campaign.component.ts");
/* harmony import */ var _account_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./account/dashboard/dashboard.component */ "./src/app/account/dashboard/dashboard.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: src_app_home_home_component__WEBPACK_IMPORTED_MODULE_5__["HomeComponent"] },
    { path: 'tip', component: src_app_tip_tip_component__WEBPACK_IMPORTED_MODULE_2__["TipComponent"] },
    {
        path: 'account', component: src_app_account_account_component__WEBPACK_IMPORTED_MODULE_3__["AccountComponent"],
        children: [
            { path: '', component: _account_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_15__["DashboardComponent"] },
            { path: 'dashboard', component: _account_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_15__["DashboardComponent"] },
            { path: 'profile', component: _account_profile_profile_component__WEBPACK_IMPORTED_MODULE_10__["ProfileComponent"] },
            { path: 'password', component: _account_password_password_component__WEBPACK_IMPORTED_MODULE_11__["PasswordComponent"] },
            { path: 'payout', component: _account_payout_payout_component__WEBPACK_IMPORTED_MODULE_12__["PayoutComponent"] },
            { path: 'social-media', component: _account_social_media_social_media_component__WEBPACK_IMPORTED_MODULE_13__["SocialMediaComponent"] },
            { path: 'campaign', component: _account_campaign_campaign_component__WEBPACK_IMPORTED_MODULE_14__["AccountCampaignComponent"] },
        ],
    },
    { path: 'login', component: src_app_login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"] },
    { path: 'signup', component: src_app_login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"], },
    { path: 'campaign/:id', component: src_app_campaign_campaign_component__WEBPACK_IMPORTED_MODULE_7__["CampaignComponent"] },
    { path: 'campaign', component: src_app_campaign_campaign_component__WEBPACK_IMPORTED_MODULE_7__["CampaignComponent"] },
    { path: 'search/:searchFilter', component: src_app_search_search_component__WEBPACK_IMPORTED_MODULE_6__["SearchComponent"] },
    { path: 'search', component: src_app_search_search_component__WEBPACK_IMPORTED_MODULE_6__["SearchComponent"] },
    { path: 'announcer', component: src_app_announcer_announcer_component__WEBPACK_IMPORTED_MODULE_8__["AnnouncerComponent"] },
    { path: 'creator', component: src_app_creator_creator_component__WEBPACK_IMPORTED_MODULE_9__["CreatorComponent"] },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "footer {\n    background-image: none;\n    position: absolute;\n    color: white;\n    bottom: 0;\n\n}"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"app-container\">\n\n<router-outlet class=\"h-100\"></router-outlet>\n<footer class=\"w-100\">\n    <div class=\"text-center\">\n        <h4>\n            \n        </h4>\n    </div>\n</footer>\n\n</div>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! .//app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _account_account_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./account/account.component */ "./src/app/account/account.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _tip_tip_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./tip/tip.component */ "./src/app/tip/tip.component.ts");
/* harmony import */ var _menu_menu_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./menu/menu.component */ "./src/app/menu/menu.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/esm5/card.es5.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _search_search_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./search/search.component */ "./src/app/search/search.component.ts");
/* harmony import */ var _campaign_campaign_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./campaign/campaign.component */ "./src/app/campaign/campaign.component.ts");
/* harmony import */ var _creator_creator_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./creator/creator.component */ "./src/app/creator/creator.component.ts");
/* harmony import */ var _announcer_announcer_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./announcer/announcer.component */ "./src/app/announcer/announcer.component.ts");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _account_social_media_social_media_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./account/social-media/social-media.component */ "./src/app/account/social-media/social-media.component.ts");
/* harmony import */ var _account_password_password_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./account/password/password.component */ "./src/app/account/password/password.component.ts");
/* harmony import */ var _account_profile_profile_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./account/profile/profile.component */ "./src/app/account/profile/profile.component.ts");
/* harmony import */ var _account_payout_payout_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./account/payout/payout.component */ "./src/app/account/payout/payout.component.ts");
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/material/expansion */ "./node_modules/@angular/material/esm5/expansion.es5.js");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm5/slide-toggle.es5.js");
/* harmony import */ var _account_campaign_campaign_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./account/campaign/campaign.component */ "./src/app/account/campaign/campaign.component.ts");
/* harmony import */ var _angular_material_radio__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @angular/material/radio */ "./node_modules/@angular/material/esm5/radio.es5.js");
/* harmony import */ var _auth_token_interceptor__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./auth/token.interceptor */ "./src/app/auth/token.interceptor.ts");
/* harmony import */ var _account_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./account/dashboard/dashboard.component */ "./src/app/account/dashboard/dashboard.component.ts");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var angular2_cookie_services_cookies_service__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! angular2-cookie/services/cookies.service */ "./node_modules/angular2-cookie/services/cookies.service.js");
/* harmony import */ var angular2_cookie_services_cookies_service__WEBPACK_IMPORTED_MODULE_32___default = /*#__PURE__*/__webpack_require__.n(angular2_cookie_services_cookies_service__WEBPACK_IMPORTED_MODULE_32__);
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/esm5/list.es5.js");
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! @angular/material/progress-spinner */ "./node_modules/@angular/material/esm5/progress-spinner.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





































// import {MDCIconButtonToggleAdapter} from '@material/icon-button';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _account_account_component__WEBPACK_IMPORTED_MODULE_5__["AccountComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_7__["LoginComponent"],
                _tip_tip_component__WEBPACK_IMPORTED_MODULE_8__["TipComponent"],
                _menu_menu_component__WEBPACK_IMPORTED_MODULE_9__["MenuComponent"],
                _search_search_component__WEBPACK_IMPORTED_MODULE_15__["SearchComponent"],
                _campaign_campaign_component__WEBPACK_IMPORTED_MODULE_16__["CampaignComponent"],
                _creator_creator_component__WEBPACK_IMPORTED_MODULE_17__["CreatorComponent"],
                _announcer_announcer_component__WEBPACK_IMPORTED_MODULE_18__["AnnouncerComponent"],
                _account_social_media_social_media_component__WEBPACK_IMPORTED_MODULE_21__["SocialMediaComponent"],
                _account_password_password_component__WEBPACK_IMPORTED_MODULE_22__["PasswordComponent"],
                _account_profile_profile_component__WEBPACK_IMPORTED_MODULE_23__["ProfileComponent"],
                _account_payout_payout_component__WEBPACK_IMPORTED_MODULE_24__["PayoutComponent"],
                _campaign_campaign_component__WEBPACK_IMPORTED_MODULE_16__["CampaignComponent"],
                _account_campaign_campaign_component__WEBPACK_IMPORTED_MODULE_27__["AccountCampaignComponent"],
                _account_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_30__["DashboardComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatToolbarModule"],
                _angular_material_card__WEBPACK_IMPORTED_MODULE_11__["MatCardModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_12__["MatInputModule"],
                _angular_material_menu__WEBPACK_IMPORTED_MODULE_13__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatButtonModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_14__["FormsModule"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_19__["MatSelectModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_20__["HttpClientModule"],
                _angular_material_expansion__WEBPACK_IMPORTED_MODULE_25__["MatExpansionModule"],
                _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_26__["MatSlideToggleModule"],
                _angular_material_radio__WEBPACK_IMPORTED_MODULE_28__["MatRadioModule"],
                _angular_material_table__WEBPACK_IMPORTED_MODULE_33__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatSidenavModule"],
                _angular_material_list__WEBPACK_IMPORTED_MODULE_34__["MatListModule"],
                _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_35__["MatProgressSpinnerModule"]
                // MDCIconButtonToggleAdapter
            ],
            providers: [
                angular2_cookie_services_cookies_service__WEBPACK_IMPORTED_MODULE_32__["CookieService"],
                _auth_auth_service__WEBPACK_IMPORTED_MODULE_31__["AuthService"],
                {
                    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_20__["HTTP_INTERCEPTORS"],
                    useClass: _auth_token_interceptor__WEBPACK_IMPORTED_MODULE_29__["TokenInterceptor"],
                    multi: true
                }
            ],
            exports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatToolbarModule"],
                _angular_material_card__WEBPACK_IMPORTED_MODULE_11__["MatCardModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_12__["MatInputModule"],
                _angular_material_menu__WEBPACK_IMPORTED_MODULE_13__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatButtonModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_14__["FormsModule"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_19__["MatSelectModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_20__["HttpClientModule"],
                _angular_material_expansion__WEBPACK_IMPORTED_MODULE_25__["MatExpansionModule"],
                _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_26__["MatSlideToggleModule"],
                _angular_material_radio__WEBPACK_IMPORTED_MODULE_28__["MatRadioModule"],
                _angular_material_table__WEBPACK_IMPORTED_MODULE_33__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatSidenavModule"],
                _angular_material_list__WEBPACK_IMPORTED_MODULE_34__["MatListModule"],
                _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_35__["MatProgressSpinnerModule"]
                // MDCIconButtonToggleAdapter
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auth/auth.service.ts":
/*!**************************************!*\
  !*** ./src/app/auth/auth.service.ts ***!
  \**************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angular2_cookie_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angular2-cookie/core */ "./node_modules/angular2-cookie/core.js");
/* harmony import */ var angular2_cookie_core__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(angular2_cookie_core__WEBPACK_IMPORTED_MODULE_1__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthService = /** @class */ (function () {
    function AuthService(_cookieService) {
        this._cookieService = _cookieService;
    }
    AuthService.prototype.getToken = function () {
        // return this._cookieService.get('accessToken');
        return localStorage.getItem('accessToken');
    };
    AuthService.prototype.isAuthenticated = function () {
        // get the token
        var token = this.getToken();
        // return a boolean reflecting
        // whether or not the token is expired
        if (token === undefined || token === null || token === '') {
            return false;
        }
        return true;
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [angular2_cookie_core__WEBPACK_IMPORTED_MODULE_1__["CookieService"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/auth/token.interceptor.ts":
/*!*******************************************!*\
  !*** ./src/app/auth/token.interceptor.ts ***!
  \*******************************************/
/*! exports provided: TokenInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenInterceptor", function() { return TokenInterceptor; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./auth.service */ "./src/app/auth/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TokenInterceptor = /** @class */ (function () {
    function TokenInterceptor(auth) {
        this.auth = auth;
    }
    TokenInterceptor.prototype.intercept = function (request, next) {
        console.log('heetp intercepted');
        if (this.auth.isAuthenticated()) {
            request = request.clone({
                setHeaders: {
                    Authorization: "Token " + this.auth.getToken(),
                },
                responseType: 'json',
            });
        }
        else {
            request = request.clone({
                setHeaders: {},
                responseType: 'json',
            });
        }
        return next.handle(request);
    };
    TokenInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], TokenInterceptor);
    return TokenInterceptor;
}());



/***/ }),

/***/ "./src/app/campaign/campaign.component.html":
/*!**************************************************!*\
  !*** ./src/app/campaign/campaign.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"campaign-container\" >\n  <!-- <div class=\"d-flex\" id=\"banner-container\">\n    <img [src]=\"model.banner\" />\n  </div> -->\n  <app-menu></app-menu>\n  <div id=\"banner-div\" [ngStyle]=\"{'background-image': 'url('+ model.banner +')'}\">\n  </div>\n  <div id=\"back\">\n\n  <div class=\"col-lg-10 col-xl-7 ml-auto mr-auto\" id=\"center-part\">\n    <mat-card class=\"d-flex w-100 flex-grow-1 flex-wrap\" id=\"user-card\">\n      <div id=\"up-part\" class=\"clearfix w-100\">\n\n      <div id=\"thumbnail-container\" class=\"float-left\">\n        <img [src]=\"model.thumbnail\" class=\"m-auto\" />\n      </div>\n      <div id=\"spinner-part\" class=\"m-auto float-right\">\n          <mat-progress-spinner id=\"spinner1\" [mode]=\"'determinate'\" [value]=\"100\" [strokeWidth]=\"25\" [diameter]=\"120\" [color]=\"'warn'\"></mat-progress-spinner>\n          <mat-progress-spinner id=\"spinner2\" [mode]=\"'determinate'\" [value]=\"78\" [strokeWidth]=\"25\" [diameter]=\"120\"></mat-progress-spinner>\n          <!-- <span id=\"progression\">78 %</span> -->\n        </div>\n      </div>\n\n      <div class=\"\" id=\"descripton-container\">\n          <h3>\n            {{model.description}}\n          </h3>\n        </div>\n      <div id=\"user-campaign-info\" class=\"d-flex\">\n\n        <div id=\"stats-part\">\n          <p>{{model.current_amount / model.objective_amount * 100 }}</p>\n          <p>{{model.current_amount}}</p>\n        </div>\n      </div>\n\n    </mat-card>\n\n    <mat-card class=\"mt-2\" id=\"video-card\">\n      <!--Div that will hold the pie chart-->\n\n      <div id=\"chart_div\"></div>\n    </mat-card>\n  </div>\n</div>\n\n\n</div>\n"

/***/ }),

/***/ "./src/app/campaign/campaign.component.less":
/*!**************************************************!*\
  !*** ./src/app/campaign/campaign.component.less ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#campaign-container {\n  background-color: #CFD8DC;\n}\n#campaign-container #banner-div {\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat;\n  height: 280px;\n  overflow: hidden;\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  right: 0;\n  left: 0;\n  flex-wrap: wrap;\n  flex: 1;\n}\n#campaign-container #center-part {\n  margin-top: 250px;\n  max-width: 1100px;\n}\n#campaign-container #user-card {\n  height: 200px;\n  z-index: 0;\n}\n#campaign-container #user-card #descripton-container {\n  width: 85%;\n}\n#campaign-container #user-card #spinner-part {\n  top: -50px;\n  right: 15px;\n  height: 120px;\n  background-color: white;\n  border-radius: 100%;\n  width: auto;\n}\n#campaign-container #user-card #spinner-part #spinner2 {\n  position: relative;\n  top: -120px;\n}\n#campaign-container #user-card #user-campaign-info {\n  position: absolute;\n  top: 0;\n  right: 0;\n}\n#campaign-container #user-card #user-campaign-info #stats-part {\n  position: absolute;\n  bottom: 15px;\n  right: 15px;\n}\n#campaign-container #banner-container {\n  position: absolute;\n  z-index: -1;\n  top: 0;\n  bottom: 0;\n  right: 0;\n  left: 0;\n  flex-wrap: wrap;\n  flex: 1;\n}\n#campaign-container #banner-container img {\n  min-width: 100%;\n  min-height: 100%;\n}\n#campaign-container #thumbnail-container {\n  top: -55px;\n  left: 15px;\n  width: 100px;\n  height: 100px;\n  border-radius: 100%;\n  border: solid 3px #cfa635;\n  overflow: hidden;\n}\n#campaign-container #thumbnail-container img {\n  width: 100%;\n  height: 100%;\n}\n#campaign-container #user-campaign-message {\n  background-color: #fc5151;\n  border: solid 2px black;\n  border-radius: 15px;\n  line-height: 1.5em;\n  margin: auto auto 0;\n  padding: 5px;\n}\n#campaign-container #video-card {\n  height: 500px;\n}\n"

/***/ }),

/***/ "./src/app/campaign/campaign.component.ts":
/*!************************************************!*\
  !*** ./src/app/campaign/campaign.component.ts ***!
  \************************************************/
/*! exports provided: CampaignComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CampaignComponent", function() { return CampaignComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _account_account_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../account/account.service */ "./src/app/account/account.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CampaignComponent = /** @class */ (function () {
    function CampaignComponent(router, activatedRoute, accountService) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.accountService = accountService;
        this.model = {};
    }
    CampaignComponent.prototype.ngOnInit = function () {
        var _this = this;
        // let id = '55b6a9e8-21f9-466b-aa20-b7bf06797dce';
        this.activatedRoute.params.subscribe(function (res) {
            var id = res['id'];
            _this.accountService.getCampaign(id).subscribe(function (campaign) {
                _this.model = campaign;
            }, function (e) {
                console.log(e);
            });
        });
    };
    CampaignComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-campaign',
            template: __webpack_require__(/*! ./campaign.component.html */ "./src/app/campaign/campaign.component.html"),
            styles: [__webpack_require__(/*! ./campaign.component.less */ "./src/app/campaign/campaign.component.less")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _account_account_service__WEBPACK_IMPORTED_MODULE_2__["AccountService"]])
    ], CampaignComponent);
    return CampaignComponent;
}());



/***/ }),

/***/ "./src/app/creator/creator.component.html":
/*!************************************************!*\
  !*** ./src/app/creator/creator.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"creator-container\" class=\"h-100\">\n  <div class=\"h-100\">\n    <form class=\"col-lg-10 offset-lg-1 col-xl-7 m-auto pt-5 pb-5\">\n      <!-- <div class=\"row\"> -->\n\n      <mat-card>\n        <mat-card-title>\n          Créez rapidement votre compte avec ces quelques informations\n        </mat-card-title>\n\n        <mat-card-content>\n\n          <mat-form-field class=\"w-100\">\n            <input class=\"w-100\" matInput placeholder=\"Nom\" [(ngModel)]=\"model.lastname\" name=\"lastname\">\n          </mat-form-field>\n          <mat-form-field class=\"w-100\">\n            <input matInput placeholder=\"Prenom\" [(ngModel)]=\"model.firstname\" name=\"firstname\">\n          </mat-form-field>\n          <mat-form-field class=\"w-100\">\n            <input matInput placeholder=\"Email\" [(ngModel)]=\"model.email\" name=\"email\">\n          </mat-form-field>\n          <mat-form-field class=\"w-100\">\n            <input matInput placeholder=\"Mot de passe\" [(ngModel)]=\"model.password\" name=\"password\">\n          </mat-form-field>\n          <mat-form-field class=\"w-100\">\n            <input matInput placeholder=\"Confirmer votre mot de passe\" [(ngModel)]=\"model.password_confirm\" name=\"password_confirm\">\n          </mat-form-field>\n        </mat-card-content>\n      </mat-card>\n    <!-- </div>\n\n    <div class=\"row\"> -->\n      <mat-card class=\"mt-1 w-100\">\n        <mat-card-content class=\"d-flex\">\n          <div class=\"col social-item d-flex\" *ngFor=\"let social of listSocialMedia\" (click)=\"selectSocialMedia(social)\" [ngClass]=\"{'selected': isSelected(social)}\">\n            <img src=\"{{social.url}}\" height=\"35px\"/>\n          </div>\n        </mat-card-content>\n      </mat-card>\n    <!-- </div> -->\n\n\n      <div class=\"row\">\n        <div class=\"col-md-6 social-input-container mt-2\" *ngFor=\"let social of selectedSocialMedia\">\n            <mat-card>\n          <div class=\"row\">\n            <div class=\"col-4 image-container m-auto\">\n                <img src=\"{{social.url}}\" class=\"w-100 m-auto d-flex\" align=\"middle\"/>\n              \n            </div>\n            <div class=\"col-8\">\n              <mat-form-field class=\"w-100\">\n                <input matInput placeholder=\"Pseudo\" [(ngModel)]=\"model.social_media[social.name]['pseudo']\" name=\"{{social.name + '_pseudo'}}\">\n              </mat-form-field>\n              <mat-form-field class=\"w-100\">\n                <input matInput placeholder=\"Url de votre page\" [(ngModel)]=\"model.social_media[social.name]['url']\" name=\"{{social.name + '_url'}}\">\n              </mat-form-field>\n            </div>\n          </div>\n        </mat-card>\n        </div>\n      </div>\n\n\n\n      <div class=\"row mt-3\">\n        <div class=\"col\">\n          <mat-card>\n            <mat-card-content >\n              <button id=\"create-account-btn\" mat-raised-button color=\"primary\" class=\"w-100\" (click)=\"createAccount()\">Creer mon compte</button>\n            </mat-card-content>\n          </mat-card>\n        </div>\n      </div>\n        \n    </form>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/creator/creator.component.less":
/*!************************************************!*\
  !*** ./src/app/creator/creator.component.less ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#creator-container {\n  height: 90vh;\n  background-color: #CFD8DC;\n}\n#creator-container form .hidden {\n  visibility: hidden;\n  display: none;\n}\n#creator-container form .social-item {\n  background-color: white;\n  padding: 8px;\n}\n#creator-container form .social-item.selected {\n  background: lightgray;\n}\n#creator-container form .social-item img {\n  margin: auto auto 0;\n}\n#creator-container form .social-item:hover {\n  cursor: pointer;\n}\n#creator-container form .image-container img {\n  max-height: 150px;\n  max-width: 150px;\n}\n#creator-container form #create-account-btn {\n  margin: auto;\n}\n"

/***/ }),

/***/ "./src/app/creator/creator.component.ts":
/*!**********************************************!*\
  !*** ./src/app/creator/creator.component.ts ***!
  \**********************************************/
/*! exports provided: CreatorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreatorComponent", function() { return CreatorComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _account_account_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../account/account.service */ "./src/app/account/account.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CreatorComponent = /** @class */ (function () {
    function CreatorComponent(accountService) {
        this.accountService = accountService;
        this.listSocialMedia = [
            { name: 'youtube', url: '/assets/statics/youtube3.png' },
            { name: 'facebook', url: '/assets/statics/facebook.png' },
            { name: 'instagram', url: '/assets/statics/instagram.png' },
            { name: 'snapchat', url: '/assets/statics/snapchat.png' },
            { name: 'dailymotion', url: '/assets/statics/dailymotion.png' },
            { name: 'twitch', url: '/assets/statics/twitch.png' },
            { name: 'twitter', url: '/assets/statics/twitter.png' },
            { name: 'tumblr', url: '/assets/statics/tumblr.png' },
        ];
        this.selectedSocialMedia = [];
        this.model = {
            firstname: '',
            lastname: '',
            email: '',
            password: '',
            password_confirm: '',
            social_media: {},
            is_creator_user: true
        };
    }
    CreatorComponent.prototype.ngOnInit = function () {
    };
    CreatorComponent.prototype.getListSocialMedia = function () {
    };
    CreatorComponent.prototype.selectSocialMedia = function (socialMedia) {
        var index = this.selectedSocialMedia.indexOf(socialMedia);
        if (index > -1) {
            this.selectedSocialMedia.splice(index, 1);
            delete this.model.socialMedia[socialMedia.name];
        }
        else {
            this.selectedSocialMedia.push(socialMedia);
            this.model.social_media[socialMedia.name] = { pseudo: '', url: '' };
        }
    };
    CreatorComponent.prototype.selectIt = function (social_media) {
        this.model.social_media.push(social_media);
    };
    CreatorComponent.prototype.isSelected = function (socialMedia) {
        if (this.selectedSocialMedia.includes(socialMedia)) {
            return true;
        }
        return false;
    };
    CreatorComponent.prototype.createAccount = function () {
        var modelToSend = {
            firstname: this.model.firstname,
            lastname: this.model.lastname,
            email: this.model.email,
            password: this.model.password,
            password_confirm: this.model.password_confirm,
            is_creator_user: true
        };
        if (Object.keys(this.model.social_media).length > 0) {
            modelToSend['social_media'] = this.socialObjectToList(this.model.social_media);
        }
        this.accountService.createAccount(modelToSend).subscribe(function (res) {
            console.log(res);
        }, function (e) {
            console.log('there is an error');
            console.log(e);
        });
    };
    CreatorComponent.prototype.socialObjectToList = function (socialMediaObj) {
        var list = [];
        for (var _i = 0, _a = Object.keys(socialMediaObj); _i < _a.length; _i++) {
            var key = _a[_i];
            list.push({
                name: key,
                url: socialMediaObj[key].url,
                pseudo: socialMediaObj[key].pseudo,
            });
        }
        return list;
    };
    CreatorComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-creator',
            template: __webpack_require__(/*! ./creator.component.html */ "./src/app/creator/creator.component.html"),
            providers: [_account_account_service__WEBPACK_IMPORTED_MODULE_1__["AccountService"]],
            styles: [__webpack_require__(/*! ./creator.component.less */ "./src/app/creator/creator.component.less")]
        }),
        __metadata("design:paramtypes", [_account_account_service__WEBPACK_IMPORTED_MODULE_1__["AccountService"]])
    ], CreatorComponent);
    return CreatorComponent;
}());



/***/ }),

/***/ "./src/app/home/home.component.html":
/*!******************************************!*\
  !*** ./src/app/home/home.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"home-container\">\n  <app-menu></app-menu>\n  <div id=\"background-container\">\n  </div>\n  <div id=\"jumbo\" class=\"m-auto\">\n    <div class=\"\">\n      <h1 class=\"m-auto w-auto\">\n        E-Tip : la plateform de référence pour les videastes et autre createur de contenu.\n      </h1>\n      <br>\n      <h4 class=\"m-auto w-auto\">\n        Inscrit toi dès maintenant et met ta communauté à contribution avec seulement 30 secondes de leur temps, tout simplement.\n      </h4>\n    </div>\n    <!-- <div class=\"row h-100\" id=\"jumbo-inside\">\n      <div class=\"col\">\n\n        <h1 class=\"left-part display-4\">Nos Objectifs</h1>\n      </div>\n      <div class=\"col\">\n        <div class=\"right-part\">\n\n          <p class=\"lead\" style=\"color: #3b5999\">Aider la création sur internet.</p>\n          <p class=\"lead\" style=\"color: #00b489\">Rendre la publicité utile et attrayante.</p>\n          <p class=\"lead\" style=\"color: #e4405f;\">Engager les internautes pour leur creatrice ou créateur préferer.</p>\n        </div>\n      </div>\n    </div> -->\n    <div id=\"register-button-container\">\n      <button mat-raised-button>Je m'inscrit !</button>\n    </div>\n  </div>\n\n  <div id=\"creator-part\" class=\"d-flex flex-column\">\n    <div class=\"row\">\n      <div class=\"col\">\n\n        <p class=\"text-center\">\n          <i>Createur de contenue pourquoi utilisé eTip pour vous remunérer ?</i>\n        </p>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"icon-item col-sm-6 col-xl-3\">\n        <i class=\"material-icons wide-icon\">\n          notifications\n        </i>\n        <p class=\"text-center\">Garder un public attentif qui choisie de visionner une publicité pour vous.</p>\n      </div>\n      <div class=\"icon-item col-sm-6 col-xl-3\">\n        <i class=\"material-icons wide-icon\">\n          link_off\n        </i>\n        <p class=\"text-center\">Garder le contrôle sur vos créations sans sponsors encombrants.</p>\n      </div>\n      <div class=\"icon-item col-sm-6 col-xl-3\">\n        <i class=\"material-icons wide-icon\">\n          all_inclusive\n        </i>\n        <p class=\"text-center\">Creer sans vous soucier de rien.</p>\n      </div>\n      <div class=\"icon-item col-sm-6 col-xl-3\">\n        <i class=\"material-icons wide-icon\">\n          timer\n        </i>\n        <p class=\"text-center\">Creer votre compte en 30s.</p>\n      </div>\n    </div>\n\n  </div>\n\n  <div id=\"announcer-part\" class=\"d-flex flex-column\">\n    <div class=\"row\">\n      <div class=\"col\">\n\n        <p class=\"text-center\">\n          <i>Annonceur pourquoi utilisé eTip pour promouvoir ?</i>\n        </p>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"icon-item col-sm-4\">\n        <i class=\"material-icons wide-icon\">\n          touch_app\n        </i>\n        <p class=\"text-center\">Grâce à une public attentif qui choisie de visionner une publicité.</p>\n      </div>\n      <div class=\"icon-item col-sm-4 \">\n        <i class=\"material-icons wide-icon\">\n          videocam\n        </i>\n        <p class=\"text-center\">Annonce exclusivement par média video qui vous garantie un maximum de visibilité.</p>\n      </div>\n      <div class=\"icon-item col-sm-4 \">\n        <i class=\"material-icons wide-icon\">\n          trending_up\n        </i>\n        <p class=\"text-center\">Atteigné facilement vos objectifs avec nos partenaires Créateurs de contenue.</p>\n      </div>\n    </div>\n  </div>\n\n  <div id=\"community-part\" class=\"d-flex flex-column\">\n    <div class=\"row\">\n      <div class=\"col\">\n        <p class=\"text-center\">\n          <i>Internautes pourquoi passer par eTip ?</i>\n        </p>\n      </div>\n\n    </div>\n    <div class=\"row\">\n      <div class=\"icon-item col-sm-6 col-xl-3\">\n        <i class=\"material-icons wide-icon\">\n          people\n        </i>\n        <p class=\"text-center\">Vous faite partie d'une communauté ? Soutenez votre  preférer.\n        </p>\n      </div>\n      <div class=\"icon-item col-sm-6 col-xl-3\">\n        <i class=\"material-icons wide-icon\">\n          money_off\n        </i>\n        <p class=\"text-center\">Service gratuit et non intrusif qui ne se lance que lorsque vous le voulez.</p>\n      </div>\n      <div class=\"icon-item col-sm-6 col-xl-3\">\n        <i class=\"material-icons wide-icon\">\n          forward_30\n        </i>\n        <p class=\"text-center\">Seulement 30s de votre temps suffit.</p>\n      </div>\n      <div class=\"icon-item col-sm-6 col-xl-3\">\n        <i class=\"fas fa-user-secret wide-icon\"></i>\n        <p class=\"text-center\">Contribuer anonymement, ou créez votre compte pour suivre vos créateur.\n          préferer.\n        </p>\n      </div>\n    </div>\n\n\n  </div>\n"

/***/ }),

/***/ "./src/app/home/home.component.less":
/*!******************************************!*\
  !*** ./src/app/home/home.component.less ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "app-menu {\n  opacity: 0.5;\n  transition-property: opacity;\n  transition-duration: 2000ms;\n  transition-timing-function: cubic-bezier(0.165, 0.84, 0.44, 1);\n}\napp-menu:hover {\n  opacity: 0.8;\n}\n#home-container .wide-icon {\n  font-size: 200px;\n}\n#background-container {\n  background-image: url(\"/assets/statics/little_character.jpg\");\n  background-repeat: no-repeat;\n  background-position: center;\n  background-size: cover;\n  min-height: 90vh;\n}\n#jumbo {\n  height: 90vh;\n  position: absolute;\n  top: 0;\n  left: 15px;\n  bottom: 0;\n  right: 15px;\n  display: flex;\n  /* contexte sur le parent */\n  flex-direction: column;\n  /* direction d'affichage verticale */\n  justify-content: center;\n  /* alig */\n  color: white;\n}\n#jumbo #register-button-container {\n  position: absolute;\n  bottom: 15px;\n  right: 15px;\n  width: 200px;\n}\n#jumbo-inside .col {\n  display: flex;\n  /* contexte sur le parent */\n  flex-direction: column;\n  /* direction d'affichage verticale */\n  justify-content: center;\n  /* alignement vertical */\n}\n#jumbo .left-part {\n  position: absolute;\n  right: 15px;\n  font-weight: 700;\n  font-size: 4.5em;\n}\n#jumbo .right-part p {\n  font-weight: 400;\n  font-size: 1.5em;\n}\n#creator-part,\n#announcer-part,\n#community-part {\n  background-color: #ffab91;\n  padding-top: 25px;\n  padding-bottom: 25px;\n}\n#announcer-part {\n  background-color: #ff8a65;\n}\n#community-part {\n  background-color: #f4511e;\n}\n.icon-item {\n  padding: 15px;\n  display: flex;\n  flex-direction: column;\n}\n.icon-item i {\n  margin: auto auto 0;\n  color: #2c3e50;\n}\n.icon-item p {\n  max-width: 50%;\n  margin: auto auto 0;\n}\n"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomeComponent = /** @class */ (function () {
    function HomeComponent(router) {
        this.router = router;
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent.prototype.search = function () {
        this.router.navigate(['/search/' + this.searchInput]);
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.less */ "./src/app/home/home.component.less")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"login-container\" class=\"\">\n\n<form class=\"m-auto pt-5 pb-5\">\n    <!-- <div class=\"row\"> -->\n\n        <mat-card *ngIf=\"mailSended\">\n            \n          </mat-card>\n\n    <mat-card  *ngIf=\"isRegisterCardShowed\">\n      <mat-card-title>\n        Créez votre compte\n      </mat-card-title>\n\n      <mat-card-content>\n          <div class=\"alert alert-info\">\n              Un mail de confirmation vous à été envoyé. Cliquer sur le lien qu'il contient pour confirmer votre comptee email\n            </div>\n        <mat-form-field class=\"w-100\">\n          <input matInput placeholder=\"Email\" [(ngModel)]=\"model.email\" name=\"email\">\n        </mat-form-field>\n        <mat-form-field class=\"w-100\">\n          <input matInput placeholder=\"Mot de passe\" [(ngModel)]=\"model.password\" name=\"password\">\n        </mat-form-field>\n        <mat-form-field class=\"w-100\">\n          <input matInput placeholder=\"Confirmer votre mot de passe\" [(ngModel)]=\"model.password_confirm\" name=\"password_confirm\">\n        </mat-form-field>\n      <!-- </mat-card-content> -->\n      <!-- <mat-card-actions> -->\n        <button mat-raised-button class=\"w-100 mb-3\" (click)=\"createAccount()\">Creer un compte</button>\n        <a class=\"mt-3\" (click)=\"showLoginCard($event)\">Vous etes déjà inscris ? Connectez-vous</a>\n      <!-- </mat-card-actions> -->\n      </mat-card-content>\n\n    </mat-card>\n\n    <mat-card  *ngIf=\"isLoginCardShowed\">\n        <mat-card-title>\n          Connectez vous\n        </mat-card-title>\n  \n        <mat-card-content>\n          <mat-form-field class=\"w-100\">\n            <input matInput placeholder=\"Email\" [(ngModel)]=\"model.email\" name=\"email\">\n          </mat-form-field>\n          <mat-form-field class=\"w-100\">\n            <input matInput placeholder=\"Mot de passe\" [(ngModel)]=\"model.password\" name=\"password\">\n          </mat-form-field>\n\n          <a (click)=\"showResetCard()\">Mot de passe perdue</a>\n        <!-- </mat-card-content> -->\n        <!-- <mat-card-actions> -->\n          <button mat-raised-button class=\"w-100 mb-3\" (click)=\"login()\">Se connecter</button>\n          <a class=\"mt-2\" (click)=\"showRegisterCard($event)\">Pas encore inscris ?</a>\n        \n      </mat-card-content>\n    <!-- </mat-card-actions> -->\n      </mat-card>\n      <mat-card *ngIf=\"isResetCardShowed\">\n          <mat-card-title>\n              Reset de votre mot de passe\n            </mat-card-title>\n      \n            <mat-card-content></mat-card-content>\n      </mat-card>\n\n</form>\n</div>"

/***/ }),

/***/ "./src/app/login/login.component.less":
/*!********************************************!*\
  !*** ./src/app/login/login.component.less ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#login-container {\n  background-image: url('/assets/statics/stephen-kennedy-552442-unsplash.jpg');\n  background-size: cover;\n  height: 100vh;\n  background-position: center;\n  background-repeat: no-repeat;\n  min-height: 100%;\n  overflow: hidden;\n}\n#login-container form {\n  width: 300px;\n}\n"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angular2_cookie_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angular2-cookie/core */ "./node_modules/angular2-cookie/core.js");
/* harmony import */ var angular2_cookie_core__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(angular2_cookie_core__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _account_account_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../account/account.service */ "./src/app/account/account.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginComponent = /** @class */ (function () {
    function LoginComponent(_cookieService, accountService, router, authService) {
        this._cookieService = _cookieService;
        this.accountService = accountService;
        this.router = router;
        this.authService = authService;
        this.cookieToken = 'accessToken';
        this.model = {
            email: '',
            password: '',
            password_confirm: '',
        };
        this.mailSended = false;
        this.isResetCardShowed = false;
        this.isRegisterCardShowed = false;
        this.isLoginCardShowed = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        if (this.authService.isAuthenticated()) {
            this.router.navigate(['account']);
        }
        if (this.router.url === '/login') {
            this.isLoginCardShowed = true;
        }
        else if (this.router.url === '/register') {
            this.isRegisterCardShowed = true;
        }
        // if (this.route.)
    };
    LoginComponent.prototype.showLoginCard = function (event) {
        console.log('regrzgrez');
        this.isLoginCardShowed = true;
        this.isRegisterCardShowed = false;
        this.isResetCardShowed = false;
        console.log(this);
    };
    LoginComponent.prototype.showRegisterCard = function (event) {
        this.isRegisterCardShowed = true;
        this.isLoginCardShowed = false;
        this.isResetCardShowed = false;
    };
    LoginComponent.prototype.showResetCard = function (event) {
        this.isResetCardShowed = true;
        this.isLoginCardShowed = false;
        this.isRegisterCardShowed = false;
    };
    LoginComponent.prototype.createAccount = function () {
        var _this = this;
        var toSend = __assign({}, this.model, { is_creator_user: true });
        this.accountService.createAccount(toSend).subscribe(function (res) {
            console.log(res);
            _this.mailSended = true;
        }, function (e) {
            console.log(e);
        });
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        var toSend = { email: this.model.email, password: this.model.password };
        this.accountService.login(toSend).subscribe(function (res) {
            console.log(res);
            if (res.hasOwnProperty('token') >= 0) {
                localStorage.setItem('accessToken', res.token);
                // this._cookieService.put('accessToken', res.token);
                _this.router.navigate(['/account', 'dashboard']);
                console.log(res);
            }
        }, function (e) {
            console.log(e);
        });
    };
    LoginComponent.prototype.isLogedIn = function () {
        var token = this._cookieService.get(this.cookieToken);
        if (token !== null && token !== undefined && token !== '') {
            return true;
        }
        return false;
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.less */ "./src/app/login/login.component.less")]
        }),
        __metadata("design:paramtypes", [angular2_cookie_core__WEBPACK_IMPORTED_MODULE_1__["CookieService"], _account_account_service__WEBPACK_IMPORTED_MODULE_2__["AccountService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/menu/menu.component.html":
/*!******************************************!*\
  !*** ./src/app/menu/menu.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [ngSwitch]=\"showMenu()\" id=\"menu-container\" class=\"fixed-top\">\n\n<mat-toolbar color=\"#03A9F4\" class=\"w-100 d-flex\" *ngSwitchCase=\"'globalMenu'\">\n\n    <span>eTip</span>\n    <div class=\"w-50 d-flex \">\n\n      <a mat-raised-button routerLink=\"/home\" color=\"warn\">Home</a>\n      <a mat-raised-button routerLink=\"/search\" color=\"warn\">Search</a>\n      <a mat-raised-button routerLink=\"/campaign\" color=\"warn\">Campaign</a>\n      <a mat-raised-button routerLink=\"/login\" color=\"warn\">login</a>\n      <a mat-raised-button routerLink=\"/account\" color=\"warn\">account</a>\n      <a mat-raised-button routerLink=\"/creator\" color=\"warn\">Createur</a>\n      <a mat-raised-button routerLink=\"/announcer\" color=\"warn\">Announcer</a>\n    </div>\n  <span>{{isAuthenticated()}}</span>\n</mat-toolbar>\n<mat-toolbar class=\"w-100 d-flex\"  *ngSwitchCase=\"'accountMenu'\" id=\"account-menu\">\n    \n  <button mat-button #menuIcon (click)=\"toggle()\"><i class=\"material-icons md-36 md-dark\">{{isMenuOpenedIcon()}}</i></button>\n  <div class=\"d-flex w-100 float-right user-part\" >\n    <a class=\"float-right m-2\" routerLink=\"/account\" [matMenuTriggerFor]=\"appMenu\">{{currentUser ? currentUser.email : ''}}</a>\n  </div>\n  <mat-menu #appMenu>\n      <button mat-menu-item><a class=\"float-right m-2\" (click)=\"logout()\"><i class=\"material-icons md-36 md-dark\">power_settings_new</i></a></button>\n    </mat-menu>\n\n</mat-toolbar>\n\n<mat-toolbar class=\"w-100 d-flex\"  *ngSwitchCase=\"'homeMenu'\" id=\"home-menu\">\n    \n  <button mat-button #menuIcon (click)=\"toggle()\"><i class=\"material-icons md-36 md-dark\">{{isMenuOpenedIcon()}}</i></button>\n  <div class=\"d-flex w-100 float-right user-part\" >\n    <a class=\"float-right m-2\" routerLink=\"/account\" [matMenuTriggerFor]=\"appMenu\">{{currentUser ? currentUser.email : ''}}</a>\n  </div>\n  <mat-menu #appMenu>\n      <button mat-menu-item><a class=\"float-right m-2\" (click)=\"logout()\"><i class=\"material-icons md-36 md-dark\">power_settings_new</i></a></button>\n    </mat-menu>\n\n</mat-toolbar>\n</div>\n"

/***/ }),

/***/ "./src/app/menu/menu.component.less":
/*!******************************************!*\
  !*** ./src/app/menu/menu.component.less ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-toolbar {\n  background: #2c3e50;\n  height: 50px;\n  color: #FF5722;\n}\nmat-toolbar > a {\n  font-size: 20px;\n  font-weight: bold;\n}\nmat-toolbar i {\n  font-size: 38px;\n  font-weight: bold;\n}\nmat-toolbar .user-part {\n  flex-direction: row-reverse;\n}\n"

/***/ }),

/***/ "./src/app/menu/menu.component.ts":
/*!****************************************!*\
  !*** ./src/app/menu/menu.component.ts ***!
  \****************************************/
/*! exports provided: MenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuComponent", function() { return MenuComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var _account_account_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../account/account.service */ "./src/app/account/account.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MenuComponent = /** @class */ (function () {
    function MenuComponent(router, authService, accountService) {
        this.router = router;
        this.authService = authService;
        this.accountService = accountService;
        this.route = ['/login', '/register', ''];
        this.global = ['/creator', '/accouncer', '/search'];
        this.home = ['/home'];
        this.account = ['/account'];
        this.menuIcon = {
            first: 'menu',
            second: 'close'
        };
        this.openSideNav = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    MenuComponent.prototype.ngOnInit = function () {
        this.getUser();
        this.currentPage = this.router.url;
        // const toggleButton = new mdc.iconButton.MDCIconButtonToggle(document.getElementById('add-to-favorites'));
    };
    MenuComponent.prototype.isMenuOpenedIcon = function () {
        if (this.isMenuOpen) {
            return 'menu';
        }
        return 'close';
    };
    MenuComponent.prototype.showMenu = function () {
        this.currentPage = this.router.url;
        var final = '';
        for (var _i = 0, _a = this.global; _i < _a.length; _i++) {
            var url = _a[_i];
            if (url.indexOf(this.currentPage) >= 0) {
                final = 'globalMenu';
            }
        }
        for (var _b = 0, _c = this.account; _b < _c.length; _b++) {
            var url = _c[_b];
            if (this.currentPage.indexOf(url) >= 0) {
                final = 'accountMenu';
            }
        }
        for (var _d = 0, _e = this.home; _d < _e.length; _d++) {
            var url = _e[_d];
            if (this.currentPage.indexOf(url) >= 0) {
                final = 'homeMenu';
            }
        }
        return final;
    };
    MenuComponent.prototype.isAuthenticated = function () {
        return this.authService.isAuthenticated();
    };
    MenuComponent.prototype.getUser = function () {
        var _this = this;
        this.accountService.getCurrent().subscribe(function (res) {
            _this.currentUser = res;
        });
    };
    MenuComponent.prototype.logout = function () {
        this.accountService.logout();
        this.router.navigate(['/home']);
    };
    MenuComponent.prototype.toggle = function () {
        this.openSideNav.emit(null);
        this.isMenuOpen = !this.isMenuOpen;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('menuIcon'),
        __metadata("design:type", Object)
    ], MenuComponent.prototype, "menuIcon", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('btn'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButton"])
    ], MenuComponent.prototype, "btn", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], MenuComponent.prototype, "openSideNav", void 0);
    MenuComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-menu',
            template: __webpack_require__(/*! ./menu.component.html */ "./src/app/menu/menu.component.html"),
            styles: [__webpack_require__(/*! ./menu.component.less */ "./src/app/menu/menu.component.less")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"], _account_account_service__WEBPACK_IMPORTED_MODULE_4__["AccountService"]])
    ], MenuComponent);
    return MenuComponent;
}());



/***/ }),

/***/ "./src/app/search/search.component.css":
/*!*********************************************!*\
  !*** ./src/app/search/search.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/search/search.component.html":
/*!**********************************************!*\
  !*** ./src/app/search/search.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-12\">\n    <div class=\"card text-center\">\n        <div class=\"card-header\">\n      Make a new research\n      </div>\n      <div class=\"card-body\">\n          <h5 class=\"card-title\">Search a creator or a campaign</h5>\n          <p class=\"card-text\">You can easily search for your favorite content creator or for a special compaign he created.</p>\n          <mat-form-field class=\"\">\n            <input matInput placeholder=\"Search a campaign or a creator\" [(ngModel)]=\"searchFilter\">\n          </mat-form-field>\n          <button mat-raised-button color=\"primary\" (click)=\"search()\">Search</button>\n        </div>\n    </div>\n\n  </div>\n<div class=\"col-8\">\n  <div class=\"card text-center\" *ngFor=\"let i of [1,2,3]\">\n    <div class=\"card-body\">\n      <h5 class=\"card-title\">Search a creator or a campaign</h5>\n      <p class=\"card-text\">You can easily search for your favorite content creator or for a special compaign he created.</p>\n    </div>\n  </div>\n</div>\n<div class=\"col-4\">\n    <div class=\"card text-center\">\n      <div class=\"card-header\">\n          <h5 class=\"card-title\">Popular Creator</h5>\n          <p class=\"card-text\">there is a list of popupal creator you may appreciate.</p>\n      </div>\n        <div class=\"card-body\"  *ngFor=\"let i of [1,2,3]\">\n          <h5 class=\"card-title\">Search a creator or a campaign</h5>\n          <p class=\"card-text\">You can easily search for your favorite content creator or for a special compaign he created.</p>\n        </div>\n      </div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/search/search.component.ts":
/*!********************************************!*\
  !*** ./src/app/search/search.component.ts ***!
  \********************************************/
/*! exports provided: SearchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchComponent", function() { return SearchComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SearchComponent = /** @class */ (function () {
    function SearchComponent(router) {
        var _this = this;
        this.router = router;
        this.router.params.subscribe(function (params) {
            _this.doSearch(params['searchFilter']);
        });
    }
    SearchComponent.prototype.ngOnInit = function () {
        console.log(this.searchFilter);
    };
    SearchComponent.prototype.doSearch = function (params) {
        // search for reference or tag in the BDD
    };
    SearchComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-search',
            template: __webpack_require__(/*! ./search.component.html */ "./src/app/search/search.component.html"),
            styles: [__webpack_require__(/*! ./search.component.css */ "./src/app/search/search.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], SearchComponent);
    return SearchComponent;
}());



/***/ }),

/***/ "./src/app/tip/tip.component.css":
/*!***************************************!*\
  !*** ./src/app/tip/tip.component.css ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/tip/tip.component.html":
/*!****************************************!*\
  !*** ./src/app/tip/tip.component.html ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"h-100\">\n  <mat-drawer-container [hasBackdrop]=\"false\">\n    <mat-drawer opened [mode]=\"'push'\">\n      <ul>\n        <li>li nuùero 1</li>\n        <li>li nuùero 1</li>\n        <li>li nuùero 1</li>\n        <li>li nuùero 1</li>\n      </ul>\n    </mat-drawer>\n    <div class=\"h-100 w-auto\">\n      getgrdgerg\n      Lorem ipsum, dolor sit amet consectetur adipisicing elit. \n      Tempora, dolores temporibus accusantium eligendi nulla amet f\n      ugit ipsa molestias voluptatibus inventore, esse dicta quas id.\n      Sapiente voluptate est eius tempore maiores!\n    </div>\n  </mat-drawer-container>\n</div>"

/***/ }),

/***/ "./src/app/tip/tip.component.ts":
/*!**************************************!*\
  !*** ./src/app/tip/tip.component.ts ***!
  \**************************************/
/*! exports provided: TipComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TipComponent", function() { return TipComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TipComponent = /** @class */ (function () {
    function TipComponent() {
    }
    TipComponent.prototype.ngOnInit = function () {
    };
    TipComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-tip',
            template: __webpack_require__(/*! ./tip.component.html */ "./src/app/tip/tip.component.html"),
            styles: [__webpack_require__(/*! ./tip.component.css */ "./src/app/tip/tip.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TipComponent);
    return TipComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    backendApi: 'http://localhost:8888'
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/fatih/Projects/etip/web/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map