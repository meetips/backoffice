import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { isNullOrUndefined } from 'util';
import { PossiblePayoutProfile } from '../models/models';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss']
})
export class ConfirmationDialogComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data: any) {
    if (!isNullOrUndefined(data)) {

      if (data.hasOwnProperty('message')) {
        this.message = data.message;
      }
      if (data.hasOwnProperty('isPayout')) {
        this.isPayout = data.isPayout;
      }
      if (data.hasOwnProperty('title')) {
        this.title = data.title;
      }
      if (data.hasOwnProperty('possiblePayoutProfile')) {
        this.possiblePayoutProfile = data.possiblePayoutProfile;
      }
    }
  }

  message: string;
  payoutModel: any = {};
  isPayout = false;
  title: string;
  possiblePayoutProfile: PossiblePayoutProfile;
  responseObject: any = {
    action: 'save',
    dest: ''
  };

  ngOnInit() {
  }

  save() {
    if (this.isPayout) {
      return this.responseObject;
    }
    return 'save';
  }

  setPayoutTo(dest: 'bank' | 'paypal') {
    this.responseObject.dest = dest;
  }

  isPayoutToBank() {
    return this.responseObject.dest === 'bank';
  }

  isPayoutToPaypal() {
    return this.responseObject.dest === 'paypal';
  }

}
