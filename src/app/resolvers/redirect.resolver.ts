import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable, of } from "rxjs";
import { AccountService } from "../services/account/account.service";
import { User } from "../models/models";


@Injectable()
export class RedirectResolver implements Resolve<any> {
    constructor(private accountService: AccountService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        
        const user: User = route.parent.data["account"];

        console.log("route ", route);

        if (user.is_announcer) {
        }

        return ;
    }
}