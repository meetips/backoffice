import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import { Observable, of } from "rxjs";
import { AccountService } from "../services/account/account.service";
import { User } from "../models/models";


@Injectable()
export class RedirectAnnouncerResolver implements Resolve<any> {
    constructor(private router: Router) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        const user: User = route.parent.data["account"];
        if (!user.is_announcer) {
            this.router.navigate(["/account", "dashboard"])
        }

        return of(user);
    }
}