import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable, of } from "rxjs";
import { AccountService } from "../services/account/account.service";
import { isNullOrUndefined } from "util";


@Injectable()
export class AccountResolver implements Resolve<Account> {
    constructor(private accountService: AccountService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Account> | Promise<Account>{  
        console.log("route", route["data"]);
        if (!isNullOrUndefined(route.data["account"])) {
            console.log("route.data", route.data);
            
            return of(route.data["account"]);
        }
        if (!isNullOrUndefined(route.parent.data["account"])) {
            console.log("route.parent.data", route.parent.data);

            return of(route.parent.data["account"]);
        }
        return this.accountService.getCurrent().toPromise();
    }
}