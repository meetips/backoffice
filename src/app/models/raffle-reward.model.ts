import { Reward } from './reward.model';

export class RaffleReward {
    id: number;
    reward: Reward;
    raffle: number;
    quantity: number;
}