import { Reward } from "./reward.model";
import { RaffleReward } from './raffle-reward.model';

export class Raffle {
    id?: number;
    name: string;
    description: string;
    rewards: RaffleReward[];
    startDate: Date;
    endDate: Date
}