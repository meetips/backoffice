import { Image } from "./models";

export class Reward {
    public id?: number;
    public name?: string;
    public description?: string;
    public quantity?: number;
    public thumbnail_media?: Image;
}
