import { Reward } from "./reward.model";

export class CreateAccount {
    public email: string;
    public password: string;
    public password_confirm: string;
    public username?: string;
    public is_creator?: boolean;
    public is_announcer?: boolean;
}

export class CommonFields {
    public created_at?: Date;
    public updated_at?: Date;
}

export class UserConnexion extends CommonFields {
    public email?: string;
    public password?: string;
}

export class CustomDate {
    public day?: number;
    public month?: number;
    public year?: number;
}

export class User extends UserConnexion {
    public id?: string;
    public first_name?: string;
    public last_name?: string;
    public username?: string;
    public address?: string;
    public city?: string;
    public country?: string;
    public postal_code?: string;
    public birthdate?: Date | CustomDate;
    public is_active?: boolean;
    public is_creator?: boolean;
    public is_announcer?: boolean;
    public userType?: string;
    public is_staff?: boolean;
    public thumbnail?: any;
    public thumbnailLoaded?: any;
    public token: string;

}


export class Campaign extends CommonFields {
    public id?: string;
    public name: string;
    // tslint:disable-next-line:no-inferrable-types
    public description = '';
    public tags?: string;
    public objective?: string;
    public url: string;
    public thanks_message?: string;
    public objective_amount?: number;
    // tslint:disable-next-line:no-inferrable-types
    public show_objective_amount?: boolean = false;
    // tslint:disable-next-line:no-inferrable-types
    public show_tip_count?: boolean = true;
    // tslint:disable-next-line:no-inferrable-types
    public show_current_amount?: boolean = false;
    public tips_count?: number;
    public tips_sum?: number;
    public tips_count_month?: number;
    public tips_sum_month?: number;
    public current_amount?: number;
    public is_monthly?= true;
    public is_active?= true;
    public is_activable?= false;
    public banner_media?: any;
    public bannerLoaded?: any;
    public thumbnail_media?: any;
    public thumbnailLoaded?: any;
    public rewards?: Reward[] = [];
    public end_date?: Date;
    public code_category_viewpay?: CodeCategoryViewPay;
    public user_username?: string;

    public potential_users?: number;
    public categories?: Category[] = [];
    public demographic_data?: Category[] = [];
    public audiences?: Category[] = [];

    //for the graphe
    public day?: string;
    public count_tips_total?: number = 0;
    public count_tips_per_day?: number = 0;
    public sum_amount_total?: number = 0;
    public sum_amount_per_day?: number = 0;
    public avg_cost_per_view?: number = 0;

    points?: any[];

}

export enum CodeCategoryViewPay {
    'Actus' = 0,
    'Sports' = 1,
    'Economie' = 2,
    'Politique' = 3,
    'Environnement' = 4,
    'Science' = 5,
    'Technologie' = 6,
    'Autres' = 7,
    'Divertissement' = 8,
    'Science-Fiction' = 9,
    'Jeunesse' = 10,
    'Série - télé' = 11,
    'Comics, BD' = 12
}

export const USER_TYPE_ENUM = {
    IS_CREATOR_USER: 'IS_CREATOR_USER',
    IS_SIMPLE_USER: 'IS_SIMPLE_USER',
    IS_ANNOUNCER_USER: 'IS_ANNOUNCER_USER'
};

export class UserPayoutMethode extends CommonFields {
    public id?: string;
    public iban?: string;
    public bic?: string;
    public paypal_account?: string;
}

export class Payout extends CommonFields {
    public id?: string;
    public amount?: number;
    public status?: string;
    public method?: string;
    public user?: number;
    public user_email?: string;
    public user_username?: string;
    public user_iban?: string;
    public user_bic?: string;
    public user_paypal?: string;
    user_profile: User;
}

export class Payin extends CommonFields {
    public id?: string;
    public amount?: number;
    public status?: string;
    public method?: string;
    public user?: number;
    public advertising?: number;
    public advertising_name?: string;
    public user_email?: string;
}

export class Password {
    public current_password?: string;
    public new_password?: string;
    public password_confirm?: string;
}

export class PossiblePayoutProfile {
    public has_paypal?: boolean;
    public has_bank?: boolean;
}

export class PayoutProfile {
    public iban?: string;
    public bic?: string;
    public paypal?: string;
    public user?: string;
}

export class Advertising extends CommonFields {
    public id?: number;
    public announcer?: User;
    public is_active?: boolean;
    public name?: string;
    public description?: string;
    public objective?: number;
    public tags?: string;
    public thumbnail?: any;
    public thumbnailLoaded?: any;
    public video_url?: string;
    public redirect_url?: string;
    public start_at?: Date | string;
    public end_at?: Date| string;
    public amount?: number;
    public target_view?: number;
    public cost_per_view?: number;
    public enable_start_at?: boolean;
    public enable_end_at?: boolean;
    public is_approuved?: boolean;
    public import_video?: boolean;
    public target_view_per_day?: boolean;
    public use_youtube_thumbnail?: boolean;

    public youtube_video_id?: string;
    public video_media?: Video;
    public thumbnail_media?: Image;

    public categories?: Category[] = [];
    public audiences?: Audience[] = [];
    public languages?: Language[] = [];
    public countries?: Country[] = [];
    public demographic_datas?: DemographicData[] = [];


    public day?: string;
    public count_tips_total?: number = 0;
    public count_tips_per_day?: number = 0;
    public sum_cost_total?: number = 0;
    public sum_cost_per_day?: number = 0;
    public avg_cost_per_view?: number = 0;

    points?: any[];
}

export class Media {
    id: string;
    name: string;
    remote_url: string;
    size: number;
    user: number;
}

export class Video extends Media {
    duration: number;
    file: string;
}

export class Image extends Media {
    file: string;
}


export class SimpleChartPoints {
    public day?: string;
    public number?: number;
}

export class AdvertisingForChart {
    public id?: number;
    public name?: string;
    public day?: string;
    public count_tips_total?: number;
    public count_tips_per_day?: number;
    public sum_amount_used_total?: number;
    public sum_amount_used_per_day?: number;
    public avg_cost_per_view?: number; 
    public points?: SimpleChartPoints[] = [];
}

export class Step {

    isCompleted() {

    }
}

export class Step1 extends Step {
    name: string;
    continue = false;
    isCompleted() {
        return !!this.name && this.continue;
    }
}

export class Step2 extends Step {
    video_url: string;
    redirect_url: string;
    continue = false;

    isCompleted() {
        return !!this.video_url && !!this.redirect_url && this.continue;
    }
}

export class Step3 extends Step {
    start_at?: Date;
    end_at?: Date;
    continue = false;

    isCompleted() {
        return this.continue;
    }
}

export class Step4 extends Step {
    target_view: number;
    continue = false;

    isCompleted() {
        return !!this.target_view && this.continue;
    }
}

export class Category {
    id?: any;
    name?: string;
    children?: Category[] = [];
    parent?: number;
    parent_id?: number;
    level?: number;
    has_children?: boolean;

    showChildren ? = false;
    isMatching ? = true;

    childLoading ? = false;
    hasMatchingChildren ? = false;

    constructor(name, id?, children?) {
        this.id = id;
        this.name = name;
        this.children = children;
    }

}

export class ImportCategory {
    '1'?: string;
    '2': string;
    '3': string[];
}

export class Audience extends Category {
    children?: Audience[] = [];
    description?: string;
}


export class DemographicData {
    id?: number;
    name?: string;
    children?: any[] = [];
    parent?: number;
    level?: number;
    has_children: boolean;
}

export class Language {
    locale: string;
    name: string
}

export class Country {
    locale: string;
    name: string;
}

