import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-announcer',
  templateUrl: './announcer.component.html',
  styleUrls: ['./announcer.component.scss']
})
export class AnnouncerComponent implements OnInit {

  constructor() { }

  public model: any = {
    firstname: '',
    lastname: '',
    email: '',
    password: '',
    company_name: '',
    password_confirm: '',
    is_announcer_user: true
  };

  ngOnInit() {
  }

}
