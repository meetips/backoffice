import { Component, OnInit, Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AccountService } from 'src/app/services/account/account.service';

@Component({
  selector: 'app-creator',
  templateUrl: './creator.component.html',
  providers: [AccountService],
  styleUrls: ['./creator.component.scss']
})
export class CreatorComponent implements OnInit {

  constructor(private titleService: Title ) {
    titleService.setTitle('Createur');
  }

  ngOnInit() {
  }

}
