import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.css']
})
export class ErrorPageComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute) { }


  error: any = {
    message: '',
    code: ''
  };

  ngOnInit() {
    this.activatedRoute.params.subscribe(res => {
      this.error = { code: res['code'], message: res['message'] };
    });

  }

}
