import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { AnnouncerComponent } from './announcer/announcer.component';
import { CreatorComponent } from './creator/creator.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { HomeComponent } from './home/home.component';
import { CategorySelectorComponent } from '../shared/components/category-selector/category-selector.component';

const routeList: Routes = [
  { path: '', component: MainComponent, children: [
    // // { path: 'campaign/:url', component: CampaignComponent },
    // // { path: 'campaign', component: CampaignComponent },
    // { path: 'announcer', component: AnnouncerComponent },
    // { path: 'creator', component: CreatorComponent },
    // { path: 'error/:code/:message', component: ErrorPageComponent },
    { path: "category", component: CategorySelectorComponent },
    { path: 'home', redirectTo: '' },
    { path: '', component: HomeComponent,  },

  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routeList)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
