import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main/main.component';
import { ArticleComponent } from './article/article.component';
import { HomeComponent } from './home/home.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { AnnouncerComponent } from './announcer/announcer.component';
import { CreatorComponent } from './creator/creator.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { CoreModule } from '../core/core.module';

@NgModule({
  imports: [
    CommonModule,
    MainRoutingModule,
    SharedModule,
    CoreModule
  ],
  declarations: [
    MainComponent,
    ArticleComponent,
    HomeComponent,
    ErrorPageComponent,
    AnnouncerComponent,
    CreatorComponent,
  ],
})
export class MainModule { }
