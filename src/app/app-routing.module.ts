import {
  Routes,
  RouterModule,
  UrlSegment,
  UrlSegmentGroup,
  Route,
  convertToParamMap,
} from "@angular/router";
import { DemographicDataComponent } from "./shared/components/demographic-data/demographic-data.component";

import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
import { AuthGuard } from "./guards/auth.guard";
import { AccountResolver } from "./resolvers/account.resolver";

export function urlMatcher(urls: string[]) {
  return (segments: UrlSegment[]) => {
    let res = null;
    for (let elt of urls) {
      segments.forEach((value) => {
        if (value.path === elt) {
          res = { consumed: segments };
        }
      });
    }
    return res;
  };
}

const routes: Routes = [
  {
    path: "account",
    loadChildren: () =>
      import("./account/account.module").then((m) => m.AccountModule),
    canLoad: [AuthGuard],
  },
  // { loadChildren: './auth/auth.module#AuthModule', matcher: urlMatcher(["login", "register"]) },
  {
    path: "auth",
    loadChildren: () => import("./auth/auth.module").then((m) => m.AuthModule),
  },
  {
    path: "",
    loadChildren: () => import("./main/main.module").then((m) => m.MainModule),
    // pathMatch: "full",
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
