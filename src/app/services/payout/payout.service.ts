import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PayoutService {


  constructor(private http: HttpClient) { }

  getPayouts() {
    return this.http.get(environment.backendApi + '/api/payouts/');
  }

  getPayout(id: string) {
    return this.http.get(environment.backendApi + '/api/payouts/' + id + '/');
  }

  getCurrentBalance() {
    return this.http.get(environment.backendApi + '/api/payouts/current_balance/');
  }

  receivePayment(model) {
    return this.http.post(environment.backendApi + '/api/payouts/', model);
  }
}
