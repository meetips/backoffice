import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Advertising } from '../../models/models';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { UsedNames } from 'src/app/shared/models/used-names.model';

@Injectable({
  providedIn: 'root'
})
export class AdvertisingService {

  constructor(private http: HttpClient) { }

  currentAdvertising: Advertising = null;

  createAdvertising(model) {
    return this.http.post(environment.backendApi + '/api/advertisings/', model);
  }

  updatePatialCampaign(model) {
    return this.http.patch(environment.backendApi + '/api/advertisings/' + model.id + '/', model);
  }

  getAdvertisings() {
    return this.http.get(environment.backendApi + '/api/advertisings/');
  }

  getData() {
    return this.http.get(environment.backendApi + '/api/advertisings/data/');
  }

  getDetailByDate(id: number, fromDate?: string, toDate?: string) {
    return this.http.get(environment.backendApi + '/api/advertisings/' + id + 
                      '/detail_by_day/?from_date=' + fromDate + '&to_date=' + toDate);
  }

  getAdvertising(id: number) {
    return this.http.get(environment.backendApi + '/api/advertisings/' + id + '/');
  }

  getAmountToPay(id: number, percent: number) {
    return this.http.get(environment.backendApi + '/api/advertisings/' + id + '/get_amount_to_pay/?percent=' + percent);
  }

  getFileUrl(url: string) {
    return `${environment.backendApi}${url}`;
  }

  getUsedNames(): Observable<UsedNames> {
    return this.http.get<UsedNames>(environment.backendApi + "/api/advertisings/used_names/");
  }

}
