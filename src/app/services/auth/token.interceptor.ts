import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpResponse
} from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private auth: AuthService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (request.headers.has('skip-auth')) {
            request.headers.delete('skip-auth');
            return next.handle(request);
        } else {

            if (this.auth.isAuthenticated()) {
                request = request.clone({
                    setHeaders: {
                        Authorization: `Bearer ${this.auth.getToken()}`,
                        // 'Content-Type': 'application/json',
                    },
                    responseType: 'json',
                });
            } else {
                request = request.clone({
                    setHeaders: {
                        // 'Content-Type': 'application/json',
                    },
                    responseType: 'json',
                });
            }
            return next.handle(request).pipe(
                map((res: HttpResponse<any>) => {
                    if (res.status === 401) {
                        this.auth.logout();
                    }
                    return res;
                })
            );
        }

    }
}
