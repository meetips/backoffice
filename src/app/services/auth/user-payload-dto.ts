export class UserPayloadDTO {
    firstname: string;
    lastname: string;
    username: string;
    email: string;
    is_creator: boolean;
    is_announcer: boolean;
    is_staff: boolean;
    is_admin: boolean;
    token: string;
}