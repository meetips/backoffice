import { Injectable } from '@angular/core';
import { UserPayloadDTO } from './user-payload-dto';
import { JwtHelperService } from '@auth0/angular-jwt';
import { helpers } from 'chart.js';


@Injectable({
    providedIn: 'root'
  })
export class AuthService {

    constructor() {

    }

    helper = new JwtHelperService();


    public getToken(): string {
        // return this._cookieService.get('accessToken');
        return localStorage.getItem('accessToken');
    }

    public setAccessToken(token: string) {
        // return this._cookieService.get('accessToken');
        localStorage.setItem('accessToken', token);
    }

    public isAuthenticated(): boolean {
        // get the token
        const token = this.getToken();
        // return a boolean reflecting
        // whether or not the token is expired
        if (token === undefined || token === null || token === '') {
            return false;
        }
        if (this.isExpired(token)) {
            this.logout();
            return false;
        }

        return true;
    }

    public logout() {
        localStorage.removeItem("accessToken");
    }

    public getUserEmail() {
        const token = localStorage.getItem('accessToken');
        const user = this.helper.decodeToken(token) as UserPayloadDTO;
        return user.email;
    }

    public geConnectedUser() {
        const token = localStorage.getItem('accessToken');
        return this.helper.decodeToken(token) as UserPayloadDTO;
    }

    public isExpired(token?: string) {
        if (!token) {    
            token = localStorage.getItem('accessToken');
        }
        return this.helper.isTokenExpired(token);
    }
}
