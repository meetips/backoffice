import { Injectable, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor() { }

  // title = new EventEmitter<String>();

  public title = new BehaviorSubject<string>("");
  public title$ = this.title.asObservable();
}
