import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { Payin } from 'src/app/models/models';

@Injectable({
  providedIn: 'root'
})
export class PayinService {

  constructor(private http: HttpClient) { }

  getMyPayinList() {
    return this.http.get<Payin[]>(environment.backendApi + '/api/payins/');
  }
}
