import { TestBed, inject } from '@angular/core/testing';

import { PayinService } from './payin.service';

describe('PayinService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PayinService]
    });
  });

  it('should be created', inject([PayinService], (service: PayinService) => {
    expect(service).toBeTruthy();
  }));
});
