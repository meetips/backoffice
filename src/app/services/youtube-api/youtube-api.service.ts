import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
// import { ISO8601_DATE_REGEX } from '@angular/common/src/i18n/format_date';

@Injectable({
  providedIn: 'root'
})
export class YoutubeApiService {

  constructor(private http: HttpClient) { }

  youtubeApi = 'https://www.googleapis.com/youtube/v3/videos';


  getImageUrl(videoId: string) {
    return 'https://img.youtube.com/vi/' + videoId +  '/hqdefault.jpg';
  }

  async getVideoDurationInfo(videoId: string) {
    const content = await  this.http.get<any>(this.youtubeApi + 
      `?id=${videoId}&key=${environment.youtubeKey}&part=contentDetails`,{
      headers:  {
        'skip-auth': 'true'
      }
    }).toPromise();
    (async () => {
      return await content;
    })
    const duration = content.items[0].contentDetails.duration;
    // const duration = "PT8M4S";
    
    const reg = /(PT(((?<hours>\d+)H)|)(((?<minutes>\d+)M)|)(?<seconds>\d+)S)/u;
    const result = reg.exec(duration);
    if (!!result && result['groups']) {
      const {hours, minutes, seconds} = result['groups'];
      return {
        hours, minutes, seconds
      }
    }
    return null;
  }

  getIdFromUrl(url: string) {
    const youtubeTypeUrl1 = url.split('watch?v=');  // https://www.youtube.com/watch?v=xy9HgIfYphM
    if (youtubeTypeUrl1.length === 2) {
      return youtubeTypeUrl1[1];
    }
    const youtubeTypeUrl2 = url.split('.be/');      // https://youtu.be/xy9HgIfYphM
    if (youtubeTypeUrl2.length === 2){
      return youtubeTypeUrl2[1];
    }
  }

  constructVideoUrlById(videoId: string) {
    return `https://youtube.com/watch?=${videoId}`;
  }

  constructEmbededVideoUrlById(videoId: string) {
    return `https://www.youtube.com/embed/${videoId}`;
  }

}
