import { TestBed, inject } from '@angular/core/testing';

import { ExclusivityService } from './exclusivity.service';

describe('ExclusivityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExclusivityService]
    });
  });

  it('should be created', inject([ExclusivityService], (service: ExclusivityService) => {
    expect(service).toBeTruthy();
  }));
});
