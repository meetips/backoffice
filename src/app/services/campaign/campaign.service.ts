import { Injectable, EventEmitter } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Campaign } from '../../models/models';
import { isNullOrUndefined, isArray, isObject } from 'util';

@Injectable({
  providedIn: 'root'
})
export class CampaignService {

  constructor(
    private http: HttpClient) {
    this.currentCampaign = new EventEmitter<any>();

  }
  public currentCampaign: EventEmitter<any>;
  public campaignApercu: any;
  public modalCampaign: Campaign;
  public selectedCampaign: Campaign;

  createCampaign(model) {
    const headers = new HttpHeaders();
    const options = {
      headers: headers
    };
    // 'Content-Type', 'multipart/form-data'
    return this.http.post<any>(environment.backendApi + '/api/campaigns/', model, options);
  }

  getCampaigns() {
    return this.http.get<any>(environment.backendApi + '/api/campaigns/' /* my_campaigns/'*/);
  }

  getCampaign(id: string) {
    return this.http.get<any>(environment.backendApi + '/api/campaigns/' + id + '/');
  }

  getCampaignByUrl(url: string) {
    return this.http.get<any>(environment.backendApi + '/api/campaigns/get_by_url/' + url + '/');
  }

  getDetailByDay(id: number, fromDate?: string, toDate?: string) {
    return this.http.get(environment.backendApi + '/api/campaigns/' + id +
      '/detail_by_day/?from_date=' + fromDate + '&to_date=' + toDate);
  }

  updateCampaign(model: FormData) {
    return this.http.put<any>(environment.backendApi + '/api/campaigns/' + model.get('id') + '/', model);
  }

  updatePatialCampaign(model: FormData) {
    return this.http.patch<Campaign>(environment.backendApi + '/api/campaigns/' + model.get('id') + '/', model);
  }

  verifyUrl(url: string) {
    return this.http.get<any>(environment.backendApi + '/api/campaigns/verify_url/?url=' + url);
  }

  createFormData(campaign: any, formData?: FormData) {
    if (!formData) {
      formData = new FormData();
    }
    for (const property of Object.keys(campaign)) {
      const value = campaign[property];
      if (!isNullOrUndefined(value)) {
        if (isArray(value)) {
          value.forEach(element => {
            formData.append(property, element);
          });
        } else if (isObject(value)) {
          formData = this.createFormData(value, formData);
          // for (const dataProp of Object.keys(value)) {
          //   if (!isNullOrUndefined(value[dataProp])) {
          //     formData.append(`${property}.${dataProp}`, value[dataProp]);
          //   }
          // }
        } else {
          formData.append(property, value);
        }
      }
    }
    return formData;
  }
}
