import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TipService {

  constructor(private http: HttpClient) { }

  makeTip(model: any) {
    return this.http.post(environment.backendApi + '/api/tips/', model);
  }

  meta() {
    return this.http.get(environment.backendApi + '/api/tips/meta/');
  }


}
