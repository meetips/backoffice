import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AudienceService {

  constructor(private http: HttpClient) { }

  getAllAudiences() {
    return this.http.get(environment.backendApi + '/api/audiences/all/');
  }

  filter(query_string) {
      return this.http.post(environment.backendApi + '/api/audiences/search/', {query_string});
  }
}
