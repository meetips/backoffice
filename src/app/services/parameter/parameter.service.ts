import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { relativeTimeThreshold } from 'moment';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class ParameterService {

  constructor(private http: HttpClient) { }

  public static PARAMETERS = {
    ADVERTISING_CHOICES_MIN_LENGTH: 'ADVERTISING_CHOICES_MIN_LENGTH',
    ADVERTISING_CHOICES_MAX_LENGTH: 'ADVERTISING_CHOICES_MAX_LENGTH',
    PERCENT_PER_REFERRER: 'PERCENT_PER_REFERRER',
    MAX_PERCENT_PER_REFERRER: 'MAX_PERCENT_PER_REFERRER',
    MAX_VIEW_PER_USER_DAY: 'MAX_VIEW_PER_USER_DAY',
    BASE_CPV: 'BASE_CPV',
    BASE_AD_DURATION: 'BASE_AD_DURATION',
    COST_PER_SECOND: 'COST_PER_SECOND',
    MAX_ADVERTISING_THUMBNAIL_SIZE: 'MAX_ADVERTISING_THUMBNAIL_SIZE',
    MAX_ADVERTISING_VIDEO_SIZE: 'MAX_ADVERTISING_VIDEO_SIZE',
    MAX_CAMPAIGN_THUMBNAIL_SIZE: 'MAX_CAMPAIGN_THUMBNAIL_SIZE',
    MAX_PROFILE_THUMBNAIL_SIZE: 'MAX_PROFILE_THUMBNAIL_SIZE',
    MAX_CAMPAIGN_BANNER_SIZE: 'MAX_CAMPAIGN_BANNER_SIZE',
  }

  getParameters() {
    return this.http.get(environment.backendApi + '/api/parameters/');
  }

  getParameterByName<T>(name: string) {
    return this.http.get<T>(environment.backendApi + '/api/parameters/' + name  +'/');
  }

  updateParameterByName(name: string, update) {
    return this.http.put(environment.backendApi + '/api/parameters/' + name  +'/', update);
  }
}
