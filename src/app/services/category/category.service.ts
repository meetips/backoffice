import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Category } from 'src/app/models/models';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  getAllCategories() {
    return this.http.get(environment.backendApi + '/api/categories/all/')
  }

  getCategories() {
    return this.http.get(environment.backendApi + '/api/categories/');
  }

  getCategoriesByParent(parentId: number) {
    return this.http.get(environment.backendApi + '/api/categories/?parent=' + parentId );
  }

  getCategory(category: number) {
    return this.http.get(environment.backendApi + '/api/categories/' + category + '/');
  }


  saveCategories(categories: Category[]) {
    return this.http.post(environment.backendApi + '/api/categories/', categories);
  }

  saveCategory(category: Category) {
    return this.http.post(environment.backendApi + '/api/categories/', category);
  }

  updateCategory(category: Category) {
    return this.http.put(environment.backendApi + '/api/categories/' + category.id + '/',
      category);
  }

  patchCategory(category: Category) {
    return this.http.patch(environment.backendApi + '/api/categories/' + category.id + '/',
      category);
  }

  filter(query_string: string) {
    return this.http.post(environment.backendApi + '/api/categories/search/', {query_string});
  }
}
