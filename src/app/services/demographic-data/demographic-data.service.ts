import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DemographicDataService {

  constructor(private http: HttpClient) { }

  public getAllDemographicData() {
    return this.http.get(environment.backendApi + '/api/demographic-datas/');
  }
}
