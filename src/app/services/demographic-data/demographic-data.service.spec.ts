import { TestBed, inject } from '@angular/core/testing';

import { DemographicDataService } from './demographic-data.service';

describe('DemographicDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DemographicDataService]
    });
  });

  it('should be created', inject([DemographicDataService], (service: DemographicDataService) => {
    expect(service).toBeTruthy();
  }));
});
