import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { isNullOrUndefined } from 'util';
import { environment } from '../../../environments/environment';
import { User, PossiblePayoutProfile, PayoutProfile } from '../../models/models';

@Injectable({
  providedIn: 'root'
})
export class AccountService  {
  constructor(private http: HttpClient) {
    this.userEvent = new EventEmitter<any>();
  }

  public userEvent: EventEmitter<any>;

  public currentUser: User;

  createAccount(model): Observable<any> {
    return this.http.post<any>(environment.backendApi + '/api/auth/register/', model, {
      headers: new HttpHeaders({'Content-Type':	'application/json'}),
      responseType: 'json',
    });
    // return this.http.get<any>('http://localhost:8888/api/users/?format=json');
  }

  login(model) {
    return  this.http.post<User>(environment.backendApi + '/api/auth/log/', model, {
      headers: new HttpHeaders({'Content-Type':	'application/json'}),
      responseType: 'json',
    });
  }

  resetPassword(model) {
    return this.http.post<any>(environment.backendApi + '/api/auth/reset_password_request/', model);
  }

  resetPasswordByToken(model) {
    return this.http.put<any>(environment.backendApi + '/api/auth/' + model.user_id + '/reset_password_by_token/', model);
  }


  updateProfile(model: FormData) {
    return this.http.patch<any>(environment.backendApi + '/api/users/' + model.get("id") + '/', model, {
      //headers: new HttpHeaders({'Content-Type':	'application/json'}),
  //    responseType: 'json',
    });
  }

  getCurrent() {
    return this.http.get<any>(environment.backendApi + '/api/users/current/', {
      headers: new HttpHeaders({'Content-Type':	'application/json'}),
      responseType: 'json',
    });
  }


  updatePassword(passwords: any) {
    return this.http.put(environment.backendApi + '/api/users/change_password/', passwords);
  }

  updatePayoutProfile(model) {
    return this.http.put(environment.backendApi + '/api/users/payout_profile/', model);
  }

  resendActivationEmail(model: any) {
    return this.http.post<any>(environment.backendApi + '/api/users/resend_activation_email/', model);
  }

  activateAccount(userId: string, token: string) {
    return this.http.get(environment.backendApi + '/api/auth/' + userId + '/activate/?token=' + token);
  }

  getPayoutProfile() {
    return this.http.get<PossiblePayoutProfile>(environment.backendApi + '/api/users/payout_profile/');
  }

  getPayoutProfileDetail(): any {
    return this.http.get<PayoutProfile>(environment.backendApi + '/api/users/my_payout_profile/');
  }

  sendContactMessage(contactForm: any) {
    return this.http.post(environment.backendApi + '/api/users/send_contact_message/', contactForm);
  }

  getCurrentBalance(): any {
    return this.http.get(environment.backendApi + '/api/users/current_balance/');
  }


}
