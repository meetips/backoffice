import { Injectable } from '@angular/core';
import { Raffle } from 'src/app/models/raffle.model';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RaffleService {


  constructor(private http: HttpClient) { }

  save(model: Raffle) {
    delete model.id;
    return this.http.post(environment.backendApi + '/api/raffles/', model);
  }

  update(model: Raffle) {
    return this.http.put(environment.backendApi + '/api/raffles/' + model.id + '/', model);
  }

  getRaffles() {
    return this.http.get<Raffle[]>(environment.backendApi + '/api/raffles/');
  }

  getRaffle(id: number) {
    return this.http.get<Raffle>(environment.backendApi + '/api/raffles/' + id + '/');
  }
}
