import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Reward } from 'src/app/models/reward.model';
import { getFormData } from 'src/app/shared/utils';

@Injectable({
  providedIn: 'root'
})
export class RewardService {

  constructor(private http: HttpClient) { }

  save(model: Reward) {

    const data = getFormData(model);
    (data as any).forEach((value, key) => {

      console.log(key, value);
    });
  
    return this.http.post(environment.backendApi + '/api/rewards/', data);
  }

  update(model: Reward) {
    return this.http.put(environment.backendApi + '/api/rewards/' + model.id + '/', model);
  }

  partialUpdate(model: Reward) {
    return this.http.patch(environment.backendApi + '/api/rewards/' + model.id + '/', model);
  }

  getRewards() {
    return this.http.get<Reward[]>(environment.backendApi + '/api/rewards/');
  }

  getReward(id: string) {
    return this.http.get(environment.backendApi + '/api/rewards/' + id + '/');
  }


  getNames() {
    return this.http.get(environment.backendApi + '/api/rewards/names/');
  }
}
