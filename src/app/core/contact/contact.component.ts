import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AccountService } from 'src/app/services/account/account.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit {

  constructor(private accountService: AccountService, private toastrService: ToastrService) { }

  @ViewChild('accountSideNav', { static: false })
  accountSideNav: any;

  contact: any = {};
  isSend = false;

  ngOnInit() {
  }

  sendMessage() {
    this.isSend = !this.isSend;
    if (!!this.contact.email && !!this.contact.message && !!this.contact.subject) {
      this.toastrService.info('Demande en cours ...');
      this.accountService.sendContactMessage(this.contact).subscribe(response => {
        this.toastrService.success('Votre demande a été transmise avec succès', 'OK');
      }, err => {
        this.toastrService.warning('Votre demande n\'a pas pu être traité, veuillez réessayer plus tard', 'Une erreur est survenue');
      });
    }
  }

  log(a) {
    console.log(a);

  }

}
