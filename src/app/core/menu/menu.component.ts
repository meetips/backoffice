import { Component, OnInit, Output, EventEmitter, ViewChild, Input } from '@angular/core';
import { MatToolbar, MatButton, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { PayoutService } from 'src/app/services/payout/payout.service';
import { AccountService } from 'src/app/services/account/account.service';
import { MenuService } from 'src/app/services/menu/menu.service';
import { ToastrService } from 'ngx-toastr';
import { isNullOrUndefined } from 'util';
import { ConfirmationDialogComponent } from 'src/app/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  currentBalance: any;

  constructor(private router: Router, private authService: AuthService, private payoutService: PayoutService,
    private accountService: AccountService,
    private menuService: MenuService, private toastrService: ToastrService, private dialog: MatDialog, ) {
    this.openSideNav = new EventEmitter<any>();
  }

  route = ['/login', '/register', ''];
  global = ['/creator', '/accouncer', '/search'];
  home = ['/home', ''];
  account = ['/account'];
  userEmail: string = null;
  currentUser: any = {};
  currentPage: string;
  isMenuOpen: boolean;

  @ViewChild('menuIcon', { static: false })
  menuIcon: any = {
    first: 'menu',
    second: 'close'
  };

  @ViewChild('btn', { static: false })
  btn: MatButton;

  @Input() showLeftBtn = true;

  @Input() showSideNavBtn = false;

  @Input() showSimpleMenu;

  @Input() showMenuType;
  @Input() title;

  @Output()
  openSideNav: EventEmitter<any>;

  tooltipLogout = 'Se déconnecter';
  tooltipAccount = 'Accéder à mon compte';

  minBalance = 45;

  ngOnInit() {
    this.getUser();
    this.getCurrentBalance();
    this.currentPage = this.router.url;

    this.menuService.title$.subscribe(title => {
      this.title = title;
    });


    // const toggleButton = new mdc.iconButton.MDCIconButtonToggle(document.getElementById('add-to-favorites'));
  }

  isMenuOpenedIcon() {
    // if (this.isMenuOpen) {
    //   return 'close';
    // }
    return 'menu';
  }

  showMenu() {
    this.currentPage = this.router.url;
    let final = '';
    for (const url of this.global) {
      if (url.indexOf(this.currentPage) >= 0) {
        final = 'globalMenu';
      }
    }

    for (const url of this.account) {
      if (this.currentPage.indexOf(url) >= 0) {
        final = 'accountMenu';
      }
    }

    for (const url of this.home) {
      if (this.currentPage.indexOf(url) >= 0) {
        final = 'homeMenu';
      }
    }
    if (this.showSimpleMenu) {
      return 'simpleMenu';
    }
    return final;
  }

  isAuthenticated() {
    return this.authService.isAuthenticated();
  }

  getUser() {
    // this.accountService.getCurrent().subscribe(res => {
    //   if (!isNullOrUndefined(res) || res !== {}) {
    //     this.currentUser = res;
    //   }
    // }, err => {
    //   this.accountService.logout();
    // });
    if (this.authService.isAuthenticated()) {
      this.userEmail = this.authService.getUserEmail();
    }
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/auth/login']);
  }

  toggle() {
    this.openSideNav.emit(null);
    this.isMenuOpen = !this.isMenuOpen;
  }

  getCurrentBalance() {
    this.accountService.getCurrentBalance().subscribe(response => {
      this.currentBalance = response.current_balance;
    });
  }

  public receivePayment(dest: 'bank' | 'paypal') {
    const model = {
      is_payout: true,
      amount: this.currentBalance,
      destination: dest
    };
    this.payoutService.receivePayment(model).subscribe((res: any) => {
      this.toastrService.success('Demande de virement effectué avec succès.');
    }, err => {
      this.toastrService.warning('Une erreur est survenue, votre demande est défaillante');
    });
  }

  openDialg() {
    if (this.currentBalance < this.minBalance) {
      this.toastrService.warning('Le seuil de paiement minimum est de ' + this.minBalance + ` €`, 'Seuil non atteint');
    } else {
      this.accountService.getPayoutProfile().subscribe(res => {
        let dest = '';
        if (res.has_bank && !res.has_paypal) {
          dest = ' bancaire ?';
        } else if (res.has_paypal && !res.has_bank) {
          dest = ' paypal ?';
        } else if (res.has_bank && res.has_paypal) {
          dest = ' ?';
        } else {
          this.toastrService.warning('Vous n\'avez renseigné aucun compte pour recevoir vos paiements', 'Aucun compte');
          return;
        }

        const message = 'Confirmer-vous vouloir virer la somme de ' + this.currentBalance + ' € vers votre compte ' + dest;
        // message += "Si oui, choisissez votre mode de virement:"
        const dialogRef = this.dialog.open(ConfirmationDialogComponent,
          {
            data: {
              message,
              isPayout: true,
              title: 'Confirmer le virement',
            }
          });
        dialogRef.afterClosed().subscribe(result => {
          console.log(`Dialog result: ${result}`);
          if (result.action === 'save') {
            // this.receivePayment(result.dest);
          }
        });
      }, err => {
        this.toastrService.warning("Vous n\'avez renseigné aucun compte pour recevoir vos paiements", 'Aucun compte');
      });
    }
  }

  balanceIsEnough() {
    return this.currentBalance > this.minBalance;
  }

}
