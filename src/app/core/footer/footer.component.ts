import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor(public authService: AuthService, public router: Router) { }

  ngOnInit() {
  }

  scrollToContact(target) {
    if (this.router.url.indexOf('creator') >= 0) {
      target.scrollIntoView();
    } else if (this.router.url.indexOf('announcer') >= 0) {
      target.scrollIntoView();
    } else if (this.router.url.indexOf('account') >= 0) {
      this.router.navigate(['creator']);
      document.getElementById('contact-container').scrollIntoView();
    }

  }

}
