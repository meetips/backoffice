import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { MenuComponent } from './menu/menu.component';
import { ContactComponent } from './contact/contact.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule
  ],
  declarations: [
    FooterComponent,
    MenuComponent,
    ContactComponent
  ],
  exports: [
    FooterComponent,
    MenuComponent,
    ContactComponent
  ]

})
export class CoreModule { }
