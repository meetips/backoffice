import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanLoad, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad, CanActivate {
  constructor(private authService: AuthService) { }

  canLoad(route: Route): boolean | Observable<boolean> | Promise<boolean> {
    // if (this.authService.isAuthenticated()) {
    //   return true;
    // }

    return true;
  }

  canActivate() {
    // if (this.authService.isAuthenticated()) {
    //   return true;
    // }

    return true;
  }
}
