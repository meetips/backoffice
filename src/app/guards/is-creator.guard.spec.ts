import { TestBed, async, inject } from '@angular/core/testing';

import { IsCreatorGuard } from './is-creator.guard';

describe('IsCreatorGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsCreatorGuard]
    });
  });

  it('should ...', inject([IsCreatorGuard], (guard: IsCreatorGuard) => {
    expect(guard).toBeTruthy();
  }));
});
