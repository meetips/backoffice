import { TestBed, async, inject } from '@angular/core/testing';

import { IsAnnouncerGuard } from './is-announcer.guard';

describe('IsAnnouncerGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsAnnouncerGuard]
    });
  });

  it('should ...', inject([IsAnnouncerGuard], (guard: IsAnnouncerGuard) => {
    expect(guard).toBeTruthy();
  }));
});
