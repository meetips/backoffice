import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from '../models/models';

@Injectable({
  providedIn: 'root'
})
export class IsCreatorGuard implements CanActivate {
  constructor(private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      console.log(next.parent);

      const account = next.parent.data["account"] as User;
      if (!!account && account.is_creator) {
        return true;
      }
      this.router.navigate(["/account", "advertising-dashboard"]);
      return false;
  }
}
