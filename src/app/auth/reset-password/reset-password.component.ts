import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/services/account/account.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { CommonValidators } from 'src/app/shared/validators/common.validators';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  constructor(private accountService: AccountService, private toastrService: ToastrService, private formBuilder: FormBuilder,
    private router: Router, private authService: AuthService, private activatedRoute: ActivatedRoute) { }

  matFormFieldType: string = "fill";
  newPasswordForm: FormGroup;
  passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/;
  token: string;
  userId: number;
  
  get newPassword(): AbstractControl {
    return this.newPasswordForm.get('password');
  }
  get newPasswordConfirm(): AbstractControl {
    return this.newPasswordForm.get('password_confirm');
  }

  ngOnInit() {

    this.newPasswordForm = this.formBuilder.group({
      token: "",
      new_password: ['', [
        Validators.required,
        Validators.pattern(this.passwordRegex)
      ]],
      new_password_confirm: ['', [
        Validators.required, CommonValidators.samePasswordValidator('new_password'),
        Validators.pattern(this.passwordRegex)
      ]],
    });

    this.activatedRoute.params.subscribe(params => {
      this.token = params['token'];
      this.newPasswordForm.patchValue({token: this.token});
    });
  }

  // showAlert(name?: string, alert?: any): void {
  //   this.incorrectPasswordAlert = false;
  //   this.createdAccountAlert = false;
  //   this.registerErrorAlert = false;
  //   this.inactiveMailAlert = false;
  //   this.passwordResetMailAlert = false;

  //   this[name] = true;
  //   if (alert) {
  //     alert = true;
  //   }
  // }

  // hideAlert(name?: string) {
  //   this.incorrectPasswordAlert = false;
  //   this.createdAccountAlert = false;
  //   this.registerErrorAlert = false;
  //   this.inactiveMailAlert = false;
  //   this.passwordResetMailAlert = false;
  // }

  hasError(formControlName, errorName) {
    if (!isNullOrUndefined(formControlName)) {
      return formControlName.errors[errorName];
    }
    return null;
  }



  changePassword() {
    if (this.newPasswordForm.invalid) {
      this.toastrService.error('Les mots de passe ne correspondent pas');
      return;
    }
    this.accountService.resetPasswordByToken(this.newPasswordForm.value).subscribe(res => {
      this.toastrService.success('Mot de passe modifié avec succès');
    }, err => {
      this.toastrService.warning('Impossible de modifier votre mot de passe');
    });
  }

}
