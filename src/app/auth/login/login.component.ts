import { Component, OnInit } from "@angular/core";
import { AccountService } from "src/app/services/account/account.service";
import { ToastrService } from "ngx-toastr";
import { FormBuilder, Validators, FormGroup, AbstractControl } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthService } from "src/app/services/auth/auth.service";
import { CommonValidators } from "src/app/shared/validators/common.validators";
import { isNullOrUndefined } from "util";
import { User } from "src/app/models/models";

interface Form {
  registerPasswordConfirm;
}


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private accountService: AccountService, private toastrService: ToastrService, formBuilder: FormBuilder,
    private router: Router, private authService: AuthService, private activatedRoute: ActivatedRoute) {
    if (this.authService.isAuthenticated()) {
      this.router.navigate(['account']);
    } else {
      this.authService.logout();
    }



    this.loginForm = formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      remember_me: [false, Validators.required],
    });

  }

  submitted = false;

  loginForm: FormGroup;

  matFormFieldType: string = "fill";

  passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/;

  token: string;
  userId: number;

  hideForm = false;  
  requestSent = false;
  invalidRequest = false;

  public mailSended = false;
  isResetCardShowed = false;
  isRegisterCardShowed = false;
  isLoginCardShowed = false;
  isResetPasswordCardShowed = false;

  public createdAccountAlert = false;
  public registerErrorAlert = false;
  public incorrectPasswordAlert = false;
  public inactiveMailAlert = false;

  // registerUsername;


  get loginEmail(): AbstractControl {
    return this.loginForm.get('email');
  }
  get loginPassword(): AbstractControl {
    return this.loginForm.get('password');
  }
  get remember_me(): AbstractControl {
    return this.loginForm.get('remember_me');
  }


  ngOnInit() {
    
  }

  async login() {
    await this.router.navigate(['/account']);

    this.submitted = true;
    if (this.loginForm.valid) {

      this.hideForm = true;

      const toSend = this.loginForm.value;
      try {
        const accessToken = await this.accountService.login(toSend).toPromise() as any as AccessTokenDTO;
        this.authService.setAccessToken(accessToken.token);
        this.router.navigate(['/account']);

      }
      catch (e) {
        console.log(e);
        this.hideForm = false;

        this.toastrService.error('Impossible de se connecter');
        if ([401, 403].includes(e.status)) {
          this.showAlert('inactiveMailAlert');
          this.toastrService.info('Pour vous connecter, veuillez activer votre compte');
        } else {
          this.showAlert('incorrectPasswordAlert');
        }
      }
    } 

  }

  showAlert(name?: string, alert?: any): void {
    this.incorrectPasswordAlert = false;
    this.createdAccountAlert = false;
    this.registerErrorAlert = false;
    this.inactiveMailAlert = false;

    this[name] = true;
    if (alert) {
      alert = true;
    }
  }

  hideAlert(name?: string) {
    this.incorrectPasswordAlert = false;
    this.createdAccountAlert = false;
    this.registerErrorAlert = false;
    this.inactiveMailAlert = false;
  }

  resendActivationMail() {
    this.toastrService.info('Email d\'activation envoyé à ' + this.loginEmail.value);
    this.accountService.resendActivationEmail({ email: this.loginEmail.value }).subscribe((res: any) => {
      this.showAlert('inactiveMailAlert');
    }, err => {
      if (err.status === 404) {
        this.toastrService.error(`L'email utilisé n'est associé à aucun compte`);
      }
    });
  }

  hasError(formControlName, errorName) {
    if (!isNullOrUndefined(formControlName)) {
      return !!formControlName.errors && formControlName.errors[errorName];
    }
    return null;
  }

}
