import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AccountService } from 'src/app/services/account/account.service';

@Component({
  selector: 'app-activate-account',
  templateUrl: './activate-account.component.html',
  styleUrls: ['./activate-account.component.scss']
})
export class ActivateAccountComponent implements OnInit {

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private accountService: AccountService) {

  }

  timer = 5;
  isActivated = false;
  message = 'Token non valide, expiré ou déjà utilisé';
  showMessage = false;

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      const user = params['user'];
      const token = params['token'];
      setTimeout(() => {
        this.accountService.activateAccount(user, token).subscribe(res => {
          this.isActivated = true;
          this.startTimer();
        }, err => {
          this.showMessage = true;
          // this.redirect();
        });
      }, 2000);
    });
  }

  startTimer() {
    setInterval(() => {
      if (this.timer === 0) {
        this.redirect();
        return;
      }
      this.timer -= 1;
    }, 1000);
  }

  redirect() {
    this.router.navigate(["/auth", 'login']);
  }

}
