import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/services/account/account.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, AbstractControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-reset-password-request',
  templateUrl: './reset-password-request.component.html',
  styleUrls: ['./reset-password-request.component.scss']
})
export class ResetPasswordRequestComponent implements OnInit {

  constructor(private accountService: AccountService, private toastrService: ToastrService, private formBuilder: FormBuilder,
    private router: Router, private authService: AuthService, private activatedRoute: ActivatedRoute) { }

    resetPasswordForm: FormGroup;
    public passwordResetMailAlert = false;

    get resetEmail(): AbstractControl {
      return this.resetPasswordForm.get('email');
    }

    matFormFieldType: string = "fill";


  ngOnInit() {

    this.resetPasswordForm = this.formBuilder.group({
      email: ['', Validators.required],
    });

  }

  resetPassword() {
    this.accountService.resetPassword({ email: this.resetEmail.value }).subscribe((res: any) => {
      this.showAlert('passwordResetMailAlert');
      this.toastrService.success('Vous devriez recevoir sous peu un email pour changer votre de passe');
    }, err => {
      this.toastrService.warning('Une erreur est survenue');
    });
  }

  showAlert(name?: string, alert?: any): void {
    this.passwordResetMailAlert = false;

    this[name] = true;
    if (alert) {
      alert = true;
    }
  }

  hideAlert(name?: string) {
    this.passwordResetMailAlert = false;
  }

  hasError(formControlName, errorName) {
    if (!isNullOrUndefined(formControlName)) {
      return formControlName.errors[errorName];
    }
    return null;
  }

}
