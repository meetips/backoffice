import { NgModule } from '@angular/core';
import { Routes, RouterModule, UrlSegment, Route, UrlSegmentGroup } from '@angular/router';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ResetPasswordRequestComponent } from './reset-password-request/reset-password-request.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ActivateAccountComponent } from './activate-account/activate-account.component';

// export function urlMatcher(segments: UrlSegment[]) {
//   console.log("segments", segments);
//   const present = segments.find((value) => value.path === "register");
//   if (present) {
//     console.log('is present');

//     return ({ consumed: [...segments] })
//   }
//   return null;
// }

export function urlMatcher(list: string[]) {
  return (segments: UrlSegment[], group: UrlSegmentGroup, route: Route) => {
    // console.log("segments", segments);
    // console.log("group", group);
    // console.log("group parent", group.parent);
    // console.log("route", route);
    let res = null
    for(let elt of list) {
      group.segments.forEach((value) => {
        console.log(value, elt);
        if (value.path === elt) {
          console.log('is present');
          res = ({ consumed: null})
        } 
      });

    }
    return res;
  }
}

const routes: Routes = [
  {
    path: "", component: AuthComponent, children: [
      // { component: LoginComponent, matcher: urlMatcher(["login"]) },
      // { component: RegisterComponent, matcher: urlMatcher(['register']) },
      { path: "login", component: LoginComponent,  },
      { path: "register/:token" , component: RegisterComponent, },
      { path: "register", component: RegisterComponent,  },
      { path: "reset-password-request", component: ResetPasswordRequestComponent },
      { path: "reset-password/:token", component: ResetPasswordComponent },
      { path: "", component: LoginComponent, pathMatch: "full" },
    { path: 'activate-account/:user/:token', component: ActivateAccountComponent },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
