import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/services/account/account.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, AbstractControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { CommonValidators } from 'src/app/shared/validators/common.validators';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private accountService: AccountService, private toastrService: ToastrService, private formBuilder: FormBuilder,
    private router: Router, private authService: AuthService, private activatedRoute: ActivatedRoute) { }

  registerForm: FormGroup;

  submitted = false;
  loading = false;


  public createdAccountAlert = false;
  public registerErrorAlert = false;
  public mailSended = false;
  matFormFieldType: string = "fill";



  get registerEmail(): AbstractControl {
    return this.registerForm.get('email');
  }
  get registerUsername(): AbstractControl {
    return this.registerForm.get('username');
  }
  get registerPassword(): AbstractControl {
    return this.registerForm.get('password');
  }
  get registerPasswordConfirm(): AbstractControl {
    return this.registerForm.get('password_confirm');
  }


  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      username: ['', CommonValidators.conditionnalRequired('is_creator')],
      password: ['', [
        Validators.required,
        // CommonValidators.passwordValidator,
      ]],
      password_confirm: ['', [Validators.required,
      // CommonValidators.passwordValidator,

      CommonValidators.samePasswordValidator('password'),
      ]],
      is_creator: false,
      is_announcer: true,
    });
  }

  createAccount() {
    this.loading = true;
    this.submitted = true;
    if (this.registerForm.valid) {
      const toSend = this.registerForm.value;
      if (toSend["is_announcer"] === true) {
        delete toSend["username"];
      }
      this.toastrService.info('Votre demande de création de compte est en cours de traitement', 'Demande en cours');  // Vérifié
      this.accountService.createAccount(toSend).subscribe((res: any) => {
        this.mailSended = true;
        this.showAlert('createdAccountAlert');
        this.loading=false;

        this.toastrService.success('Compte créé avec succès. Vous allez recevoir un mail pour activer ce dernier', 'Compte créé');  // Vérfié
      }, e => {
        this.loading=false;

        this.showAlert('registerErrorAlert');
        this.toastrService.warning(`Nous n\'avons pas pu créé votre compte.
        Une erreur est survenue. Veuillez réessayer plus tard`, 'Action impossible'); // Vérifié
      });
    } else {
      console.log(this.registerForm)
      this.registerForm.updateValueAndValidity();
      this.toastrService.warning('Nous avons detecter des erreurs dans le formulaire');  // Vérifié
      this.loading=false;
    }
  }

  showAlert(name?: string, alert?: any): void {
    this.createdAccountAlert = false;
    this.registerErrorAlert = false;

    this[name] = true;
    if (alert) {
      alert = true;
    }
  }

  hideAlert(name?: string) {
    this.createdAccountAlert = false;
    this.registerErrorAlert = false;
  }

  switchUserType($event) {
      this.registerForm.get('is_creator').setValue(!$event);
      this.registerForm.get('is_announcer').setValue($event);
      this.registerForm.get('username').updateValueAndValidity();
  }

  hasError(formControlName, errorName) {
    if (!isNullOrUndefined(formControlName) && !isNullOrUndefined(formControlName.errors)) {
      return formControlName.errors[errorName];
    }
    return null;
  }
}
