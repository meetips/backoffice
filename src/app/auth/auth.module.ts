import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { ResetPasswordRequestComponent } from './reset-password-request/reset-password-request.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { SharedModule } from '../shared/shared.module';
import { AuthComponent } from './auth.component';
import { ActivateAccountComponent } from './activate-account/activate-account.component';

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    SharedModule
  ],
  declarations: [ResetPasswordRequestComponent, ResetPasswordComponent, LoginComponent, RegisterComponent, AuthComponent, ActivateAccountComponent]
})
export class AuthModule { }
