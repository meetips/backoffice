import { CommonModule, registerLocaleData } from '@angular/common';
import { HAMMER_GESTURE_CONFIG, BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { ModalRewardComponent } from './modal-reward/modal-reward.component';

import { GestureConfig, MAT_DATE_LOCALE } from '@angular/material';

import localeFr from '@angular/common/locales/fr';
import { CoreModule } from './core/core.module';
import { AuthService } from './services/auth/auth.service';
import { TokenInterceptor } from './services/auth/token.interceptor';
import { SharedModule } from './shared/shared.module';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccountResolver } from './resolvers/account.resolver';
import { RedirectResolver } from './resolvers/redirect.resolver';
import { RedirectCreatorResolver } from './resolvers/redirect-creator.resolver';
import { RedirectAnnouncerResolver } from './resolvers/redirect-announcer.resolver';


// import { ToastrModule } from 'ngx-toastr';
// import {MDCIconButtonToggleAdapter} from '@material/icon-button';

registerLocaleData(localeFr, 'fr');

@NgModule({
  declarations: [
    AppComponent,
    // CampaignComponent,
  ],
  imports: [
    // BrowserModule,
    AppRoutingModule,
    CommonModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    BrowserAnimationsModule
  ]
  ,
  providers: [
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    // {
    //   provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig
    // },
    { provide: MAT_DATE_LOCALE, useValue: 'fr-FR' },
    AccountResolver,
    RedirectResolver,
    RedirectCreatorResolver,
    RedirectAnnouncerResolver
  ],
  exports: []
  ,
  entryComponents: [],
  bootstrap: []
})
export class AppModule { }
