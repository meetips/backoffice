import { Component, OnInit, TemplateRef, AfterViewInit, ContentChild, ViewChild, ElementRef, EmbeddedViewRef, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { CommonValidators } from 'src/app/shared/validators/common.validators';
import { FileValidator } from 'src/app/shared/validators/file.validators';
import { FormGenerator } from 'src/app/shared/models/form-generator.model';
import { FieldComponentType } from 'src/app/shared/models/fields-component-type';
import { DataType } from 'src/app/shared/models/data-type';
import { RewardService } from 'src/app/services/reward/reward.service';
import { RaffleService } from 'src/app/services/raffle/raffle.service';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import { Reward } from 'src/app/models/reward.model';
import { ActivatedRoute } from '@angular/router';
import { Raffle } from 'src/app/models/raffle.model';
import { Title } from '@angular/platform-browser';
import { MenuService } from 'src/app/services/menu/menu.service';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { debounceTime, map, startWith } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-raffle-detail',
  templateUrl: './raffle-detail.component.html',
  styleUrls: ['./raffle-detail.component.css']
})
export class RaffleDetailComponent implements OnInit, AfterViewInit {

  constructor(private fb: FormBuilder,
    private rewardService: RewardService,
    private raffleService: RaffleService,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private menuService: MenuService
  ) {

  }

  separatorKeysCodes: number[] = [ENTER, COMMA];

  @ViewChild("rewardOptionTemplate", { read: TemplateRef, static: true })
  rewardOptionTemplate: TemplateRef<any>;


  raffleForm = {
    fields: [
      {
        formControlField: "name",
        label: "Nom",
        dataType: DataType.String,
        fieldComponentType: FieldComponentType.MatFormField,
        errors: {
          required: "Ce champ est requis"
        }
        
      },
      {
        formControlField: "description",
        label: "Description",
        dataType: DataType.String,
        fieldComponentType: FieldComponentType.MatFormField,
        errors: {
          required: "Ce champ est requis"
        }
      },
      {
        formControlField: "start_date",
        label: "Date de début",
        placeholder: "jj/mm/yyyy",
        dataType: DataType.Date,
        fieldComponentType: FieldComponentType.MatFormField,
        dateParams: {
          min: new Date()
        },
        inputParams: {
          type: "date"
        },
        errors: {
          required: "Ce champ est requis"
        }
      },
      {
        formControlField: "end_date",
        label: "Date de fin",
        placeholder: "jj/mm/yyyy",
        dataType: DataType.Date,
        fieldComponentType: FieldComponentType.MatFormField,
        dateParams: {
          min: new Date()
        },
        inputParams: {
          type: "date"
        },
        errors: {
          required: "Ce champ est requis"
        }
      },
      {
        formControlField: "enable",
        label: "Activer le tirage au sorts et permettre aux utilisateur de s'y inscrire",
        dataType: DataType.Boolean,
        fieldComponentType: FieldComponentType.SlideToggle,
      },
    ]
  } as FormGenerator;

  raffleFormGroup: FormGroup;
  raffle: Raffle;

  rewardControl = new FormControl;

  submitted: boolean = false;

  ngOnInit() {
    this.raffleFormGroup = this.fb.group({
      id: null,
      name: ["", [Validators.required]],
      description: ["", [Validators.required]],
      start_date: ["", [Validators.required]],
      end_date: ["", [Validators.required]],
      enable: false,
      rewards: this.fb.array([])
    });

    this.getRaffle();
    this.setTitle();


  }

  ngAfterViewInit() {
    // console.log(this.rewardsTemplate);
  }

  setTitle() {
    this.activatedRoute.params.subscribe(params => {
      let title = "Mettre à jour un tirage au sort";
      if (params.id === "new") {
        title = "Créer un nouveau tirage au sort";
      } else {
        title = "Mettre à jour un tirage au sort";
      }
      this.titleService.setTitle(title);
      this.menuService.title.next(title);
    });
  }


  execute(func, ...args) {
    return this[func](args);
  }


  async save() {
    this.submitted = true;
    if (this.raffleFormGroup.invalid) {
      return;
    }

    try {
      await this.raffleService.save(this.raffleFormGroup.value as Raffle).toPromise();
    } catch (e) {
      console.error(e);
    }
  }

  changeHandler($event, formControlName) {
    console.log($event);
    this.raffleFormGroup.get(formControlName).patchValue($event);
  }

  async getRaffle() {
    this.activatedRoute.params.subscribe(async param => {
      if (param["id"] !== "new") {
        this.raffle = await this.raffleService.getRaffle(param.id).toPromise();
        this.raffleFormGroup.patchValue(this.raffle);
      }
    });
  }

  handleRewardsChange($event) {
    this.raffleFormGroup.setControl("rewards", $event);
  }

  getTemplate(template) {
    return this[template];
  }

  selectReward() {

  }

  removeRaward() {

  }





}
