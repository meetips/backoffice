import { FormGenerator } from "src/app/shared/models/form-generator.model";
import { FieldComponentType } from "src/app/shared/models/fields-component-type";
import { DataType } from "src/app/shared/models/data-type";

export default {
    fields: [
        {
            formControlField: "name",
            label: "Nom",
            fieldComponentType: FieldComponentType.MatFormField
        },
        {
            formControlField: "description",
            label: "Description",
            fieldComponentType: FieldComponentType.MatFormField
        },
        {
            formControlField: "start_date",
            label: "Date de début",
            dateParams: {
                min: new Date()
            },
            inputParams: {
                type: "date"
            }
        },
        {
            formControlField: "end_date",
            label: "Date de fin",
            dateParams: { 
                min: new Date()
            },
            inputParams: { 
                type: "date"
            }
        },
    ]
} as FormGenerator