import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RafflesRoutingModule } from './raffles-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { RaffleDetailComponent } from './raffle-detail/raffle-detail.component';
import { RafflesComponent } from './raffles/raffles.component';
import { RewardsSelectorComponent } from './rewards-selector/rewards-selector.component';
import { RewardDetailComponent } from '../rewards/reward-detail/reward-detail.component';
import { RewardsModule } from '../rewards/rewards.module';
import { MatDialogModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    RafflesRoutingModule,
    SharedModule,
    RewardsModule,
    MatDialogModule,
  ],
  declarations: [
    RafflesComponent,
    RaffleDetailComponent,
    RewardsSelectorComponent,
  ]
})
export class RafflesModule { }
