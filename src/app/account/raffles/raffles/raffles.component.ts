import { Component, OnInit } from '@angular/core';
import { RaffleService } from 'src/app/services/raffle/raffle.service';
import { Raffle } from 'src/app/models/raffle.model';
import { MenuService } from 'src/app/services/menu/menu.service';
import { Title } from '@angular/platform-browser';
import { RaffleReward } from 'src/app/models/raffle-reward.model';

@Component({
  selector: 'app-raffles',
  templateUrl: './raffles.component.html',
  styleUrls: ['./raffles.component.css']
})
export class RafflesComponent implements OnInit {

  constructor(private raffleService: RaffleService, private titleService: Title, private menuService: MenuService) { }

  raffles: Raffle[];

  displayedColumns = [
    "name",
    "description",
    "start_date",
    "end_date",
    "enable",
    "rewards",
    "actions"
  ];

  ngOnInit() {
    this.chargeRaffles();
  }

  setTitle() {
    let title = "Mes tirages au sorts";
    this.titleService.setTitle(title);
    this.menuService.title.next(title);
  }

  async chargeRaffles() {
    this.raffles = await this.raffleService.getRaffles().toPromise();
  }

  edit(id: number) {
    
  }

}
