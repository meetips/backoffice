import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RaffleDetailComponent } from './raffle-detail/raffle-detail.component';
import { PageContainerComponent } from 'src/app/shared/components/page-container/page-container.component';
import { RafflesComponent } from './raffles/raffles.component';

const routes: Routes = [
  {
    path: "", component: PageContainerComponent,
    children: [
      { path: "", component: RafflesComponent },
      { path: ":id", component: RaffleDetailComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RafflesRoutingModule { }
