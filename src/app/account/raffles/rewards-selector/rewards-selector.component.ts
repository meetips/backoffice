import { Component, OnInit, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { FormArray, Validators, AbstractControl, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Reward } from 'src/app/models/reward.model';
import { Observable } from 'rxjs';
import { startWith, map, debounceTime } from 'rxjs/operators';
import { RewardService } from 'src/app/services/reward/reward.service';
import { ModalDialogComponent } from 'src/app/shared/components/modal-dialog/modal-dialog.component';
import { MatDialog } from '@angular/material';
import { RewardDetailComponent } from '../../rewards/reward-detail/reward-detail.component';

@Component({
  selector: 'app-rewards-selector',
  templateUrl: './rewards-selector.component.html',
  styleUrls: ['./rewards-selector.component.css']
})
export class RewardsSelectorComponent implements OnInit, OnChanges {

  constructor(private fb: FormBuilder, private rewardService: RewardService, private dialog: MatDialog,) { }

  rewardControl: AbstractControl = new FormControl();

  @Input()
  rewardsFormArray: FormArray = new FormArray([]);

  rewardsMap: { [s: number]: Reward } = {};

  selectedRewards: Reward[] = [];
  filteredRewards: Observable<Reward[]>;
  baseRewards: Reward[] = [];

  @Output()
  rewardsChange = new EventEmitter<FormArray>();


  ngOnInit() {
    this.getRewards();

    this.filteredRewards = this.rewardControl.valueChanges.pipe(
      startWith(''),
      debounceTime(1000),
      map(value => { console.log(value); return value; }),
      map(value => this.filterRewardsByName(value)),
      map(rewards => this.removeSelectedRewardsFromFiltered(rewards)),
    );

  }

  ngOnChanges() {
    this.rewardsFormArray.value.forEach(value => {
      
    })

  }

  async getRewards() {
    try {
      this.baseRewards = await this.rewardService.getRewards().toPromise();
      this.setRewardsMap(this.baseRewards);
    } catch (e) {
      console.error(e);
    }
  }

  setRewardsMap(list: Reward[]) {
    list.forEach(r => {
      this.rewardsMap[r.id] = r;
    });
  }

  selectReward(reward: Reward) {
    console.log(this.rewardsMap);

    // this.filteredRewards = this.baseRewards.filter((value) => {
    //   return value.id !== reward.id
    // });
    const rewardId = reward.id;
    this.selectedRewards.push(this.rewardsMap[rewardId]);
    this.addRewardFormGroupToRaffle(reward);
    this.rewardControl.reset('');
  }

  findRewardById(id: number) {
    return this.selectedRewards.find((reward) => {
      return reward.id === id;
    });
  }

  filterRewardsByName(value: string) {
    if (value && value !== '') {
      return this.baseRewards.filter((val) => val.name.includes(value));
    }
    return this.baseRewards;
  }

  removeSelectedRewardsFromFiltered(list: Reward[]) {
    return list.filter(reward => {
      const index = this.findRewardIndexById(this.selectedRewards, reward.id);
      if (index >= 0) {
        return false;
      } else {
        return true;
      }
    });
  }

  findRewardIndexById(list: Reward[], id: number) {
    return list.findIndex((reward) => {
      if (reward.id === id) {
        return true;
      }
      return false;
    })
  }

  addRewardFormGroupToRaffle(reward) {
    this.rewardsFormArray.push(this.fb.group({
      quantity: [1, [
        Validators.required,
        Validators.min(1),
        Validators.pattern("[0-9]{1,}")
      ]],
      reward: reward.id   
    }));
    this.rewardsChange.emit(this.rewardsFormArray);
  }

  removeRewardFromRaffle(index: number) {
    this.rewardsFormArray.removeAt(index);
    this.selectedRewards.splice(index, 1);
    this.rewardControl.reset();
    this.rewardsChange.emit(this.rewardsFormArray);
  }

  displayFn(reward?: Reward): any {
      return reward ? reward.name : undefined;
  }

  getReward(id: number): Reward {
    if (this.rewardsMap) {
      return this.rewardsMap[id];
    }
    // this.setRewardsMap();
    return this.rewardsMap[id];

  }

  showRewardModal() {
    // const dialogRef = this.dialog.open(RewardDetailComponent,  {
    //   width: '250px',
    //   data: {}
    // });
    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    // });
  }

}
