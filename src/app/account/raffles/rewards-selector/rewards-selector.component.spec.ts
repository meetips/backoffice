import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RewardsSelectorComponent } from './rewards-selector.component';

describe('RewardsSelectorComponent', () => {
  let component: RewardsSelectorComponent;
  let fixture: ComponentFixture<RewardsSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RewardsSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RewardsSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
