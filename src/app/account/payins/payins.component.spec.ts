import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayinsComponent } from './payins.component';

describe('PayinsComponent', () => {
  let component: PayinsComponent;
  let fixture: ComponentFixture<PayinsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayinsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayinsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
