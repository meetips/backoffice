import { Component, OnInit } from '@angular/core';
import { Payin } from 'src/app/models/models';
import { PayinService } from 'src/app/services/payin/payin.service';
import { MenuService } from 'src/app/services/menu/menu.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-payins',
  templateUrl: './payins.component.html',
  styleUrls: ['./payins.component.css'],
})
export class PayinsComponent implements OnInit {

  constructor(private payinService: PayinService, private menuService: MenuService, private title: Title) { 

  }

  myPayinList: Payin[] = [];
  displayedColumns = [
    'advertising_name',
    'amount',
    'status',
    'method',
    'created_at',
    'updated_at',
    'estimatedView',
    'viewCount'
  ]

  ngOnInit() {
    this.title.setTitle("Mes derniers paiements");
    this.menuService.title.next("Mes derniers paiements");
    
    this.payinService.getMyPayinList().subscribe(response => {   
      this.myPayinList = response;
    });
  }

}
