import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../services/account/account.service';
import { CampaignService } from '../../services/campaign/campaign.service';
import { PayoutService } from '../../services/payout/payout.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { ConfirmationDialogComponent } from '../../confirmation-dialog/confirmation-dialog.component';
import { Title } from '@angular/platform-browser';
import { MenuService } from 'src/app/services/menu/menu.service';
import * as moment from 'moment';
import { range } from 'rxjs';
import * as Chart from 'chart.js';
import { Campaign } from 'src/app/models/models';
import { Router } from '@angular/router';


export class SimpleChartModelPoint {
  day: string;
  number: number;
  total_number?: number;
}

class ChartColors {
  back: string;
  border: string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private campaignService: CampaignService, private payoutService: PayoutService, private titleService: Title,
    private toastrService: ToastrService, private dialog: MatDialog, private accountService: AccountService,
    private menuService: MenuService, private router: Router) {
    titleService.setTitle('Mon dashboard');
  }


  myCampaignsDataSource: any[] = [];
  displayedCampaignColumns: any[] = [
    'name', 'tips_count', 'tips_sum', 'tips_count_month', 'tips_sum_month', 'created_at', 'actions'
  ];

  displayedPayoutColumns: any[] = [
    'amount', 'status', 'method', 'created_at'
  ];

  payoutsDataSource: any[] = [];
  currentBalance = 0;
  minBalance = 45;

  colors: ChartColors[] = [
    { back: 'rgba(255, 99, 132, 0.2)', border: 'rgba(255,99,132,1)' },
    { back: 'rgba(54, 162, 235, 0.2)', border: 'rgba(54, 162, 235, 1)' },
    { back: 'rgba(75, 192, 192, 0.2)', border: 'rgba(75, 192, 192, 1)' },
    { back: 'rgba(255, 159, 64, 0.2)', border: 'rgba(255, 159, 64, 1)' },
    { back: 'rgba(215, 159, 64, 0.2)', border: 'rgba(215, 159, 64, 1)' },
  ];

  myChart: Chart;
  fromDate: any;
  toDate: any;

  currentCampaign: Campaign;
  currentReferenceProperty: string = 'count_tips_per_day';
  namedDataset: any = {};




  ngOnInit() {
    this.menuService.title.next('Mon tableau de bord');
    
    this.getMyCampaigns();
    this.getCurrentBalance();
    this.getPayouts();

    const ctx: any = document.getElementById('myChart');
    this.toDate = moment().add(1, 'day');
    this.fromDate = moment(this.toDate).add(-10, 'day');

    const days = this.getDaysFromTo(this.fromDate, this.toDate);

    this.myChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: days,
      },
      options: {
        legend: {
          display: false
        },
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
        },
        hover: {
          mode: 'nearest',
          intersect: true
        },
      }
    });

  }

  public getMyCampaigns(): any {
    this.campaignService.getCampaigns().subscribe((res: any[]) => {
      this.myCampaignsDataSource = res;
      return res;
    });
  }

  public getPayouts() {
    this.payoutService.getPayouts().subscribe((res: any[]) => {
      this.payoutsDataSource = res;
    }, err => {
      console.log(err);
    });
  }

  public getCurrentBalance() {
    this.payoutService.getCurrentBalance().subscribe((res: any) => {
      this.currentBalance = res.current_balance;
    });
  }

  getDaysFromTo(date1: Date, date2: Date) {
    const res = moment(date2).diff(moment(date1), 'days');
    const days = [];
    // tslint:disable-next-line:forin
    range(0, res).forEach((value) => {
      days.push(moment(date1).add(value, 'day').format('DD/MM/YYYY'));
    });
    return days;
  }

  edit(id: number) {
    this.router.navigate(['/account', 'campaign', id]);
  }


  async viewGraph(id: number) {
    let points;
    this.currentCampaign = this.myCampaignsDataSource.find((value) => value.id === id);
    if (!this.namedDataset.hasOwnProperty(id)) {
      await this.campaignService.getDetailByDay(
        id,
        moment(this.fromDate).format('DD-MM-YYYY'),
        moment(this.toDate).format('DD-MM-YYYY'),
      ).subscribe((campaign: Campaign[]) => {
        const days = this.getDaysFromTo(this.fromDate, this.toDate);
        points = this.getPoints(this.currentCampaign, campaign, days, this.currentReferenceProperty);
        this.currentCampaign = points;
        this.setMyChartDataset(points);
      });
    } else {
      points = this.namedDataset[id];
      this.setMyChartDataset(points);
    }

  }

  setMyChartDataset(campaign: Campaign) {
    this.myChart.data.datasets = [{
      label: campaign.name,
      backgroundColor: this.colors[0].back,
      borderColor: this.colors[0].border,
      data: campaign.points.map(e => e.number),
      fill: false,
    }];
    this.myChart.data.labels = campaign.points.map(e => e.day);
    this.myChart.update(1500);
    console.log(this.myChart);
  }

  getPoints(currentCampaign: Campaign, campaign: Campaign[], days: string[], referenceName: string) {
    const points = [];

    if (campaign.length > 0) {
      days.forEach((day) => {
        const a = campaign.find((value) => {
          return day === moment(value.day).format('DD/MM/YYYY');
        });
        if (!a) {
          points.push({
            day: day,
            number: 0
          });
        } else {
          points.push({
            day: day,
            number: a[referenceName]
          });
        }
      });
      const r = { ...campaign[0], points: points };
      return r

    } else {
      days.forEach((day) => {
        points.push({
          day: day,
          number: 0
        });

      });
      return { ...currentCampaign, points: points };
    }

  }


}
