import { Component, OnInit, ViewChild, ElementRef, OnChanges } from '@angular/core';
import { Advertising, Step1, Step2, Step3, Step4, Step, Category } from '../../models/models';
import { MatDialog, MatChipInputEvent, MatAutocompleteSelectedEvent } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Title, DomSanitizer } from '@angular/platform-browser';
import { ConfirmationDialogComponent } from '../../confirmation-dialog/confirmation-dialog.component';
import { AdvertisingService } from '../../services/advertising/advertising.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { map, startWith } from 'rxjs/operators';
import { FormControl, Validators, FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { Observable, range, PartialObserver } from 'rxjs';
import { isNullOrUndefined, isArray, isObject } from 'util';
import { MenuService } from 'src/app/services/menu/menu.service';
import { YoutubeApiService } from 'src/app/services/youtube-api/youtube-api.service';
import * as moment from 'moment';
import { HttpClient } from '@angular/common/http';
import { CommonValidators } from 'src/app/shared/validators/common.validators';
import { FileValidator } from 'src/app/shared/validators/file.validators';
import { ParameterService } from 'src/app/services/parameter/parameter.service';
import { Parameter } from 'src/app/shared/models/parameter.models';
import { AdvertisingForm } from './advertising.form-group';
import { FileInfo } from 'src/app/shared/models/file-info.model';
import { FileUpload } from 'src/app/shared/models/file-upload.model';


@Component({
  selector: 'app-advertising',
  templateUrl: './advertising.component.html',
  styleUrls: ['./advertising.component.scss']
})
export class AdvertisingComponent extends AdvertisingForm implements OnInit {
  successCreated: boolean;
  saveErrors: any;
  existingModel: any;

  constructor(public dialog: MatDialog, private titleService: Title, private router: Router,
    private toastrService: ToastrService, private advertisingService: AdvertisingService,
    private sanitizer: DomSanitizer, private menuService: MenuService, private youtubeApi: YoutubeApiService,
    private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private http: HttpClient,
    private parameterService: ParameterService
  ) {
    super();
    range(this.min_cpv * 100, this.max_cpv * 100).subscribe(i =>
      this.cpvRange.push(Number.parseFloat(i.toFixed(2)) / 100)
    );

    this.menuService.title.next('Création d\'une nouvelle annonce');
  }

  model: Advertising = {};
  formGroup: FormGroup = new FormGroup({});

  matFormFieldType = "fill";

  isNew = true;
  isUpdate = false;

  cpvRange: number[] = [];

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = false;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  tagCtrl = new FormControl();
  filteredTags: Observable<string[]>;
  min_cpv = 0.010;
  max_cpv = 0.35;
  videoId = null;
  embededVideo = null;
  selectedCategory: number[] = [];
  costPerSecond: number;
  maxThumbnailSize = 2457600;
  maxVideoSize: number;
  baseCPV;

  @ViewChild('tagInput', { static: false }) tagInput: ElementRef<HTMLInputElement>;

  startAtDate = null;
  step: number = null;


  ngOnInit() {

    this.createFormGroup();
    this.initializeParemeters();
    this.initializeFileParameter();

    const id = this.activatedRoute.snapshot.params['id'];
    if (!!id) {
      this.chargeAdvertising(id);
    }

    this.step = 1;
  }

  initializeParemeters() {
    this.parameterService.getParameterByName<Parameter>(
      ParameterService.PARAMETERS.COST_PER_SECOND)
      .subscribe(value => this.costPerSecond = value.value_decimal);
    this.parameterService.getParameterByName<Parameter>(
      ParameterService.PARAMETERS.BASE_CPV).subscribe(value => this.baseCPV = value.value_decimal);
  }

  async initializeFileParameter() {
    this.maxThumbnailSize = (await this.parameterService
      .getParameterByName<Parameter>(ParameterService.PARAMETERS.MAX_ADVERTISING_THUMBNAIL_SIZE)
      .toPromise()).value_integer;
    this.maxVideoSize = (await this.parameterService
      .getParameterByName<Parameter>(ParameterService.PARAMETERS.MAX_ADVERTISING_VIDEO_SIZE)
      .toPromise()).value_integer;

    const forbiddenNames = (await this.advertisingService.getUsedNames().toPromise());



    this.video_media.get("file").setValidators(
      [
        FileValidator.maxSizeValidator(this.maxVideoSize),
        Validators.required,
        FileValidator.fileFormatValidator("video")
      ]
    );

    this.thumbnail_media.get("file").setValidators(
      [FileValidator.maxSizeValidator(this.maxThumbnailSize),
      Validators.required,
      FileValidator.fileFormatValidator("video")]

    );

    this.name.setValidators([
      Validators.required, CommonValidators.forbiddenNames(forbiddenNames)
    ])
  }


  createFormGroup() {
    this.formGroup = this.formBuilder.group({
      name: ['', Validators.required, CommonValidators.forbiddenNames([])],
      enable_start_at: [false],
      enable_end_at: [false],
      start_at: ['', CommonValidators.conditionnalRequired('enable_start_at')],
      end_at: ['', CommonValidators.conditionnalRequired('enable_end_at')],
      cost_per_view: ['', Validators.required],
      target_view: [1000, Validators.required],
      redirect_url: ['', Validators.required],
      video_media: this.formBuilder.group({
        id: [""],
        file: ["",
          [Validators.required],
        ],
        name: [""],
        localeFile: [false]

      }),
      thumbnail_media: this.formBuilder.group({
        id: [""],
        file: ["",
          [Validators.required],
        ],
        name: [""],
        localeFile: [false]

      }),
      target_view_per_day: [false, Validators.required],
      categories: [[], [Validators.required, CommonValidators.minListLength(1)]],
      audiences: [[], [Validators.required, CommonValidators.minListLength(1)]],
      countries: [['fr'], [Validators.required, CommonValidators.minListLength(1)]],
      languages: [['fr-FR'], [Validators.required, CommonValidators.minListLength(1)]],
      demographic_datas: [[], [Validators.required, CommonValidators.minListLength(1)]],
    });

  }

  chargeAdvertising(id) {
    this.advertisingService.getAdvertising(id).subscribe((response: Advertising) => {
      this.model = response;
      this.formGroup.patchValue(response);
      this.model.enable_start_at = !!response.start_at;
      this.model.enable_end_at = !!response.end_at;
      this.video_media.get("file").setValue(this.advertisingService.getFileUrl(this.video_media.get("file").value));
      this.thumbnail_media.get("file").setValue(this.advertisingService.getFileUrl(this.thumbnail_media.get("file").value));
      this.enable_start_at.setValue(!!response.start_at);
      this.enable_end_at.setValue(!!response.end_at);
      this.categories.setValue(response.categories.map(elt => elt.id));
      this.audiences.setValue(response.audiences.map(elt => elt.id));
      this.countries.setValue(response.countries.map(elt => elt.locale));
      this.languages.setValue(response.languages.map(elt => elt.locale));
      this.demographic_datas.setValue(response.demographic_datas.map(elt => elt.id));
    });
  }

  change() {
    this.startAtDate = new FormControl(this.model.start_at);
  }

  nextStep($event) {
    $event.preventDefault();
    this.step++;
  }

  hasStarted() {
    if (!this.isNew) {
      const today = moment();
      if (!this.model.start_at) {
        if (today.isSameOrBefore(moment(this.model.start_at))) {
          return true;
        }
      }
    }
    return false;
  }

  showModal() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  continu() {
    if (!isNullOrUndefined(this.formGroup.get("id").value) && this.formGroup.get("id").value !== "") {
      this.save();
    }
    else {
      this.update();
    }
  }


  getFormData(toSend): FormData {

    const formData = new FormData();
    for (const property of Object.keys(toSend)) {
      const value = toSend[property];
      if (!isNullOrUndefined(value)) {
        if (isArray(value)) {
          value.forEach(element => {
            formData.append(property, element);
          });
        } else if (isObject(value)) {
          for (const dataProp of Object.keys(value)) {
            if (!isNullOrUndefined(value[dataProp])) {
              formData.append(`${property}.${dataProp}`, value[dataProp]);
            }
          }
        } else {
          formData.append(property, value);
        }
      }
    }
    return formData;
  }

  save() {

    if (this.formGroup.invalid) {
      this.markControlsDirty(this.formGroup);
      return;
    }

    const toSend = Object.assign({}, this.formGroup.value);
    if (!!toSend.start_at) {
      toSend.start_at = moment(toSend.start_at).format('YYYY-MM-DD');
    }
    if (!!toSend.end_at) {
      toSend.end_at = moment(toSend.end_at).format('YYYY-MM-DD');
    }
    const formData: FormData = this.getFormData(toSend);
    this.advertisingService.createAdvertising(formData).subscribe((res: any) => {
      if (res.id) {
        this.model.id = res.id;
      }
      this.successCreated = true;
      this.toastrService.success('La création de votre Campagne a réussi.', 'Tous est bon');
      this.router.navigate(['order', this.model.id]);
    }, (err: any) => {
      this.toastrService.error('Une erreur est survenue lors de la création de votre campagne', 'Oups !');
      this.successCreated = false;
      this.saveErrors = err;
    });

  }

  update() {
    // const toSend = this.model;
    const toSend: Advertising = {};
    for (const item in this.model) {
      if (this.model.hasOwnProperty(item)) {
        if (this.model[item] !== this.existingModel[item]) {
          toSend[item] = this.model[item];
        }
      }
    }
    toSend['id'] = this.existingModel.id;
    if (!!toSend.start_at) {
      toSend.start_at = moment(toSend.start_at).format('YYYY-MM-DD');
    }
    if (!!toSend.end_at) {
      toSend.end_at = moment(toSend.end_at).format('YYYY-MM-DD');
    }
    const formData = this.getFormData(toSend);
    // this.addImages(formData);
    this.advertisingService.updatePatialCampaign(formData).subscribe(res => {
      this.successCreated = true;
      this.toastrService.success('Campagne mise à jour avec succès');
      this.existingModel = Object.assign({}, res);
    }, (err: any) => {
      this.successCreated = false;
      this.saveErrors = err;
      this.toastrService.error('La campagne n\'a pas pu être mise à jour');
    });
  }

  markControlsDirty(formGroup: FormGroup): void {
    for (let controlName in formGroup.controls) {
      const control = formGroup.get(controlName);
      if (control instanceof FormGroup) {
        this.markControlsDirty(control);
      } else {
        control.markAsDirty();
        control.markAsTouched();
        control.updateValueAndValidity();
      }
    }
    formGroup.updateValueAndValidity();
  }

  enableStartAt(event) {
    this.model.enable_start_at = event;
    this.enable_start_at.setValue(event);
    this.start_at.updateValueAndValidity();
  }

  enableEndAt(event) {
    this.model.enable_end_at = event;
    this.enable_end_at.setValue(event);
    this.start_at.updateValueAndValidity();
    this.end_at.updateValueAndValidity();

  }


  selectCategory(categories: number[]) {
    this.selectedCategory = categories;
    this.categories.setValue(categories);
  }

  selectAudience(audiences: number[]) {
    // this.selectedCategory = audiences;
    this.audiences.setValue(audiences)
  }

  getMaxCost() {
    return this.model.cost_per_view * this.model.target_view;
  }

  async getMinCPV() {
    const cost = await this.parameterService.getParameterByName<Number>(ParameterService.PARAMETERS.COST_PER_SECOND).toPromise()
  }

  fileUploadChange(fileUpload: FileUpload, formControlName) {
    if (formControlName === "video_media") {
      this.video_media.patchValue(fileUpload);
    } else if (formControlName === "thumbnail_media") {
      this.thumbnail_media.patchValue(fileUpload);
    }
  }

  changeTargetViewType(event) {
    this.target_view_per_day.setValue(event);
  }

  hasErrors(...formControlName) {
    for (const item of formControlName) {
      let item_ = item as AbstractControl;
      if (!isNullOrUndefined(item_) && item_.dirty && item_.invalid && item_.touched) {
        return true;
      }
    }
    return false;
  }

  updateSelectedLanguages($event) {
    this.languages.setValue($event);
  }

  updateSelectedCountries($event) {
    this.countries.setValue($event);
  }

  selectedDemographicDataChange($event) {
    this.demographic_datas.setValue($event);
  }

  fileInfoChange(fileInfo: FileInfo) {
    if (fileInfo.fileType === "video") {
      const min_cpv = this.costPerSecond * fileInfo.duration;
      if (this.min_cpv < min_cpv) {
        this.cpvRange = [];
        range(min_cpv * 100, this.max_cpv * 100).subscribe(i => {
          const fixed = (i / 100).toFixed(2);
          const floatNumber = Number.parseFloat(Number.parseFloat(fixed).toPrecision(3));
          this.cpvRange.push(floatNumber);
        });
      }
    }

  }

}
