import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { CommonValidators } from "src/app/shared/validators/common.validators";
import { FileValidator } from "src/app/shared/validators/file.validators";

export class AdvertisingForm {

    formGroup: FormGroup;

    constructor() { }

    get name() {
        return this.formGroup!.get('name');
    }
    get start_at() {
        return this.formGroup!.get('start_at');
    }
    get end_at() {
        return this.formGroup!.get('end_at');
    }
    get cost_per_view() {
        return this.formGroup.get('cost_per_view');
    }
    get target_view() {
        return this.formGroup.get('target_view');
    }
    get redirect_url() {
        return this.formGroup.get('redirect_url');
    }
    get categories() {
        return this.formGroup.get('categories');
    }
    get video_media() {
        return this.formGroup.get('video_media') as FormGroup;
    }
    get thumbnail_media() {
        return this.formGroup.get('thumbnail_media') as FormGroup;
    }
    get target_view_per_day() {
        return this.formGroup.get('target_view_per_day');
    }
    get enable_start_at() {
        return this.formGroup.get('enable_start_at');
    }
    get enable_end_at() {
        return this.formGroup.get('enable_end_at');
    }
    get demographic_datas() {
        return this.formGroup.get('demographic_datas');
    }
    get audiences() {
        return this.formGroup.get('audiences');
    }
    get countries() {
        return this.formGroup.get('countries');
    }
    get languages() {
        return this.formGroup.get('languages');
    }


}