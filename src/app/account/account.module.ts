import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountRoutingModule } from './account-routing.module';
import { SharedModule } from '../shared/shared.module';
import { AdvertisingComponent } from './advertising/advertising.component';
import { AdvertisingDashboardComponent } from './advertising-dashboard/advertising-dashboard.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PasswordComponent } from './password/password.component';
import { OrderComponent } from './order/order.component';
import { PayinsComponent } from './payins/payins.component';
import { PayoutComponent } from './payout/payout.component';
import { ProfileComponent } from './profile/profile.component';
import { AccountComponent } from './account.component';
import { AccountCampaignComponent } from './campaign/campaign.component';
import { CoreModule } from '../core/core.module';
import { PayoutProfileComponent } from './payout-profile/payout-profile.component';
// import { RewardsComponent } from './rewards/rewards.component';
// import { RaffleDetailComponent } from './raffle-detail/raffle-detail.component';
// import { RafflesComponent } from './raffles/raffles.component';

@NgModule({
  imports: [
    CommonModule,
    AccountRoutingModule,
    SharedModule,
    CoreModule,
  ],
  declarations: [
    AccountComponent,
    AccountCampaignComponent,
    AdvertisingComponent,
    AdvertisingDashboardComponent,
    DashboardComponent,
    PasswordComponent,
    OrderComponent,
    PayinsComponent,
    PayoutComponent,
    ProfileComponent,
    // RewardComponent,
    PayoutProfileComponent,
    // RewardsComponent,
    // RaffleDetailComponent,
    // RafflesComponent,
  ]
})
export class AccountModule { }
