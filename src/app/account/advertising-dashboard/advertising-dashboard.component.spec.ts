import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertisingDashboardComponent } from './advertising-dashboard.component';

describe('AdvertisingDashboardComponent', () => {
  let component: AdvertisingDashboardComponent;
  let fixture: ComponentFixture<AdvertisingDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertisingDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertisingDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
