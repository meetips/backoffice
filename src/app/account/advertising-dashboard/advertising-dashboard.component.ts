import { Component, OnInit } from '@angular/core';
import * as CanvasJS from 'canvasjs';
import * as Chart from 'chart.js';
import { AdvertisingService } from 'src/app/services/advertising/advertising.service';
import { Advertising, AdvertisingForChart } from 'src/app/models/models';
import * as moment from 'moment';
import { MenuService } from 'src/app/services/menu/menu.service';
import { log } from 'util';
import { range } from 'rxjs';
import { Router } from '@angular/router';

export class SimpleChartModelPoint {
    day: string;
    number: number;
    total_number?: number;
}

class ChartColors {
    back: string;
    border: string;
}

class AdvertisingByDay {
    public day?: string;
    public count_tips_total?: number;
    public count_tips_per_day?: number;
    public sum_cost_total?: number;
    public sum_cost_per_day?: number;
    public avg_cost_per_view?: number;
}

class AdvertisingDayList {

}

@Component({
    selector: 'app-advertising-dashboard',
    templateUrl: './advertising-dashboard.component.html',
    styleUrls: ['./advertising-dashboard.component.scss']
})
export class AdvertisingDashboardComponent implements OnInit {


    constructor(private advertisingService: AdvertisingService,
        private menuService: MenuService,
        private router: Router) {
        menuService.title.next('Tableau de bord de mes annonces');
    }

    dataSource: Advertising[] = [];
    displayedColumns = [
        'is_active',
        'name',
        'is_approuved',
        // 'budget',
        'target_view',
        'cost_per_view',
        'count_impressions_total',
        'count_tips_total',
        'view_rate',
        'avg_cost_per_view',
        'sum_cost_total',
        'count_clicked_total',
        'cost_per_click',
        'conversion_rate',
        'star'
    ];

    colors: ChartColors[] = [
        { back: 'rgba(255, 99, 132, 0.2)', border: 'rgba(255,99,132,1)' },
        { back: 'rgba(54, 162, 235, 0.2)', border: 'rgba(54, 162, 235, 1)' },
        { back: 'rgba(75, 192, 192, 0.2)', border: 'rgba(75, 192, 192, 1)' },
        { back: 'rgba(255, 159, 64, 0.2)', border: 'rgba(255, 159, 64, 1)' },
        { back: 'rgba(215, 159, 64, 0.2)', border: 'rgba(215, 159, 64, 1)' },
    ];

    possibleReferencePropertyList: any = {
        'count_tips_per_day': 'Nombre de vue par jour',
        'sum_cost_per_day': 'Couts des vues par jour'
    }

    myChart: Chart;
    advertisingsForChart: { [s: number]: AdvertisingForChart } = [];

    namedDataset: any = {};
    fromDate: any;
    toDate: any;

    currentReferenceProperty: string = 'count_tips_per_day';
    currentAdvertising: Advertising = new Advertising();


    ngOnInit() {
        this.menuService.title.next('Tableau de bord de mes annonces');
        
        this.advertisingService.getAdvertisings().subscribe((response: Advertising[]) => {
            this.dataSource = response;
        });
        
        const ctx: any = document.getElementById('myChart');
        this.toDate = moment().add(1, 'day');
        this.fromDate = moment(this.toDate).add(-10, 'day');

        const days = this.getDaysFromTo(this.fromDate, this.toDate);

        this.myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: days,
            },
            options: {
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
            }
        });

    }


    creditMyBalance(id: number) {
        this.router.navigate(['/account/order', id, 0]);
    }

    async viewGraph(id: number) {
        let points;
        this.currentAdvertising = this.dataSource.find((value) => value.id === id);
        if (!this.namedDataset.hasOwnProperty(id)) {
            await this.advertisingService.getDetailByDate(
                id,
                moment(this.fromDate).format('DD-MM-YYYY'),
                moment(this.toDate).format('DD-MM-YYYY'),
            ).subscribe((advertising: Advertising[]) => {
                const days = this.getDaysFromTo(this.fromDate, this.toDate);
                points = this.getPoints(this.currentAdvertising, advertising, days, this.currentReferenceProperty);
                this.currentAdvertising = points;
                this.setMyChartDataset(points);
            });
        } else {
            points = this.namedDataset[id];
            this.setMyChartDataset(points);
        }

    }

    setMyChartDataset(advertising: Advertising) {
        this.myChart.data.datasets = [{
            label: advertising.name,
            backgroundColor: this.colors[0].back,
            borderColor: this.colors[0].border,
            data: advertising.points.map(e => e.number),
            fill: false,
        }];
        this.myChart.data.labels = advertising.points.map(e => e.day);
        this.myChart.update(1500);
    }

    reloadGraph() {
        if (!!this.currentAdvertising) {
            this.advertisingService.getDetailByDate(
                this.currentAdvertising.id,
                moment(this.fromDate).format('DD-MM-YYYY'),
                moment(this.toDate).format('DD-MM-YYYY'),
            ).subscribe((advertising: Advertising[]) => {
                const days = this.getDaysFromTo(this.fromDate, this.toDate);
                const points = this.getPoints(this.currentAdvertising, advertising, days, this.currentReferenceProperty);
                this.setMyChartDataset(points);
                this.currentAdvertising = points;
            });
        }
    }

    computeWholeData(colName: string): number {
        let total = 0;
        this.dataSource.forEach((element: Advertising) => {
            if (!!element[colName]) {
                total += element[colName];
            }
        });
        return total;
    }

    computeAvgData(colName: string): number {
        let avg = 0;
        this.dataSource.forEach((element: Advertising) => {
            if (!!element[colName]) {
                avg += element[colName];
            }
        });
        return avg / this.dataSource.length;
    }

    computeRateData(colName1: string, colName2: string): number {
        let sum1 = 0;
        let sum2 = 0;
        this.dataSource.forEach((element: Advertising) => {
            if (!!element[colName1] && element[colName2]) {
                sum1 += element[colName1];
                sum2 += element[colName2];
            }
        });
        return sum1 / sum2;
    }

    getDaysFromTo(date1: Date, date2: Date) {
        const res = moment(date2).diff(moment(date1), 'days');
        const days = [];
        // tslint:disable-next-line:forin
        range(0, res).forEach((value) => {
            days.push(moment(date1).add(value, 'day').format('DD/MM/YYYY'));
        });
        return days;
    }

    getPoints(currentAdvertising: Advertising, advertising: Advertising[], days: string[], referenceName: string) {
        const points = [];

        if (advertising.length > 0) {
            days.forEach((day) => {
                const a = advertising.find((value) => {
                    return day === moment(value.day).format('DD/MM/YYYY');
                });
                if (!a) {
                    points.push({
                        day: day,
                        number: 0
                    });
                } else {
                    points.push({
                        day: day,
                        number: a[referenceName]
                    });
                }
            });
            const r = { ...advertising[0], points: points };
            return r

        } else {
            days.forEach((day) => {
                points.push({
                    day: day,
                    number: 0
                });

            });
            return { ...currentAdvertising, points: points };
        }

    }

    hasStarted(advertising: Advertising) {
        const today = moment();
        if (moment(advertising.start_at) <= today) {
            return true;
        }
        return false;
    }

    countDayToToday(advertising: Advertising) {
        const today = moment();
        if (!!advertising.start_at) {

            if (moment(advertising.start_at).isSameOrAfter(today)) {
                const diff = moment(advertising.start_at).diff(today, 'days');
                return diff;
            } else {
                return moment(today).diff(advertising.start_at, 'days');
            }
        }
        else {
            return null;
        }
    }

    countDayToEnd(advertising: Advertising) {
        const today = moment();
        if (!!advertising.end_at) {
            if (moment(advertising.end_at).isSameOrBefore(today)) {
                return moment(today).diff(advertising.end_at, 'days') + 1;
            } else {
                return moment(advertising.end_at).diff(today, 'days') + 1;
            }
        } else {
            return null;
        }
    }

    countDayToStart(advertising: Advertising) {
        const today = moment();
        return moment(advertising.start_at).diff(today, 'days');
    }

    activate() {

    }

    hasNoRows(displayedColumns?) {
        if (this.dataSource.length >= 1) {
            return [];
        } else {
            if (!displayedColumns) {
                return ['hasNoRows'];
            }
            else {
                return [];
            }
        }
    }

    edit(id: number) {
        this.router.navigate(['account', 'advertising', id]);
    }

    detail(id: number) {
    }

    goToAdvertising() {
        this.router.navigate(['account', 'advertising']);
    }



}
