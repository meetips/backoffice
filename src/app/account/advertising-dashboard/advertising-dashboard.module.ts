import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdvertisingDashboardRoutingModule } from './advertising-dashboard-routing.module';

@NgModule({
  imports: [
    CommonModule,
    AdvertisingDashboardRoutingModule
  ],
  declarations: []
})
export class AdvertisingDashboardModule { }
