import { AdvertisingDashboardModule } from './advertising-dashboard.module';

describe('AdvertisingDashboardModule', () => {
  let advertisingDashboardModule: AdvertisingDashboardModule;

  beforeEach(() => {
    advertisingDashboardModule = new AdvertisingDashboardModule();
  });

  it('should create an instance', () => {
    expect(advertisingDashboardModule).toBeTruthy();
  });
});
