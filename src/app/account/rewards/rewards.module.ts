import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RewardsRoutingModule } from './rewards-routing.module';
import { RewardDetailComponent } from './reward-detail/reward-detail.component';
import { PageContainerComponent } from 'src/app/shared/components/page-container/page-container.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { RewardsComponent } from './rewards/rewards.component';

@NgModule({
  imports: [
    CommonModule,
    RewardsRoutingModule,
    SharedModule,
  ],
  declarations: [
    RewardsComponent,
    RewardDetailComponent
  ],
  entryComponents: [
    PageContainerComponent,
    RewardsComponent,
    RewardDetailComponent
  ],
  exports: [
    RewardDetailComponent
  ]
})
export class RewardsModule { }
