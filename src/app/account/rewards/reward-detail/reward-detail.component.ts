import { Component, OnInit, Inject } from '@angular/core';
import { FormGenerator } from 'src/app/shared/models/form-generator.model';
import rewardForm from './reward-form';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RewardService } from 'src/app/services/reward/reward.service';
import { ActivatedRoute } from '@angular/router';
import { CommonValidators } from 'src/app/shared/validators/common.validators';
import { Title } from '@angular/platform-browser';
import { MenuService } from 'src/app/services/menu/menu.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Reward } from 'src/app/models/reward.model';
import { FileUpload } from 'src/app/shared/models/file-upload.model';
import { FileValidator } from 'src/app/shared/validators/file.validators';

@Component({
  selector: 'app-reward-detail',
  templateUrl: './reward-detail.component.html',
  styleUrls: ['./reward-detail.component.css']
})
export class RewardDetailComponent implements OnInit {

  constructor(private fb: FormBuilder,
    private rewardService: RewardService,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    /*public dialogRef: MatDialogRef<RewardDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Reward,*/
    private menuService: MenuService) { }

  rewardForm: FormGenerator = rewardForm;

  rewardFormGroup: FormGroup;

  submitted = false;

  id: string;

  ngOnInit() {
    this.generateFormGroup();
    this.activatedRoute.params.subscribe(params => {
      this.id = params.id;
      if (params.id !== "new") {
        this.chargeReward();
      }
    });



    this.setTitle();

  }

  setTitle() {
    this.activatedRoute.params.subscribe(params => {
      let title = "Mettre à jour une récompense";
      if (params.id === "new") {
        title = "Créer une nouvelle récompense";
      } else {
        title = "Mettre à jour une récompense";
      }
      this.titleService.setTitle(title);
      this.menuService.title.next(title);
    });
  }

  generateFormGroup() {
    this.rewardFormGroup = this.fb.group({
      id: ["",],
      name: ["", [Validators.required]],
      description: ["", ],
      quantity: ["", [Validators.required]],
      thumbnail_media: this.fb.group({
        file: [null, [Validators.required, FileValidator.fileFormatValidator("image")]],
        name: ""
      })
    });
  }

  async save() {
    this.submitted = true;
    if (this.rewardFormGroup.invalid) {
      console.log(this.rewardFormGroup);
      return;
    }

    try {
      console.log(this.rewardFormGroup.value);
      
      await this.rewardService.save(this.rewardFormGroup.value).toPromise();
    }
    catch (e) {
      console.error(e);
    }

  }

  async chargeReward() {
    if (!!this.id) {
      try {
        const reward = await this.rewardService.getReward(this.id).toPromise()
        this.rewardFormGroup.patchValue(reward);
      } catch (e) {
        console.error(e);
      }
    }
  }

  changeHandler($event, formControlName) {
    console.log($event);
    this.rewardFormGroup.get(formControlName).patchValue($event);
  }

  fileUploadChange(event: FileUpload, formControlName: string) {
    this.rewardFormGroup.get(formControlName).patchValue(event);
    this.rewardFormGroup.get(formControlName).updateValueAndValidity();
  }

}
