import { FormGenerator } from "src/app/shared/models/form-generator.model";
import { FieldComponentType } from "src/app/shared/models/fields-component-type";
import { FileType } from "src/app/shared/models/file-type";
import { DataType } from "src/app/shared/models/data-type";

export default {
    fields: [
        {
            formControlField: "name", 
            label: "Nom",
            errors: {
                required: "Ce champ est requis",
                uniqueName: "Le nom de la récompense dois être unique parmi les vôtres"
            },
            dataType: DataType.String,
            fieldComponentType: FieldComponentType.MatFormField
        },
        {
            formControlField: "description",
            label: "Description",
            dataType: DataType.String,
            fieldComponentType: FieldComponentType.MatFormField
        },
        {
            formControlField: "quantity",
            label: "Quantité total",
            errors: {
                required: "Ce champ est requis",
            },
            dataType: DataType.String,
            fieldComponentType: FieldComponentType.MatFormField
        },
        // {
        //     formControlField: "thumbnail_media",
        //     fieldComponentType: FieldComponentType.FileUpload,
        //     fileUploadParams: {
        //         fileType: FileType.Image,
        //     },
        //     errors: {
        //         required: "Ce champ est requis",
        //     },
        // },

    ]
} as FormGenerator