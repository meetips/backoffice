import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageContainerComponent } from 'src/app/shared/components/page-container/page-container.component';
import { RewardDetailComponent } from './reward-detail/reward-detail.component';
import { RewardsComponent } from './rewards/rewards.component';


const routes: Routes = [
  {
    path: "", component: PageContainerComponent,
    children: [
      { path: "", component: RewardsComponent },
      { path: ":id", component: RewardDetailComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RewardsRoutingModule { }
