import { Component, OnInit, ViewChild } from '@angular/core';
import { RewardService } from 'src/app/services/reward/reward.service';
import { Reward } from 'src/app/models/reward.model';
import { MenuService } from 'src/app/services/menu/menu.service';
import { Title } from '@angular/platform-browser';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-rewards',
  templateUrl: './rewards.component.html',
  styleUrls: ['./rewards.component.scss']
})
export class RewardsComponent implements OnInit {

  constructor(private rewardService: RewardService, 
    private titleService: Title, 
    private menuService: MenuService,
    private router: Router) { }

  rewards: Reward[];
  dataSource;

  displayedColumns = [
    "name",
    "description",
    "quantity",
    "thumbnail_media",
    "actions"
  ];

  @ViewChild(MatPaginator, { read: true, static: true }) paginator: MatPaginator;


  ngOnInit() {
    this.chargeRewards();
  }

  async chargeRewards() {
    try {
      this.rewards = (await this.rewardService.getRewards().toPromise());
      this.dataSource = new MatTableDataSource<Reward>(this.rewards);

      this.dataSource.paginator = this.paginator;

    } catch (e) {
      console.error(e);
    }
  }

  edit(rewardId: number) {
    this.router.navigate(['/account/rewards', rewardId]);
  }

}
