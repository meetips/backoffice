import { Component, OnInit } from '@angular/core';
import { PayoutService } from '../../services/payout/payout.service';
import { Payout } from '../../models/models';
import { ToastrService } from 'ngx-toastr';
import { Title } from '@angular/platform-browser';
import { MenuService } from 'src/app/services/menu/menu.service';



@Component({
  selector: 'app-payout',
  templateUrl: './payout.component.html',
  styleUrls: ['./payout.component.css']
})
export class PayoutComponent implements OnInit {

  constructor(private payoutService: PayoutService, private toatrService: ToastrService, private titleService: Title, private menuService: MenuService) { }

  isEnable = false;

  model: any = {
    iban: '',
    bic: '',
    paypal_account: ''
  };

  payoutsRequestDataSource: Payout[] = [];
  payoutsRequestColumns: string[] = [
    'user_email', 'amount', 'status', 'method', 'created_at', 'updated_at'
  ];
  currentPayoutRequest: any;

  ngOnInit() {
    this.chargePayoutsRequest();
  }

  enableChange(event) {
    this.isEnable = ! this.isEnable;
  }

  chargePayoutsRequest() {
    this.payoutService.getPayouts().subscribe((res: any) => {
      this.payoutsRequestDataSource = res;
    }, err => this.toatrService.error('Cannot import payout request list'));
  }

  openRow(row?) {
    if (!this.currentPayoutRequest) {
      this.currentPayoutRequest = row;
    } else {
      this.currentPayoutRequest = null;
    }


  }


}
