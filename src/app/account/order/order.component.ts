import { Component, OnInit } from '@angular/core';
import { AdvertisingService } from '../../services/advertising/advertising.service';
import { Advertising } from '../../models/models';
import { Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer, Title } from '@angular/platform-browser';
import { YoutubeApiService } from 'src/app/services/youtube-api/youtube-api.service';
import * as moment from 'moment';
import { range } from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';
import { environment } from 'src/environments/environment';
import { MenuService } from 'src/app/services/menu/menu.service';
declare var paypal;

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  constructor(private advertisingService: AdvertisingService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private sanitizer: DomSanitizer,
    private youtubeService: YoutubeApiService,
    private authService: AuthService
    , private titleService: Title, private menuService: MenuService
  ) { 
  }

  model: Advertising = {};
  isTotallyPaid = true;

  paypalButtonRendered = false;
  touched = false;

  paypalPayment = false;
  thumbnail: string = null;
  orderId: any = null;
  token: string = null;
  durationString: string = '';

  toPayRate = null;

  amountToPay = 0;
  estimatedView = 0;
  percentList = [];

  async ngOnInit() {

    this.percentList = this.initRangeList(5, 100, 5);

    this.activatedRoute.params.subscribe((params: any) => {
      this.orderId = params['id'] | 0;
      console.log(this.orderId);
      this.token = params.hasOwnProperty('token') ? params['token'] : null;


      this.advertisingService.getAdvertising(this.orderId).subscribe((response: Advertising) => {
        this.model = response;
        this.model.video_media.file = this.advertisingService.getFileUrl(response.video_media.file)
        this.model.thumbnail_media.file = this.advertisingService.getFileUrl(response.thumbnail_media.file)
      });


      if (this.isTotallyPaid && this.paypalPayment) {

        console.log('paypal button will be rendered');
        this.chargePaypalButton();

      } else {
        // this.router.navigate(['/account']);
      }
      // }
    });
  }

  initRangeList(from: number, to: number, step: number) {
    const res = [];
    for (let i = from; i <= to ; i+= step) {
      res.push(i);
    }
    return res;
  }

  getDurationString(duration) {
    let res = '';
    if (duration.hours) {
      res += duration.hours + ' heure ';
    }
    if (duration.minutes) {
      res += duration.minutes + ' minutes et ';
    }
    if (duration.seconds) {
      res += duration.seconds + ' secondes';
    }
    return res;
  }

  updatePaymentMethod(event) {
    this.paypalPayment = event;
    this.touched = true;

    // console.log(event);
    if (this.paypalPayment) {

      console.log('paypal button will be rendered');
      setTimeout(() => this.chargePaypalButton(), 100);

    } 
  }

  async chargePaypalButton() {
    const button = document.getElementById('paypal-button');
    const id = this.orderId;
    const user = this.authService.getToken();
    const percent_to_pay = this.toPayRate;

    if (button && !!this.toPayRate) {
      button.innerHTML = '';      
      paypal.Button.render({
        env: 'sandbox', // Or 'production'
        commit: true,
        // Set up the payment:
        // 1. Add a payment callback
        payment: function (data, actions) {
          // 2. Make a request to your server
          return actions.request.post(`${environment.backendApi}/api/payins/create_payment/`, {
            advertising_id: id,
            user_token: user,
            percent_to_pay:  percent_to_pay 
          }).then(function (res) {
            // 3. Return res.id from the response
            return res.id;
          }).catch(function(error) {
            console.log(error);
          });
        },
        // Execute the payment:
        // 1. Add an onAuthorize callback
        onAuthorize: function (data, actions) {
          // 2. Make a request to your server
          return actions.request.post(`${environment.backendApi}/api/payins/execute_payment/`, {
            payment_id: data.paymentID,
            payer_id: data.payerID
          })
            .then(function (res) {
              // 3. Show the buyer a confirmation message.
            });
        }
      }, '#paypal-button');
    }

  }

  getDaysFromTo(date1: Date, date2: Date) {
    const res = moment(date2).diff(moment(date1), 'days');
    const days = [];
    // tslint:disable-next-line:forin
    range(0, res).forEach((value) => {
        days.push(moment(date1).add(value, 'day').format('DD/MM/YYYY'));
    });
    return days;
  }


  updateEstimation() {
    this.advertisingService.getAmountToPay(this.model.id, this.toPayRate).subscribe((response: any) => {
      this.amountToPay = response.to_pay;
      this.estimatedView = response.estimated_view;
      this.touched = true;
      this.chargePaypalButton();
    });
  }

}
