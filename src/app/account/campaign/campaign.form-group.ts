import { FormGroup, FormArray } from "@angular/forms";

export class CampaignFormGroup {
    constructor() {

    }

    campaignFormGrp: FormGroup = null;


  get name() {
    return this.campaignFormGrp.get('name');
  }
  get url() {
    return this.campaignFormGrp.get('url');
  }
  get description() {
    return this.campaignFormGrp.get('description');
  }
  get is_monthly() {
    return this.campaignFormGrp.get('is_monthly');
  }
  get is_active() {
    return this.campaignFormGrp.get('is_active');
  }
  get thanks_message() {
    return this.campaignFormGrp.get('thanks_message');
  }
  get potential_users() {
    return this.campaignFormGrp.get('potential_users');
  }
  get objective_amount() {
    return this.campaignFormGrp.get('objective_amount');
  }
  get show_objective_amount() {
    return this.campaignFormGrp.get('show_objective_amount');
  }
  get show_tip_count() {
    return this.campaignFormGrp.get('show_tip_count');
  }
  get show_current_amount() {
    return this.campaignFormGrp.get('show_current_amount');
  }
  get end_date() {
    return this.campaignFormGrp.get('end_date');
  }
  get categories() {
    return this.campaignFormGrp.get('categories');
  }
  get banner_media() {
    return this.campaignFormGrp.get('banner_media');
  }
  get thumbnail_media() {
    return this.campaignFormGrp.get('thumbnail_media');
  }
  get demographic_data() {
    return this.campaignFormGrp.get('demographic_data');
  }
  get audiences() {
    return this.campaignFormGrp.get('audiences');
  }
  get rewards(): FormArray {
    return this.campaignFormGrp.get('rewards') as FormArray
  }
}