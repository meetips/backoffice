import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountCampaignComponent } from './campaign.component';

describe('CampaignComponent', () => {
  let component: AccountCampaignComponent;
  let fixture: ComponentFixture<AccountCampaignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountCampaignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountCampaignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
