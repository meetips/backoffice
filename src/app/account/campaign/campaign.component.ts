import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { isNullOrUndefined, isArray, isNumber, isObject } from 'util';
import { CampaignService } from '../../services/campaign/campaign.service';
import { ToastrService } from 'ngx-toastr';
import { Campaign } from '../../models/models';
import { RewardService } from '../../services/reward/reward.service';
import { Title } from '@angular/platform-browser';
import { MenuService } from 'src/app/services/menu/menu.service';
import { FormBuilder, Validators, ValidatorFn, AbstractControl, FormGroup, FormArray } from '@angular/forms';
import * as moment from 'moment';
import { CommonValidators } from 'src/app/shared/validators/common.validators';
import { FileUpload } from 'src/app/shared/models/file-upload.model';
import { FileValidator } from 'src/app/shared/validators/file.validators';
import { CampaignFormGroup } from './campaign.form-group';
import { HttpErrorResponse } from '@angular/common/http';

import * as CampaignForm from './campaign.form';
import { Reward } from 'src/app/models/reward.model';


@Component({
  selector: 'app-account-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.scss']
})
export class AccountCampaignComponent extends CampaignFormGroup implements OnInit {

  constructor(
    private campaignService: CampaignService,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService,
    private router: Router,
    private rewardService: RewardService,
    private titleService: Title,
    private menuService: MenuService,

    private formBuilder: FormBuilder
  ) {
    super();
    titleService.setTitle('Ma campagne');
    this.menuService.title.next('Ma campagne');

    this.campaignFormGrp = formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.maxLength(40)])],
      url: ['', Validators.compose([Validators.required, Validators.pattern("^[a-zA-Z0-9._-]+$"),
      Validators.minLength(4), Validators.maxLength(25)])],
      description: ['', Validators.maxLength(140)],
      thanks_message: ['', Validators.required],
      objective_amount: ['', Validators.required],
      thumbnail_media: this.formBuilder.group({
        id: [""],
        file: [null,
          [Validators.required, FileValidator.fileFormatValidator("image")],
        ],
        name: [""],
        localeFile: [false]
      }),
      banner_media: this.formBuilder.group({
        id: [""],
        file: [null,
          [Validators.required],
        ],
        name: [""],
        localeFile: [false]
      }),
      // thumbnail_media: ['', Validators.required],
      // banner_media: ['', Validators.required],
      show_current_amount: [true, Validators.required],
      show_tip_count: [true, Validators.required],
      show_objective_amount: [true, Validators.required],
      is_active: [true, Validators.required],
      is_monthly: [true, Validators.required],
      end_date: [],
      potential_users: ['', Validators.required],
      categories: [[], [Validators.required, CommonValidators.minListLength(1)]],
      demographic_data: [[], [Validators.required, CommonValidators.minListLength(1)]],
      audiences: [[], [Validators.required, CommonValidators.minListLength(1)]],
      rewards: this.formBuilder.array([])
    });

    activatedRoute.paramMap.subscribe(params => {
      if (params.has('id')) {
        this.campaignService.getCampaign(params.get('id')).subscribe((campaign: Campaign) => {
          if (!isNullOrUndefined(campaign)) {

            this.chargeCampaign(campaign);

            this.name.setValue(campaign.name);
            this.url.setValue(campaign.url);
            this.description.setValue(campaign.description);
            this.thanks_message.setValue(campaign.thanks_message);
            this.objective_amount.setValue(campaign.objective_amount);
            this.show_current_amount.setValue(campaign.show_current_amount);
            this.show_tip_count.setValue(campaign.show_tip_count);
            this.show_objective_amount.setValue(campaign.show_objective_amount);
            this.is_active.setValue(campaign.is_active);
            this.is_monthly.setValue(campaign.is_monthly);
            this.end_date.setValue(campaign.end_date);
            this.potential_users.setValue(campaign.potential_users);
            this.categories.setValue(campaign.categories);
            this.demographic_data.setValue(campaign.demographic_data);
            this.audiences.setValue(campaign.demographic_data);
            this.banner_media.setValue(campaign.banner_media);
            this.thumbnail_media.setValue(campaign.thumbnail_media);

          } else {
            this.model = new Campaign();
          }
        });
      } else {

      }
    });

  }

  campaignForm = CampaignForm.default;

  matFormFieldType = "fill";


  possibleTag;

  public model: Campaign = {
    id: null,
    name: '',
    url: '',
    description: '',
    thanks_message: null,
    objective: null,
    objective_amount: 1.00,
    show_current_amount: false,
    show_tip_count: false,
    show_objective_amount: false,
    is_monthly: true,
    is_active: true,
  };

  // public rewards: Reward[] = [];

  public successCreated = null;
  public saveErrors: any = {};

  isUpdate = false;

  @ViewChild('bannerInput', { static: false })
  bannerInput: HTMLInputElement;

  @ViewChild('thumbnailInput', { static: false })
  thumbnailInput: HTMLInputElement;

  existingModel: Campaign;
  maxBannerSize = 4096000;
  maxThumbnailSize = 2457600;

  ngOnInit() {
    const id = this.activatedRoute.snapshot.params["id"] as number;
    if (!isNullOrUndefined(id)) {
      this.chargeCampaign(id);
    }


  }


  containValidator(control?: AbstractControl, validator?: ValidatorFn) {
    const v = this.campaignFormGrp.get('gfr').validator({} as AbstractControl);
  }


  setIsMonthly(event: boolean) {
    if (event === true) {
      this.is_monthly.setValue(true);
      this.end_date.setValidators([Validators.required]);
      this.end_date.updateValueAndValidity();
    } else {
      this.is_monthly.setValue(false);
      this.end_date.clearValidators();
      this.end_date.updateValueAndValidity();
    }
  }

  isDisabled() {
    if (!this.campaignFormGrp.touched && this.campaignFormGrp.invalid) {
      return false;
    } else if (this.campaignFormGrp.touched && this.campaignFormGrp.invalid) {
      return true;
    } else if (this.campaignFormGrp.invalid) {
      return true;
    }
    return false;
  }

  saveOrUpdate() {
    Object.values(this.campaignFormGrp.controls).forEach(element => {
      element.markAsTouched();
      element.markAsDirty();
    });
    if (this.campaignFormGrp.valid) {
      if (this.isUpdate) {
        this.update();
      } else {
        this.save();
      }
    } else {
      console.log('is invalid');
      console.log(this.campaignFormGrp.errors);

    }
  }

  chargeCampaign(id: any) {
    this.campaignService.getCampaign(id).subscribe((campaign: Campaign) => {
      this.campaignFormGrp.patchValue(campaign);
    });
    // if (!!campaign) {
    //   this.isUpdate = true;
    //   this.model = Object.assign({}, campaign);
    //   this.rewards = campaign.rewards;
    //   if (isArray(this.rewards) && this.rewards.length > 0) {
    //     this.rewards.forEach(item => item.imageLoaded = item.image);
    //   }
    //   this.model.bannerLoaded = this.model.banner_media;
    //   this.model.thumbnailLoaded = this.model.thumbnail_media;
    //   this.existingModel = Object.assign({}, this.model);
    // }
  }

  todayDate() {
    return moment().toDate();
  }

  save() {

    const toSend = Object.assign({}, this.campaignFormGrp.value);
    const formData: FormData = this.getFormData(toSend);

    if (this.isUpdate) {
      this.update();
    } else {
      // formData = this.addImages(formData);
      // formData.append('banner', this.bannerFile, this.bannerFile.name);
      this.campaignService.createCampaign(formData).subscribe((res: any) => {
        if (res.id) {
          this.model.id = res.id;
        }
        // for (const reward of this.rewards) {
        //   reward.campaign = this.model.id;
        //   // this.saveReward(this.getFormData(reward));
        // }
        this.successCreated = true;
        this.toastrService.success('La création de votre Campagne a réussi.', 'Tous est bon');
      }, (err: HttpErrorResponse) => {
        console.log(err);
        this.toastrService.error('Une erreur est survenue lors de la création de votre campagne', 'Oups !');
        this.successCreated = false;
        this.saveErrors = err;
      });
    }


  }

  saveReward(reward: Reward) {
    this.rewardService.save(reward).toPromise().then().catch(err => {

    });
  }

  // getFormData(toSend): FormData {
  //   const formData = new FormData();
  //   for (const property of Object.keys(toSend)) {
  //     // console.log(property);
  //     // console.log(toSend[property]);
  //     const value = toSend[property];
  //     if (!isNullOrUndefined(value)) {
  //       formData.append(property, value);
  //     }
  //   }
  //   return formData;
  // }

  getFormData(toSend): FormData {
    const formData = new FormData();
    for (const property of Object.keys(toSend)) {
      const value = toSend[property];
      if (!isNullOrUndefined(value)) {
        if (isArray(value)) {
          value.forEach(element => {
            formData.append(property, element);
          });
        } else if (isObject(value)) {
          for (const dataProp of Object.keys(value)) {
            if (!isNullOrUndefined(value[dataProp])) {
              formData.append(`${property}.${dataProp}`, value[dataProp]);
            }
          }
        } else {
          formData.append(property, value);
        }
      }
    }
    return formData;
  }

  // addImages(formData): any {
  //   if (!isNullOrUndefined(this.bannerFile)) {
  //     formData.append('banner', this.bannerPreview);
  //   }
  //   if (!isNullOrUndefined(this.thumbnailFile)) {
  //     formData.append('thumbnail', this.thumbnailPreview);
  //   }
  //   return formData;
  // }

  update() {
    // const toSend = this.model;
    const toSend = Object.assign({}, this.campaignFormGrp.value);

    for (const item in this.campaignFormGrp.value) {
      // console.log('item ' + item);
      if (this.campaignFormGrp.value[item] !== this.existingModel[item]) {
        if ((item === 'banner' && this.campaignFormGrp.value['banner'] !== '') ||
          (item === 'thumbnail' && this.campaignFormGrp.value['thumbnail'] !== '') ||
          (item !== 'banner' && item !== 'thumbnail')) {
          toSend[item] = this.campaignFormGrp.value[item];

        }
      }
    }

    console.log('toSend');
    console.log(toSend);
    toSend['id'] = this.existingModel.id;
    const formData = this.getFormData(toSend);
    // this.addImages(formData);
    this.campaignService.updatePatialCampaign(formData).subscribe(res => {
      this.successCreated = true;
      this.toastrService.success('Campagne mise à jour avec succès');
      this.existingModel = Object.assign({}, res);
    }, (err: any) => {
      console.log(err);
      this.successCreated = false;
      this.saveErrors = err;
      this.toastrService.error('La campagne n\'a pas pu être mise à jour');
    });
  }

  fileEvent(event, typeImage: 'banner' | 'thumbnail') {
    if (event.target.files.length === 0) {
      this.model[typeImage] = null;
      this.model[typeImage + 'Loaded'] = null;
      return;
    }
    const reader = new FileReader();
    reader.onload = (e: any) => {
      this.model[typeImage + 'Loaded'] = e.target.result;

    };
    if (typeImage === 'banner' && event.target.files[0].size > this.maxBannerSize ||
      typeImage === 'thumbnail' && event.target.files[0].size > this.maxThumbnailSize) {
      this.toastrService.warning('Fichier trop grand, Veuillez en choisir un plus petit');
      event.target.value = '';
    } else {
      reader.readAsDataURL(event.target.files[0]);
      this.model[typeImage] = event.target.files[0];
    }
  }

  rewardFileEvent(event, rewardImageIndex) {
    if (event.target.files.length === 0) {
      this.rewards[rewardImageIndex].imageLoaded = null;
      this.rewards[rewardImageIndex].image = null;
      return;
    }
    const reader = new FileReader();
    reader.onload = (e: any) => {
      this.rewards[rewardImageIndex].imageLoaded = e.target.result;
    };
    reader.readAsDataURL(event.target.files[0]);
    this.rewards[rewardImageIndex].image = event.target.files[0];
  }

  verifyUrl() {
    if (!isNullOrUndefined(this.model.url) && this.model.url !== '') {
      this.campaignService.verifyUrl(this.model.url).subscribe((res: any) => {
        this.toastrService.success('L\'url ' + this.model.url + ' que vous avez entré est valide.', 'Url valide');
      }, (err: any) => {
        const valid = 'Une url valide et unique ne peut contenir que les caractères [A-Z] ou [a-z] ou [0-9] ou [_ - .].';
        this.toastrService.error('L\'url "' + this.model.url + '" que vous avez entré n\'est pas valable. ' + valid, 'Url non valide',
          { timeOut: 8000 });
      });
    } else {
      this.toastrService.warning('L\'url ne peut être vide');
    }
  }

  is_activable() {
    return true;
  }

  addReward() {
    this.rewards.push(this.formBuilder.group({
      id: null,
      name: ['', Validators.required],
      description: ['', Validators.required],
      quantity: ['', Validators.required],
      thumbnail_media: this.formBuilder.group({
        id: [""],
        file: [null,
          [Validators.required, FileValidator.fileFormatValidator("image")],
        ],
        name: [""],
        localeFile: [false]
      })
    }));
  }

  removeReward(index?) {
    if (!!index) {
      this.rewards.removeAt(index);
    } else {
      this.rewards.removeAt(this.rewards.length - 1);
    }
  }

  addTag(tag) {
    this.model.code_category_viewpay = tag;
  }

  selectCategory($event) {
    this.model.categories = $event;
    this.categories.setValue($event);
  }

  selectAudience($event) {
    this.model.audiences = $event;
    this.audiences.setValue($event);
  }

  fileUploadChange(event: FileUpload, formControlName: string) {
    this.campaignFormGrp.get(formControlName).patchValue(event);
    this.campaignFormGrp.get(formControlName).updateValueAndValidity();
    console.log(event);
  }

  rewardFileChange(event: FileUpload, index: number) {
    this.rewards.at(index).get('thumbnail_media').patchValue(event);
  }

  hasErrors(field) {

  }

  computeParams(params?) {
    const res = {};
    for (const param in params) {
      if ((param as string).startsWith('$')) {
        res[param] = this[param];
      } else {
        res[param] = param;
      }
    }
    return res;
  }

  execute(func, ...args) {
    return this[func](args);
  }

  isMonthly() {
    return this.is_monthly.value;
  }

  fieldChange($event, formControlName) {
    this.campaignFormGrp.get(formControlName).patchValue($event);
  }

}
