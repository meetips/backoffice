import { AccordeonFormGenerator } from "src/app/shared/models/accordeon-form-generator.model";
import { FieldComponentType } from "src/app/shared/models/fields-component-type";

export default {
  "states": [{
    "title": "Informations de base",
    "description": "Nom, url et description et message de remerciement",
    "fields": [{
      "label": "Nommez votre campagne",
      "formControlField": "name",
      "mat-iconMatSuffix": "mode_edit",
      hint: { end: "${length} " },
      "errors": {
        "required": "Ce champ est requis",
        "forbiddenName": "Une de vos campagnes utilise déjà ce nom. Veuillez en choisir un autre"
      }
    },
    {
      "label": "Url",
      "formControlField": "url",
      "hint": `Une url valide et unique ne peut contenir que les caractères <
          code > [A - Z] < /code> ou <code>[a-z]</code > ou < code > [0 - 9] < /code> ou <
          code > [_ - .] < /code> .`,
      "errors": {}
    },
    {
      "label": "Une courte description pour cette campagne",
      "formControlField": "description"
    },
    {
      "label": "Message de remerciement",
      "formControlField": "thanks_message"
    },
    {
      "label": "Montant de l'objectif à atteindre",
      "formControlField": "objective_amount",
      "type": "number",
      "matPrefix": `<i class="material-icons">euro_symbol</i>`,
      "hint": `Tu peux inscire dans cette case le montant que tu souhaite
        atteindre pour cette campaign (mensuel ou non)`,
      "errors": {
        "required": "Ce champ est requis"
      }
    },
    {
      "label": "Nombre d'utilisateur potentiel",
      "formControlField": "potential_users",
      "type": "number",
      "hint": `Donne une estimation du nombre de personne susceptible de te
        soutenir par ce service (nombre d'abonnés ou de followers par
        exemple)`,
      "errors": {
        "required": "Ce champ est requis"
      }
    }
    ]
  },
  {
    "title": "Media",
    "description": "Bannière et image de profile",
    "fields": [{
      "label": "Importez une image de bannière: dimensions recommandées 1920px x 400px. Poids maximal: 500ko.",
      "fieldComponentType": FieldComponentType.FileUpload,
      "formControlField": "banner_media"
    }, {
      "label": "Importez une image de profile: dimensions recommandées 300px x 300px. Poids maximal: 200ko.",
      "fieldComponentType": FieldComponentType.FileUpload,
      "formControlField": "thumbnail_media"
    }]
  },
  {
    title: "Objectif",
    description: "Objectif et nombre d'utilsateurs potentiel",
    fields: [
      {
        formControlField: "objective_amount",
        label: "Montant de l'objectif à atteindre",
      },
      {
        formControlField: "potential_users",
        label: "Nombre d'utilisateur potentiel"
      }
    ]
  },
  {
    "title": "Paramètres",
    "description": "Type de campagne et durée",
    "fields": [
      {
        "fieldComponentType": FieldComponentType.ChoicesButton,
        "label": "",
        "formControlField": "is_monthly",
        "choicesButtonsParams": {
          "onTrue": "Campagne mensuel",
          "onFalse": "Campagne unique",
          "isTrue": true,
          "change": "fieldChange"
        }
      },
      {
        "if": "isMonthly",
        "label": "Date de fin de campagne",
        "formControlField": "end_date",
        "dateParams": {
          "min": "$todayDate",
          "matDatepicker": "Campagne unique",
        },
        "errors": {
          "required": "Ce champ est requis"
        }
      },
      {
        "fieldComponentType": "ChoicesButtonComponent",
        "label": "",
        "formControlField": "is_active",
        "choicesButtonsParams": {
          "onTrue": "Activer la campagne",
          "onFalse": "Désactiver la campagne",
          "isTrue": true
        }
      }
    ]
  }
  ]
} as any as AccordeonFormGenerator;
