import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { AccountComponent } from "./account.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { AdvertisingComponent } from "./advertising/advertising.component";
import { OrderComponent } from "./order/order.component";
import { ProfileComponent } from "./profile/profile.component";
import { PasswordComponent } from "./password/password.component";
import { PayoutComponent } from "./payout/payout.component";
// import { RewardComponent } from "./reward/reward.component";
import { PayinsComponent } from "./payins/payins.component";
import { AdvertisingDashboardComponent } from "./advertising-dashboard/advertising-dashboard.component";
import { NgModule, Injectable } from "@angular/core";
import { PayoutProfileComponent } from "./payout-profile/payout-profile.component";
import { User, Campaign } from "../models/models";
import { AccountService } from "../services/account/account.service";
import { Observable } from "rxjs";
import { IsCreatorGuard } from "../guards/is-creator.guard";
import { IsAnnouncerGuard } from "../guards/is-announcer.guard";
import { AccountResolver } from "../resolvers/account.resolver";
import { AuthGuard } from "../guards/auth.guard";
import { RedirectGuard } from "../guards/redirect.guard";
import { RedirectResolver } from "../resolvers/redirect.resolver";
import { RedirectCreatorResolver } from "../resolvers/redirect-creator.resolver";
import { RedirectAnnouncerResolver } from "../resolvers/redirect-announcer.resolver";
import { AccountCampaignComponent } from "./campaign/campaign.component";
// import { RewardsComponent } from "./rewards/rewards.component";
// import { RaffleDetailComponent } from "./raffle-detail/raffle-detail.component";
// import { RafflesComponent } from "./raffles/raffles.component";
import { PageContainerComponent } from '../shared/components/page-container/page-container.component';
// import { RewardDetailComponent } from './rewards/reward-detail/reward-detail.component';

const routes: Routes = [
  {
    path: '',
    component: AccountComponent,
    resolve: {
      // account: AccountResolver
    },
    children: [
      {
        path: '',
        component: DashboardComponent,
        // pathMatch: "full",
        resolve: [
          // RedirectCreatorResolver
        ]
      },
      {
        path: 'dashboard', component: DashboardComponent,
        canActivate: [IsCreatorGuard],
        resolve: [
          // RedirectCreatorResolver
        ]
      },
      {
        path: 'campaign/:id', component: AccountCampaignComponent,
        // canActivate: [IsCreatorGuard],
        resolve: [
          // RedirectCreatorResolver
        ]
      },
      {
        path: 'campaign', component: AccountCampaignComponent,
        // canActivate: [IsCreatorGuard],
        resolve: [
          // RedirectCreatorResolver
        ]
      },
      // {
      //   path: 'rewards/:id', component: RewardDetailComponent,
      // },
      // {
      //   path: 'rewards', component: RewardsComponent,
      // },
      {
        path: 'rewards', 
        component: PageContainerComponent, 
        loadChildren: () => import('./rewards/rewards.module').then(m => m.RewardsModule),
      },
      // // {
      // //   path: 'raffles/:id', component: RaffleDetailComponent,
      // // },
      // // {
      // //   path: 'raffles', component: RafflesComponent,
      // // },
      {
        path: 'raffles',
        loadChildren: () => import('./raffles/raffles.module').then(m => m.RafflesModule),
        component: PageContainerComponent, 
      },
      {
        path: 'advertising/:id', component: AdvertisingComponent,
        resolve: [
          // RedirectAnnouncerResolver
        ]
      },
      {
        path: 'advertising', component: AdvertisingComponent,
        resolve: [
          // RedirectAnnouncerResolver
        ]
      },
      {
        path: 'order', component: OrderComponent,
        resolve: [
          // RedirectAnnouncerResolver
        ]
      },
      { path: 'profile', component: ProfileComponent },
      { path: 'password', component: PasswordComponent },
      {
        path: 'payouts', component: PayoutComponent,
      },
      { path: 'payout-profil', component: PayoutProfileComponent },
      {
        path: 'payins', component: PayinsComponent,
        resolve: [
          // RedirectAnnouncerResolver
        ]
      },
      {
        path: 'advertising-dashboard',
        component: AdvertisingDashboardComponent,
        resolve: [
          // RedirectAnnouncerResolver
        ]
      },
    ],
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
