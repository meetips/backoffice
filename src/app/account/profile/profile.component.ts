import { Component, OnInit, Input, Inject } from '@angular/core';
import { AccountService } from '../../services/account/account.service';
import { USER_TYPE_ENUM, User, Password, PayoutProfile, CustomDate } from '../../models/models';
import { MatDialog } from '@angular/material';
import { ConfirmationDialogComponent } from '../../confirmation-dialog/confirmation-dialog.component';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { isNullOrUndefined } from 'util';
import { MenuService } from 'src/app/services/menu/menu.service';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { CommonValidators } from 'src/app/shared/validators/common.validators';
import { hasError, markControlsDirty } from "src/app/shared/utils";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  // tslint:disable-next-line:max-line-length
  constructor(private accountService: AccountService, public dialog: MatDialog, private titleService: Title,
    private toastrService: ToastrService, private router: Router, private activatedRoute: ActivatedRoute,
    private menuService: MenuService, formBuilder: FormBuilder) {

    this.profileForm = formBuilder.group({
      username: ['', Validators.required],
      id: [''],
      email: ['', [Validators.required, Validators.email],],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      // birthdate: ['', Validators.required],
      birthdate: formBuilder.group({
        day: [null, Validators.required],
        month: [null, Validators.required],
        year: [null, Validators.required],
      }, [Validators.required]),
      address: ['', Validators.required],
      city: ['', Validators.required],
      country: ['', Validators.required],
      zip_code: ['', [Validators.required, Validators.pattern(/^[0-9]{5}/)]],
      thumbnail: ['',],
      is_announcer: false,
      is_creator: false,
      vat_intra: ["", CommonValidators.conditionnalRequired("is_announcer")],
      siret: ["", CommonValidators.conditionnalRequired("is_announcer")],
      company_name: ["", CommonValidators.conditionnalRequired("is_announcer")]
    });
  }

  profileForm: FormGroup;

  matFormFieldType = "fill";

  get username(): AbstractControl {
    return this.profileForm.get('username');
  }
  get email(): AbstractControl {
    return this.profileForm.get('email');
  }
  get firstname(): AbstractControl {
    return this.profileForm.get('firstname');
  }
  get lastname(): AbstractControl {
    return this.profileForm.get('lastname');
  }
  get birthdate(): AbstractControl {
    return this.profileForm.get('birthdate');
  }
  get address(): AbstractControl {
    return this.profileForm.get('address');
  }
  get city(): AbstractControl {
    return this.profileForm.get('city');
  }
  get zip_code(): AbstractControl {
    return this.profileForm.get('zip_code');
  }
  get country(): AbstractControl {
    return this.profileForm.get('country');
  }
  get is_announcer(): AbstractControl {
    return this.profileForm.get('is_announcer');
  }
  get is_creator(): AbstractControl {
    return this.profileForm.get('is_creator');
  }
  get company_name(): AbstractControl {
    return this.profileForm.get('company_name');
  }
  get siret(): AbstractControl {
    return this.profileForm.get('siret');
  }
  get vat_intra(): AbstractControl {
    return this.profileForm.get('vat_intra');
  }

  public currentUser: any = {};

  possibleTags: any[];
  maxThumbnailSize = 2457600;

  @Input()
  isMenuOpened = false;

  isEnable = true;

  isFullWidth = false;

  USER_TYPE_ENUM = USER_TYPE_ENUM;

  // hasError = hasError;

  ngOnInit() {

    this.titleService.setTitle('Mon profil');

    this.menuService.title.next('Mon profil');

    this.getConnectedUser();
    const x = window.matchMedia('(max-width: 950px)');
    this.isFullWidth = x.matches;
  }

  enableChange(event) {
    this.isEnable = !this.isEnable;
  }

  isUserActive() {
    if (this.currentUser.is_active) {
      return true;
    }
    return false;
  }

  async getConnectedUser() {
    //  subscribe(res => {
    //   console.log("res");
    //   console.log(res);
    //   this.profileForm.reset(res);
    // });

    this.activatedRoute.parent.data.subscribe(data => {
      console.log("subcribe data", data);
      const p = Object.assign({}, data["account"]);

      if (isNullOrUndefined(p.birthdate)) {
        p.birthdate = new CustomDate();
      } else {
        p.birthdate = {
          day: (p.birthdate as Date).getDate(),
          month: (p.birthdate as Date).getMonth(),
          year: (p.birthdate as Date).getFullYear(),
        }
      }

      this.profileForm.patchValue(p);
    });


    // for (const control in this.profileForm.controls) {
    //   if (p.hasOwnProperty(control)) {
    //     this.profileForm.get(control).setValue(p[control]);
    //   }
    // }

  }

  resetForm() {

    this.accountService.getCurrent().subscribe(account => {

      if (isNullOrUndefined(account.birthdate)) {
        account.birthdate = new CustomDate();
      } else {
        account.birthdate = {
          day: (account.birthdate as Date).getDate(),
          month: (account.birthdate as Date).getMonth(),
          year: (account.birthdate as Date).getFullYear(),
        }
      }
      this.profileForm.reset();
      this.birthdate.reset();
      this.profileForm.patchValue(account);
      this.profileForm.markAsUntouched();
    });

  }

  updateProfile() {
    const toSend = {};

    if (this.profileForm.valid) {

      console.log(this.profileForm.value);
      console.log(this.currentUser);
      for (const item in this.profileForm.value) {
        if (this.profileForm.value[item] !== this.currentUser[item]) {
          console.log('is nothing', this.profileForm.value[item]);
          toSend[item] = this.profileForm.value[item];
        }
      }
      console.log('toSend', toSend);
      const formData: FormData = this.getFormData(toSend);
      formData.append("id", this.profileForm.value.id);
      console.log('formData', formData);

      this.accountService.updateProfile(formData).subscribe((res: any) => {
        this.toastrService.success('Informations correctement mise à jour');
      }, e => {
        this.toastrService.warning('Impossible de mettre à jours vos informations');
      });
    } else {
      markControlsDirty(this.profileForm);
    }

  }

  getFormData(toSend): FormData {
    const formData = new FormData();
    for (const property of Object.keys(toSend)) {
      const value = toSend[property];
      if (!isNullOrUndefined(value)) {
        formData.append(property, value);
      }
    }
    return formData;
  }

  dateChange($event) {
    this.birthdate.patchValue($event);
  }

  // isCreator() {
  //   return this.model.userType === USER_TYPE_ENUM.IS_CREATOR_USER;
  // }

  // isAnnouncer() {
  //   return this.model.userType === USER_TYPE_ENUM.IS_ANNOUNCER_USER;
  // }

  // isSimpleUser() {
  //   return this.model.userType === USER_TYPE_ENUM.IS_SIMPLE_USER;
  // }

  // fileEvent(event) {
  //   if (event.target.files.length === 0) {
  //     this.model.thumbnail = null;
  //     this.model.thumbnailLoaded = null;
  //     return;
  //   }
  //   const reader = new FileReader();
  //   reader.onload = (e: any) => {
  //     this.model.thumbnailLoaded = e.target.result;
  //   };
  //   if (event.target.files[0].size > this.maxThumbnailSize) {
  //     this.toastrService.warning('Fichier trop grand, Veuillez en choisir un plus petit');
  //     event.target.value = '';
  //   } else {
  //     reader.readAsDataURL(event.target.files[0]);
  //     this.model.thumbnail = event.target.files[0];
  //   }
  // }
}
