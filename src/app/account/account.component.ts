import { Component, OnInit, ViewChild, Inject, OnChanges, Renderer2 } from '@angular/core';
import { AccountService } from '../services/account/account.service';
import { Router, Event, NavigationEnd, ActivatedRoute } from '@angular/router';
import { CampaignService } from '../services/campaign/campaign.service';
import { Campaign, User, Advertising } from '../models/models';
import { Title } from '@angular/platform-browser';
import { AdvertisingService } from '../services/advertising/advertising.service';
import { TipService } from '../services/tip/tip.service';
import { MenuService } from '../services/menu/menu.service';
import { AuthService } from '../services/auth/auth.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],

})
export class AccountComponent implements OnInit {

  constructor(private authService: AuthService, private accountService: AccountService, private renderer2: Renderer2,
    private router: Router, private campaignService: CampaignService, private tipService: TipService,
    private titleService: Title, private advertisingService: AdvertisingService, private menuService: MenuService,
    private activatedRoute: ActivatedRoute
  ) {
    titleService.setTitle('Mon compte');
    menuService.title.next('Mon Compte');

  }

  @ViewChild('accountSideNav', { static: true })
  accountSideNav: any;
  drawer: any = { mode: 'side', hasBackDrop: true, showBtn: false, isOpen: true };
  user: User = new User();
  menuTitle = "";


  largeComponent = [
    "advertising-dashboard",
    "dashboard",
  ];

  commonPages = [
    { route: "/account/profile", label: "Mon profile" },
    { route: "/account/password", label: "Mon mot de passe" },
    { route: "/account/payout-profil", label: "Mes informations de paiements" },
  ];

  creatorPages = [
    { route: "/account/dashboard", label: "Mon tableau de bord" },
    { route: "/account/payouts", label: "Mes dernièrs paiements" },
    { route: "/account/raffles", label: "Mes tirages au sorts" },
    { route: "/account/rewards", label: "Mes récompenses" },
  ];

  announcerPages = [
    { route: "/account/advertising-dashboard", label: "Mon tableau de bord annonceur" },
    { route: "/account/payins", label: "Mes dernièrs paiements" },
  ];

  userPages = [
    // { route: "/account/tips", label: "Mes dernièrs soutiens" },
    // { route: "/account/participations", label: "Mes participations aux tirages" },
    // { route: "/account/earned-rewards", label: "Mes récompenses" },
  ];

  ngOnInit() {

    // if (this.authService.isAuthenticated()) {
    this.getUser();

    this.updateNavigationDrawer(this.router.routerState.snapshot.url);

    this.router.events.subscribe((e: Event) => {
      if (e instanceof NavigationEnd) {
        this.updateNavigationDrawer(e.url);
      }
    });


    window.addEventListener("resize", (w) => {
      this.updateNavigationDrawer(this.router.routerState.snapshot.url);
    });

    // this.menuService.title.subscribe(title => {
    //   this.menuTitle = title;
    // })
  }

  updateNavigationDrawer(url: string) {
    const match = window.matchMedia("(max-width: 1200px)");
    const largeMatch = window.matchMedia("(max-width: 1500px)");
    let finded = false;
    if (url.includes("advertising-dashboard") && match.matches) {
      this.drawer.mode = "over";
      this.drawer.showBtn = true;
      this.drawer.isOpen = false;
      this.drawer.hasBackDrop = true;
    } else if (match.matches) {
      this.drawer.mode = "over";
      this.drawer.showBtn = true;
      this.drawer.isOpen = false;
      this.drawer.hasBackDrop = true;
    } else {
      this.drawer.mode = "side";
      this.drawer.showBtn = false;
      this.drawer.isOpen = true;
      this.drawer.hasBackDrop = false;
    }

  }

  getUser() {
    this.activatedRoute.data.subscribe(obs => {
      console.log(obs);
      this.user = obs.account;
    })
  }

  contentWidth() {
    if (this.accountSideNav.opened) {
      return true;
    }
    return false;
  }

  toggleMenu() {
    // const x = window.matchMedia('(max-width: 1150px)');
    // if (x.matches) {
    //   this.drawer.mode = 'over';
    //   this.drawer.hasBackDrop = true;
    // } else {
    //   this.drawer.mode = 'side';
    //   this.drawer.hasBackDrop = false;
    // }
    this.accountSideNav.toggle();
  }

  setTitle(title: string) {
    this.menuTitle = title;
  }

}
