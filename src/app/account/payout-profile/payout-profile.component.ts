import { Component, OnInit } from '@angular/core';
import { CommonValidators } from 'src/app/shared/validators/common.validators';
import { AccountService } from 'src/app/services/account/account.service';
import { MatDialog } from '@angular/material';
import { Title } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MenuService } from 'src/app/services/menu/menu.service';
import { FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { ConfirmationDialogComponent } from 'src/app/confirmation-dialog/confirmation-dialog.component';
import { PayoutProfile } from 'src/app/models/models';

@Component({
  selector: 'app-payout-profile',
  templateUrl: './payout-profile.component.html',
  styleUrls: ['./payout-profile.component.scss']
})
export class PayoutProfileComponent implements OnInit {

  constructor(private accountService: AccountService, public dialog: MatDialog, private titleService: Title,
    private toastrService: ToastrService, private router: Router,
    private menuService: MenuService, formBuilder: FormBuilder) {



    this.payoutProfileForm = formBuilder.group({
      paypal: ['', CommonValidators.requiredIfNull('iban')],
      iban: ['', [CommonValidators.conditionnalRequired('bic'), CommonValidators.requiredIfNull('paypal')]],
      bic: ['', CommonValidators.conditionnalRequired('iban')],
    });
  }

  matFormFieldType = "fill";

  payoutProfileForm: FormGroup;

  public payout: PayoutProfile = {};
  public currentPayoutProfile: PayoutProfile = {};

  ngOnInit() {
    this.titleService.setTitle('Mon profil de paiement');
    this.menuService.title.next('Mon profil de paiement');
  }

  get iban(): AbstractControl {
    return this.payoutProfileForm.get('iban');
  }
  get paypal(): AbstractControl {
    return this.payoutProfileForm.get('paypal');
  }
  get bic(): AbstractControl {
    return this.payoutProfileForm.get('bic');
  }

  updatePayout() {
    if (this.payoutProfileForm.valid) {
      this.accountService.updatePayoutProfile(this.payoutProfileForm.value).subscribe((res: any) => {
        console.log(res);
        this.toastrService.success('Informations correctement mise à jour');
      }, e => {
        console.log(e);
        this.toastrService.warning('Impossible de mettre à jours vos informations');
      });
    }
  }

  getPayoutProfileDetail() {
    this.accountService.getPayoutProfileDetail().subscribe((res: any) => {
      this.payout = res;
      this.currentPayoutProfile = res;
    });
  }



}
