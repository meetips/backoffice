import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayoutProfileComponent } from './payout-profile.component';

describe('PayoutProfileComponent', () => {
  let component: PayoutProfileComponent;
  let fixture: ComponentFixture<PayoutProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayoutProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayoutProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
