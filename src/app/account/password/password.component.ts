import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { AccountService } from 'src/app/services/account/account.service';
import { MatDialog, ThemePalette } from '@angular/material';
import { Title } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogComponent } from 'src/app/confirmation-dialog/confirmation-dialog.component';
import { MenuService } from 'src/app/services/menu/menu.service';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss']
})
export class PasswordComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, 
    private accountService: AccountService, 
    private menuService: MenuService,
    public dialog: MatDialog, 
    private titleService: Title,
    private toastrService: ToastrService) {

      titleService.setTitle('Modifier mon mot de passe');
      menuService.title.next('Modifier mon mot de passe');

    this.passwordForm = formBuilder.group({
      current_password: ['', Validators.required],
      new_password: ['', [Validators.required, Validators.pattern(this.passwordRegex)]],
      new_password_confirm: ['', [Validators.required, Validators.pattern(this.passwordRegex)]],
    });
   }

  model: any = {
    currect_password: '',
    new_password: '',
    password_confirm: ''
  };

  matFormFieldType = "fill";


  passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/;


  passwordForm: FormGroup;

  ngOnInit() {
  }

  get current_password(): AbstractControl {
    return this.passwordForm.get('current_password');
  }
  get new_password(): AbstractControl {
    return this.passwordForm.get('new_password');
  }
  get new_password_confirm(): AbstractControl {
    return this.passwordForm.get('new_password_confirm');
  }

  updatePassword() {
    if (this.passwordForm.valid) {
      this.accountService.updatePassword(this.passwordForm.value).subscribe((res: any) => {
        this.toastrService.success('Mot de passe correctement mise à jour');
      }, e => {
        this.toastrService.warning('Impossible de modifier votre mot de passe');
      });
    } else {
      this.toastrService.warning('Les nouveaux mot de passe ne corresponde pas');

    }
  }

}
