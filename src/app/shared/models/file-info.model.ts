export class FileInfo {
    size: number;
    width?: number;
    height?: number;
    fileType?: "video" | "image";
    fileExtension?: string;
    duration?: number;
}