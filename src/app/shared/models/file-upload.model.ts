export type FileUpload = {
    id?: number,
    file: File,
    name: string,
    localFile: boolean
  }