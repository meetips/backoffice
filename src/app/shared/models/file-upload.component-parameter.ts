import { FormGroup } from "@angular/forms";

import { FileUpload } from "./file-upload.model";

import { FileInfo } from "./file-info.model";
import { EventEmitter } from "@angular/core";
import { FileType } from "./file-type";

export class FileUploadComponentParameter {
    fileFormGroup: FormGroup;
    fileUploadChange: EventEmitter<FileUpload> = new EventEmitter();  
    fileInfoChange?: EventEmitter<FileInfo> = new EventEmitter();
    fileType: FileType;
    filePreviewSrc: any;
    ratioRange?: number[] = null;
    acceptedImageFormat? = ['image/jpeg', 'image/png', 'image/jpg', 'image/gif', '*/jpeg'];
    acceptedVideoFormat? = [
      'video/mpeg',
      'video/ogg',
      'video/webm',
      'video/x-flv',
      'video/mp4'
    ];
}