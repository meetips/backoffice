export class Page<T> {
    content: T[];
    number?: number;
    offset?: number;
    size?: number;
    totalPages?: number;
    totalElements?: number;
}