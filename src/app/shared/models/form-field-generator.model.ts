import { DateInputComponentParameter } from "./date-input.component-parameter";

import { InputComponentParameter } from "./input.component-parameter";

import { FileUploadComponentParameter } from "./file-upload.component-parameter";

import { ChoicesButtonComponentParameter } from "./choices-button.component-parameter";
import { DataType } from "./data-type";
import { FieldComponentType } from "./fields-component-type";
import { TemplateRef } from "@angular/core";
import { MultipleValueSelectorComponentParameter } from "./multiple-value-selector.component-parameter";

export class FormFieldGenerator {
    if?: string;
    classes?: string;
    label?: string;
    placeholder?: string;
    formControlField?: string;
    dataType: DataType = DataType.String;
    dateParams?: DateInputComponentParameter;
    inputParams?: InputComponentParameter;
    fileUploadParams?: FileUploadComponentParameter;
    choicesButtonParams?: ChoicesButtonComponentParameter;
    multipleValueParams?: MultipleValueSelectorComponentParameter;
    fieldComponentType: FieldComponentType = FieldComponentType.MatFormField;
    hint?: string | {start: string, end: string};
    errors?: {[error: string]: [string]};
    content: TemplateRef<any>;
}