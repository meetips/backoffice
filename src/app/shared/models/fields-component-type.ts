export enum FieldComponentType {
    MatFormField = "MatFormField",
    FileUpload = "FileUpload",
    ChoicesButton = "ChoicesButton",
    SlideToggle = "SlideToggle",
    MultipleValueSelector = "MultipleValueSelector",
    AutoCompleteSelector = "AutoCompleteSelector",
}