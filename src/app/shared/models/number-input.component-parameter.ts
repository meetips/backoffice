export class NumberInputComponentParameter {
    min?: number;
    max?: number;
    step?: number;
}