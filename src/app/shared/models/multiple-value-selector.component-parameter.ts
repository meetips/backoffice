export class MultipleValueSelectorComponentParameter {
    keyValueType: boolean = false;
    options: any[];
    keyName: string;
    valueName: string;
    multiple?: boolean;
    optionTemplate?: any;
}
