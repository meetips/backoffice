export class ChoicesButtonComponentParameter {
    onTrue: string;
    onFalse: string;
    isTrue: boolean;
}