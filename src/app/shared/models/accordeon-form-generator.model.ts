import { DateInputComponentParameter } from "./date-input.component-parameter";
import { InputComponentParameter } from "./input.component-parameter";
import { FileUploadComponentParameter } from "./file-upload.component-parameter";
import { AbstractControl } from "@angular/forms";
import { ChoicesButtonComponentParameter } from "./choices-button.component-parameter";
import { FormFieldGenerator } from "./form-field-generator.model";

export class AccordeonFormGenerator {
    states: [{
        title?: string,
        description?: string,
        fields?: FormFieldGenerator[]
    }];
}