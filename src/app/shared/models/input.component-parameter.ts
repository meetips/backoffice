export class InputComponentParameter {
    maxLength?: number;
    type?: "number" | "string";
}