export enum DataType {
    String = "string",
    Number = "number",
    Enum = "enum",
    Date = "date",
    Boolean = "boolean"
}