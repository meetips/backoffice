export class Parameter {
    name: string;
    type: string;
    value_boolean?: boolean;
    value_string?: string;
    value_integer?: number;
    value_decimal?: number;
    value?: boolean | string | number;
}