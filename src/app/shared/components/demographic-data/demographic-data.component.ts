import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { MatCheckboxChange } from '@angular/material';
import { Audience, DemographicData } from 'src/app/models/models';
import { DemographicDataService } from 'src/app/services/demographic-data/demographic-data.service';

@Component({
  selector: 'app-demographic-data',
  templateUrl: './demographic-data.component.html',
  styleUrls: ['./demographic-data.component.scss']
})
export class DemographicDataComponent implements OnInit {

  constructor(private demographicDataService: DemographicDataService) { }

  @Output()
  selectedDemographicData: EventEmitter<number[]> = new EventEmitter();

  @Input()
  selectedItems: number[] = [];

  initialData: any[] = [];

  items: DemographicData[] = [];

  ngOnInit() {
    this.demographicDataService.getAllDemographicData().subscribe((data: DemographicData[]) => {
      this.initialData = data;
      const selectableId: number[] = [];
      data.forEach(elt => {
       if (elt.children.length > 0) {
         this.items.push(elt);
       } else {
        selectableId.push(elt.id);
       }
      });
      this.selectedItems = selectableId;

      this.selectedDemographicData.emit(selectableId);
    });
  }

  getElementById(id: number) {
    return this.initialData.find((predicate) => predicate.id === id)
  }
 
  selectItem(event: MatCheckboxChange, itemId) {
    const indexOfItem = this.selectedItems.indexOf(itemId); 
    if (event.checked) {
      if ( indexOfItem < 0) {
        this.selectedItems.push(itemId);
      } 
    } else {
      if ( indexOfItem >= 0) {
        this.selectedItems.splice(indexOfItem, 1);
      }
    }
    this.selectedDemographicData.emit(this.selectedItems);
  }

  isSelected(id: number) {
    return this.selectedItems.includes(id);
  }

}
