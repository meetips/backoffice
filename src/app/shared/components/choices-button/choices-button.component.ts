import { Component, OnInit, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-choices-button',
  templateUrl: './choices-button.component.html',
  styleUrls: ['./choices-button.component.scss']
})
export class ChoicesButtonComponent implements OnInit {

  constructor(private elRef: ElementRef) {
    // console.log(elRef.nativeElement.classList.add('w-50'));

   }

  @Input()
  isTrue = false;

  @Input()
  formControl: FormControl;

  @Output()
  changed = new EventEmitter();

  @Input() onTrue: string = null;
  @Input() onFalse: string = null;
  @Input() classes: string = null;
  @Input() id = 'null';


  ngOnInit() {

  }

  change(bool) {
    this.isTrue = bool;
    this.changed.emit(bool);
  }

}
