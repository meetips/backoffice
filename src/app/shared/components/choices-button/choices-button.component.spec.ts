import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoicesButtonComponent } from './choices-button.component';

describe('ChoicesButtonComponent', () => {
  let component: ChoicesButtonComponent;
  let fixture: ComponentFixture<ChoicesButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoicesButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoicesButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
