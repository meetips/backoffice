import {
  Component,
  OnInit,
  Injectable,
  EventEmitter,
  Input,
  Output,
  OnChanges
} from '@angular/core';
import { CategoryService } from 'src/app/services/category/category.service';
import { Category, ImportCategory, Audience } from 'src/app/models/models';

// import * as importCategories from 'src/app/common/category-selector/categories';
import { isNullOrUndefined, isNumber } from 'util';
import * as _ from 'lodash';
import { AudienceService } from 'src/app/services/audience/audience.service';

@Component({
  selector: 'app-category-selector',
  templateUrl: './category-selector.component.html',
  styleUrls: ['./category-selector.component.scss']
})
export class CategorySelectorComponent implements OnInit, OnChanges {
  constructor(
    private categoryService: CategoryService,
    private audienceService: AudienceService
  ) { }

  categories: Category[];
  allCategories: Category[] = [];
  filteredCategories: Category[] = [];
  categoriesMap: { [s: number]: Category } = {};
  expandedIds: number[] = [];

  initialMapData: { [n: number]: any | Category | Audience } = {};

  initialData: any[] = [];
  initialRootElements: any[] = [];
  filteredData: any[] = [];
  filteredRootElements: any[] = [];

  filteredMapIds = {};

  @Input()
  hasError = false;

  @Input()
  selectedIds: number[] = [];

  @Input()
  showDescription: boolean = false;

  selectedCategories: Category[] = [];
  selectedAudienceInfo: Audience[] = [];
  categorySearch: string;

  @Output()
  selectedIdsChange: EventEmitter<number[]> = new EventEmitter();

  @Input()
  type: 'category' | 'audience';

  filtered: boolean = false;
  search: string = '';
  inputFocus;

  ngOnInit() {
    // this.getAllCategories();

    console.log(this.selectedIds);

    if (!this.type) {
      this.type = 'audience';
    }

    if (this.type === 'category') {
      this.getAllCategories();
    } else {
      this.getAllAudience();
    }  

    if (this.selectedIds.length > 0) {
      this.selectedIds.forEach((id: number) => {
        this.selectElement(id, true);
      });
    }
  }

  ngOnChanges() {
  }

  getAllCategories() {
    this.categoryService
      .getAllCategories()
      .subscribe((categories: Category[]) => {
        // this.allCategories = categories;
        // this.filteredCategories = categories;

        // this.categories = this.getRootParents(categories);

        categories.forEach(_item => {
          const item = Object.assign({}, _item);
          // if (this.selectedIds.)
          this.initialMapData[item.id] = item;
          if (isNullOrUndefined(item['parent'])) {
            this.filteredRootElements.push(Object.assign({}, item));
          }
          this.initialData.push(Object.assign({}, item));
        });

        this.filteredData = Object.assign([], this.initialData);
        this.initialRootElements = Object.assign([], this.filteredRootElements);
      });
  }

  getAllAudience() {
    this.audienceService.getAllAudiences().subscribe((response: Audience[]) => {
      response.forEach(item => {
        this.initialMapData[item.id] = item;
        if (isNullOrUndefined(item['parent'])) {
          this.filteredRootElements.push(Object.assign({}, item));
        }
        this.filteredData.push(Object.assign({}, item));
      });
      this.initialData = Object.assign([], this.filteredData);
      this.initialRootElements = Object.assign([], this.filteredRootElements);
    });
  }

  getRootParents(allItems: any[]) {
    return allItems.filter(value => {
      this.categoriesMap[value.id] = value;
      if (!value.parent) {
        return true;
      }
      return false;
    });
  }


  selectId(id: number, onlyAdd: boolean = false) {
    const index = this.selectedIds.indexOf(id);
    if (index <= 0) {
      this.selectedIds.push(id);
    } else if (onlyAdd === false) {
      this.selectedIds.splice(index, 1);
    }
  }

  selectElement(element: number, onlyAdd = false) {
    let index = this.selectedIds.indexOf(element);
    
    if (index < 0) {
      if (isNumber(element)) {
        this.selectedIds.push(element);
      } else {
        this.selectedIds.push((element as Category).id);
      }
      // this.checkAllChildren(category);
    } else {
      if (!onlyAdd) {
        this.selectedIds.splice(index, 1);
      }
    }
    this.selectedIdsChange.emit(this.selectedIds);
  }

  getParents(id: number, depthLevel: number = 1, currentParent: string = '') {
    // const element = this.categoriesMap[id] as Category;
    const element = this.initialMapData[id] as Category | Audience;

    if (!isNullOrUndefined(element)) {
      if (!element.parent) {
        if (depthLevel !== 1) {
          return element.name + ' > ' + currentParent;
        }
        return null;
      } else {
        if (depthLevel === 1) {
          return this.getParents(element.parent, depthLevel + 1, currentParent);
        } else {
          return this.getParents(
            element.parent,
            depthLevel + 1,
            element.name + ' > ' + currentParent
          );
        }
      }
    }
  }

  showChildrenFromInitialSelectedIds(element: Category) {
    const parent = this.initialMapData[element.id].parent;

    if (!isNullOrUndefined(parent)) {

    }
  }

  searchChange() {
    if (this.search.length >= 3) {
      this.doSearch();
    }
  }

  resetSearch() {
    // tslint:disable-next-line:forin
    for (const el in this.initialMapData) {
      this.initialMapData[el].hasMatchingChildren = false;
      this.initialMapData[el].isMatching = false;
    }
    this.filteredData = Object.assign([], this.initialData);
    this.filteredMapIds = {};
    this.search = '';
    this.filtered = false;
    this.filteredRootElements = Object.assign([], this.initialRootElements);
  }

  doSearch() {
    this.filteredData = [];
    this.filteredRootElements = [];
    const rootIds = [];

    const toMatch = _.deburr(this.search).toLowerCase();

    if (this.type === 'audience') {
      this.audienceService.filter(toMatch).subscribe((res: Audience[]) => {
        res.forEach(value => {
          const name = _.deburr(value.name).toLowerCase();
          const indexInName = name.indexOf(toMatch);
          if (indexInName >= 0) {
            this.itemMatchFilter(value, toMatch, 'name', indexInName);
          }

          const description = _.deburr(value.description).toLowerCase();
          const indexInDescription = description.indexOf(toMatch);
          if (indexInDescription >= 0) {
            this.itemMatchFilter(
              value,
              toMatch,
              'description',
              indexInDescription
            );
          }

        });
      });
    } else {
      this.categoryService.filter(toMatch).subscribe((res: Audience[]) => {
        res.forEach(value => {
          const name = _.deburr(value.name).toLowerCase();
          const indexInName = name.indexOf(toMatch);
          this.itemMatchFilter(value, toMatch, 'name', indexInName);

        });
      });
    }

    this.filtered = true;
  }

  itemMatchFilter(
    value: any,
    toMatch: string,
    matchtype: 'name' | 'description',
    matchingIndex
  ) {
    const matchingValue = { ...value, isMatching: true };
    const boldPart = (str: string): string => {
      return (
        str.substr(0, matchingIndex) +
        `<b>${str.substr(matchingIndex, toMatch.length)}</b>` +
        str.substr(matchingIndex + toMatch.length)
      );
    };
    if (matchtype === 'description') {
      matchingValue.description = boldPart(value.description);
    } else {
      matchingValue.name = boldPart(value.name);
    }
    this.filteredData.push(matchingValue);
    this.filteredMapIds[matchingValue.id] = true;
    if (isNullOrUndefined(matchingValue.parent)) {
      this.filteredRootElements.push(matchingValue);
    } else {
      const rootElement = this.setHasMatchingChildrenToRootCategory(
        matchingValue
      );
      this.filteredMapIds[rootElement.id] = true;
    }
  }

  setHasMatchingChildrenToRootCategory(element: Category | Audience) {
    if (isNullOrUndefined(element.parent)) {
      element.hasMatchingChildren = true;
      element.showChildren = true;
      if (!this.filteredMapIds.hasOwnProperty(element.id)) {
        this.filteredRootElements.push(element);
      }
      this.filteredMapIds[element.id] = true;

      return element;
    } else {
      element.hasMatchingChildren = true;
      element.showChildren = true;

      if (!this.filteredMapIds.hasOwnProperty(element.id)) {
        this.filteredData.push(element);
      }
      this.filteredMapIds[element.id] = true;

      return this.setHasMatchingChildrenToRootCategory(
        this.initialMapData[element.parent]
      );
    }
  }

  expandId(id: number) {
    const index = this.expandedIds.indexOf(id) 
    if (index > 0) {
      this.expandedIds.splice(index, 1);
    } else {
      this.expandedIds.push(id);
    }
  }
}
