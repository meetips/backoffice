import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { MatAutocomplete, MatChipInputEvent, MatAutocompleteSelectedEvent } from '@angular/material';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { startWith, map, debounceTime } from 'rxjs/operators';
import * as _ from 'lodash';
import { CountryService } from 'src/app/services/country/country.service';
import { Country, Language } from 'src/app/models/models';
import { LanguageService } from 'src/app/services/language/language.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-multiple-value-selector',
  templateUrl: './multiple-value-selector.component.html',
  styleUrls: ['./multiple-value-selector.component.scss']
})
export class MultipleValueSelectorComponent implements OnInit {

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = false;
  separatorKeysCodes: number[] = [ENTER, COMMA];

  @Input()
  matFormFieldType = "fill";

  @Input()
  selectionType: 'country' | 'language';


  selectedElements: any[] = []
  elements: any[] = [];
  filteredElements: Observable<any[]>;
  control = new FormControl();
  @Input()
  nameProperty;
  @Input()
  valueProperty;

  @Input() selectedValues: any[] = [];


  @Output()
  selectionChange: EventEmitter<any[]> = new EventEmitter();

  @ViewChild('input', { static: true }) input: ElementRef<HTMLInputElement>;
  @ViewChild('auto', { static: true }) matAutocomplete: MatAutocomplete;

  constructor(private languageService: LanguageService, private countryService: CountryService) {

    this.filteredElements = this.control.valueChanges.pipe(
      debounceTime(1000),
      startWith(null),
      map((element: string | null) => {
        console.log(element);
        if (isNullOrUndefined(element)) {
          return this.elements.slice();
        }
        return this._filter(element);
      }));
  }

  ngOnInit() {

    if (this.selectionType === 'country') {
      this.getCountries();
    } else {
      this.getLanguages();
    }

    
  }

  getInitialSelectedElement() {
    this.selectedValues.forEach(value => {
      this.elements.forEach((element) => {
        if (element[this.valueProperty] === value) {
          this.selectedElements.push(element);
        }
      });
    })
  }

  getLanguages() {
    this.languageService.getLangages().subscribe((languages: Language[]) => {
      this.elements = languages;
      this.control.setValue('');
      this.getInitialSelectedElement();
    });
  }

  getCountries() {
    this.countryService.getCountries().subscribe((countries: Country[]) => {
      this.elements = countries;
      this.control.setValue('');
      this.getInitialSelectedElement();

    });
  }

  add(event: MatChipInputEvent): void {
    // Add fruit only when MatAutocomplete is not open
    // To make sure this does not conflict with OptionSelected Event
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      // Add our fruit
      if (!this.elementsContainsItem(value)) {
        this.selectedElements.push(value.trim());
      }
      // Reset the input value
      if (input) {
        input.value = '';
      }
      this.control.setValue(null);
    }
  }

  remove(item: string): void {
    // const index = this.fruits.indexOf(fruit);
    const index = this.selectedElements.indexOf(item);

    if (index >= 0) {
      // this.fruits.splice(index, 1);
      this.selectedElements.splice(index, 1);
    }
  }

  elementsContainsItem(itemValue) {
    const include = this.selectedElements.filter((searchElement) => {
      return searchElement[this.valueProperty] === itemValue;
    });
    if (include.length === 0) {
      return true;
    }
    return false;
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    const toAdd = {};
    toAdd[this.nameProperty] = event.option.viewValue;
    toAdd[this.valueProperty] = event.option.value;
    const include = this.selectedElements.filter((searchElement) => {
      return searchElement[this.valueProperty] === event.option.value;
    });
    if (include.length === 0) {
      this.selectedElements.push(toAdd);
    }
    this.input.nativeElement.value = '';
    this.control.setValue(null);

  }

  private _filter(value: string): any[] {
    const filterValue = _.deburr(value.toLowerCase());

    const res = this.elements.filter(element => {
      return _.deburr(element[this.nameProperty].toLowerCase()).indexOf(filterValue) >= 0
    });
    return res;
  }

}
