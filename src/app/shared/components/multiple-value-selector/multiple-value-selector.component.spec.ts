import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MultipleValueSelectorComponent } from './multiple-value-selector.component';


describe('MultipleValueSelectorComponent', () => {
  let component: MultipleValueSelectorComponent;
  let fixture: ComponentFixture<MultipleValueSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultipleValueSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleValueSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
