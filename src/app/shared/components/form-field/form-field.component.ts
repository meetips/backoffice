import { Component, OnInit, Input, OnChanges, Output, EventEmitter, TemplateRef } from '@angular/core';
// import { MatFormFieldBase } from '@angular/material';
import { AbstractControl, FormGroup } from '@angular/forms';
import { InputComponentParameter } from '../../models/input.component-parameter';
import { FileUploadComponentParameter } from '../../models/file-upload.component-parameter';
import { StringInputComponentParameter } from '../../models/string-input.component-parameter';
import { FieldComponentType } from '../../models/fields-component-type';
import { DataType } from '../../models/data-type';
import { ChoicesButtonComponentParameter } from '../../models/choices-button.component-parameter';
import { MultipleValueSelectorComponentParameter } from '../../models/multiple-value-selector.component-parameter';

@Component({
  selector: 'app-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.scss']
})
export class FormFieldComponent implements OnInit, OnChanges {

  constructor() { }

  @Input()
  formControlName?: string;

  @Input()
  formControlField?: AbstractControl;

  @Input()
  dataType?: DataType;

  @Input()
  label?: string;

  @Input()
  placeholder?: string;

  @Input()
  classes?: string = ""

  @Input()
  matFormFieldType: string = 'fill';

  @Input()
  hint?: any | string | { start: string, end: string };

  @Input()
  errors?: any;

  @Input()
  content?: TemplateRef<any>;

  @Input()
  fieldComponentType?: any;

  @Input()
  dateParams?: any;

  @Input()
  choicesButtonsParams: ChoicesButtonComponentParameter;

  @Input()
  stringInputComponentParameter?: StringInputComponentParameter;

  @Input()
  inputParams?: InputComponentParameter;

  @Input()
  fileUploadParams?: FileUploadComponentParameter;

  @Input()
  multipleValueParams?: MultipleValueSelectorComponentParameter;

  @Output()
  changed: EventEmitter<any> = new EventEmitter();

  @Input()
  submitted = false;

  FieldComponentType = FieldComponentType;
  DataType = DataType;

  ngOnInit() {
  }

  ngOnChanges() {

  }

  hasError(error) {
    return this.formControlField.invalid && (
                        this.formControlField.dirty ||
                        this.formControlField.touched ||
                        this.submitted
                      ) && 
                      this.formControlField.hasError(error);
  }

  inputChange($event?) {
    // console.log(this.formControlField.value);
    this.changed.emit(this.formControlField.value);
  }
}
