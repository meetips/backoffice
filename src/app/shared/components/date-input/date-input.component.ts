import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import * as moment from 'moment';
import { FormControl, FormGroup } from '@angular/forms';
import { isNullOrUndefined } from 'util';
import { range } from 'rxjs';

@Component({
  selector: 'app-date-input',
  templateUrl: './date-input.component.html',
  styleUrls: ['./date-input.component.scss']
})
export class DateInputComponent implements OnInit {

  constructor() { }

  @Input() 
  matFormFieldType = "fill";
  
  @Input()
  date: FormGroup;

  @Input()
  format: "fr" | "us";

  @Input()
  dateFormGroup: FormGroup = new FormGroup({});

  // @Input()
  // day: FormControl = new FormControl();
  // @Input()
  // month: FormControl = new FormControl();
  // @Input()
  // year: FormControl = new FormControl();

  @Input()
  minYearFromNow: number;
  @Input()
  maxYearFromNow: number;

  dateChange: EventEmitter<string> = new EventEmitter();

  months: any[] = [];
  days: any[] = [];
  years: any[] = [];

  get day() {
    return this.dateFormGroup.get("day");
  }

  get month() {
    return this.dateFormGroup.get("month");
  }

  get year() {
    return this.dateFormGroup.get("year");
  }


  ngOnInit() {
    moment.locale("fr");
    this.months = this.getMonths();
    this.days = this.getDays();
    this.years = this.getYears();

    this.month.valueChanges.subscribe(value => {
      this.days = this.getDays();
      this.dateChanged();
    });

    this.year.valueChanges.subscribe(value => {
      this.days = this.getDays();
      this.dateChanged();
    });

    this.day.valueChanges.subscribe(value => {
      this.dateChanged();
    });

  }

  getMonths() {
    return moment.months("DDD").map((month, index) => { return { label: month, value: index + 1 }; })
  }

  getDays() {
    console.log(moment.months("DDD"));
    const res = [];
    let dayCount = 31
    if (!isNullOrUndefined(this.month.value) && !isNullOrUndefined(this.year.value)) {
      dayCount = moment(`${this.year.value}-${this.month.value}`, "YYYY-MM").daysInMonth();
    }
    for (let i = 1; i <= dayCount; i++) {
      res.push(i);
    }
    return res;
  }

  getYears() {
    const res = [];
    const minYear = moment().get("year") - this.minYearFromNow;
    const maxYear = moment().get("year") - this.maxYearFromNow;
    for (let i = maxYear; i >= minYear; i--) {
      res.push(i);
    }
    return res;
  }

  dateChanged() {
    if (!isNullOrUndefined(this.day.value) &&
      !isNullOrUndefined(this.month.value) &&
      !isNullOrUndefined(this.year.value)) {
        this.dateChange.emit(`${this.day.value}/${this.month.value}/${this.year.value}`);
      }
  }

}
