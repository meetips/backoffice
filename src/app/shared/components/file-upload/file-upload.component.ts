import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  ViewRef,
  ViewChild,
  ElementRef,
  TemplateRef,
  ContentChild,
  OnChanges,
  AfterViewInit,
  AfterContentInit,
  AfterViewChecked,
  ViewChildren,
  Directive,
  QueryList
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { isNullOrUndefined } from 'util';
import { AbstractControl, FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { FileInfo } from '../../models/file-info.model';
import { FileUpload } from '../../models/file-upload.model';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit, OnChanges {
  constructor(private sanitizer: DomSanitizer, private formBuilder: FormBuilder) { }

  @Input()
  fileFormGroup: FormGroup;

  @Output()
  fileUploadChange: EventEmitter<FileUpload> = new EventEmitter();

  @Output()
  fileInfoChange: EventEmitter<FileInfo> = new EventEmitter();

  @Input()
  fileType: 'image' | 'video';

  @Input()
  filePreviewSrc: any;

  filePreviewSrc_: any;

  @Input()
  ratioRange: number[] = null;

  @Input()
  submitted = false;

  @Input()
  acceptedImageFormat = ['image/jpeg', 'image/png', 'image/jpg', 'image/gif', '*/jpeg'];

  @Input()
  acceptedVideoFormat = [
    'video/mpeg',
    'video/ogg',
    'video/webm',
    'video/x-flv',
    'video/mp4'
  ];

  canUpload = true;
  fileName: string;
  fileInfo: FileInfo = new FileInfo();
  currentFile: File;

  @ViewChild('fileInput', { static: true }) fileInput: ElementRef<HTMLInputElement>;
  @ViewChild('imageMedia', { static: false }) imageMedia: ElementRef<HTMLImageElement>;
  @ViewChild('videoMedia', { static: false }) videoMedia: ElementRef<HTMLVideoElement> = { nativeElement: null };

  ngOnInit() {
    if (!isNullOrUndefined(this.filePreviewSrc)) {
      this.filePreviewSrc_ = this.filePreviewSrc;
    }
  }

  ngOnChanges(): void {
    if (!isNullOrUndefined(this.filePreviewSrc)) {
      this.filePreviewSrc_ = this.filePreviewSrc;
    }
  }

  loadedMetatData(event) {
    this.fileInfo.fileType = this.fileType;
    if (this.fileType === "video") {
      this.fileInfo.duration = event.target.duration;
      this.fileInfo.height = event.target.videoHeight;
      this.fileInfo.width = event.target.videoWidth;
    } else {
      this.fileInfo.height = event.target.naturalHeight;
      this.fileInfo.width = event.target.naturalWidth;
    }
    this.fileInfoChange.emit(this.fileInfo);
  }

  drop(ev) {
    ev.preventDefault();
    if (ev.dataTransfer.files.length === 1) {
      const file = ev.dataTransfer.files[0] as File;

      this.fileInfo.size = file.size;
      this.fileInfo.fileExtension = file.type;

      const reader = new FileReader();
      reader.onload = e => this.filePreviewSrc_ = reader.result;
      reader.readAsDataURL(file);

      this.fileUploadChange.emit({
        id: null,
        file,
        name: file.name,
        localFile: true
      });
      this.canUpload = false;
    }
  }

  dragOverHandler(ev) {
    ev.preventDefault();
  }

  deleteFile(event) {
    event.stopPropagation();
    event.preventDefault();
    window.URL.revokeObjectURL(this.filePreviewSrc_);
    this.filePreviewSrc_ = null;
    this.fileInput.nativeElement.value = '';

    this.fileFormGroup.reset();
    // this.fileUploadChange.emit(this.fileFormGroup);

    this.canUpload = true;
  }

  fileInputUpload(event) {
    if (event.target.files.length === 1) {
      const file = event.target.files[0] as File;

      this.fileInfo.size = file.size;
      this.fileInfo.fileExtension = file.type;

      const reader = new FileReader();
      reader.onload = e => {
        this.filePreviewSrc_ = reader.result;
      }
      reader.readAsDataURL(file);

      this.currentFile = file;

      this.fileUploadChange.emit({
        id: null,
        file,
        name: file.name,
        localFile: true
      });

      this.canUpload = false;

    }
  }

  fileInputClick() {
    if (this.canUpload) {
      this.fileInput.nativeElement.click();
    }
  }

  hasErrors() {
    return this.fileFormGroup.get('file').invalid && this.submitted &&
      (this.fileFormGroup.get('file').dirty || this.fileFormGroup.get('file').touched)
  }
}
