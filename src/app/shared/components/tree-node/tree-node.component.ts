import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Category, Audience } from 'src/app/models/models';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-tree-node',
  templateUrl: './tree-node.component.html',
  styleUrls: ['./tree-node.component.scss']
})
export class TreeNodeComponent implements OnInit {

  @Input() style?: string;
  @Input() children?: Category[]|Audience[];
  @Input() element?: Category|Audience;
  @Input() selectedIds: number[] = [];
  @Input() expandedIds: number[] = [];
  @Input() childLoading = false;

  @Input() flatArray: any[];
  @Input() filteredData: any[];
  @Input() filtered = false;

  @Output('expandId') expandId: EventEmitter<Category|Audience> = new EventEmitter();
  @Output('selectId') selectId: EventEmitter<number> = new EventEmitter();
  @Output('selectElement') selectElement: EventEmitter<Category|Audience> = new EventEmitter();


  constructor() { }

  ngOnInit() {
  }

  makeParentSelection() {
  }

  idInList(list: any[], id: number) {
    if (!!list) {
      return list.indexOf(id) >= 0;
    }
    return null;
  }


  ngOnChanges() {
    if (this.element.showChildren) {
      this.getChildren();
      this.showChildren(true);
    }
  }


  showChildren(value?) {
    this.getChildren();
    if (!!value) {
      this.element.showChildren = value;
    } else {
      this.element.showChildren = !this.element.showChildren;
    }
  }

  showElement() {
    if (this.filtered === true && (this.element.hasMatchingChildren === true || this.element.isMatching === true)) {
      return true;
    }
    if (this.filtered === false) {
      return true;
    }
    return false;
  }

  getChildren() {
    
    if (isNullOrUndefined(this.element.children) || this.element.children.length === 0 ) {
      this.element.children = this.filteredData.filter(value => {
        if (value.parent === this.element.id) {
          return true ;
        }
        return false;
      });
    }
    return this.element.children;
  }




}
