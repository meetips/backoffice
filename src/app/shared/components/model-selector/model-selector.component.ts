import { Component, OnInit, Output, EventEmitter, Input, TemplateRef, ElementRef } from '@angular/core';

@Component({
  selector: 'app-model-selector',
  templateUrl: './model-selector.component.html',
  styleUrls: ['./model-selector.component.css']
})
export class ModelSelectorComponent implements OnInit {

  constructor() { }

  @Output()
  change = new EventEmitter<any>();

  @Input()
  selected = [];

  @Input()
  content: TemplateRef<ElementRef>;

  ngOnInit() {
  }

}
