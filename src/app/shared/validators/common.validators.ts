import { AbstractControl } from '@angular/forms';
import { isNullOrUndefined } from 'util';
import { Observable } from 'rxjs';

export class CommonValidators {

  static passwordRegex: RegExp = /^(?=.*[a-zA-Z])(?=.*\d).{6,20}$/;

  /**
   * @param conditionnalField 
   * errors for validator: required
   */
  static conditionnalRequired(conditionnalField: string) {
    return (control: AbstractControl) => {
      if (!!control && control.parent) {
        const required: boolean = control.parent.get(conditionnalField).value;
        if (required && (isNullOrUndefined(control.value) || control.value === '')
        ) {
          return {
            required: true
          };
        }
      }
      return null;
    };
  }


  /**
   * @param minElementCount 
   * errors for validator: minListLength
   */
  static minListLength(minElementCount: number = 1) {
    return (control: AbstractControl) => {
      if (control.value.length < minElementCount) {
        return { minListLength: true };
      }
      return null;
    }
  }


  /**
   * 
   * @param sameToControlName name of the control which the password have to be identique to
   * @returns passwordConfirm error when values not corresponding
   */
  static samePasswordValidator(sameToControlName: string) {
    return (control: AbstractControl) => {
      if (!!control && !!control.parent) {
        const confirm = control.parent.get(sameToControlName).value;
        if (control.value !== confirm) {
          return {
            passwordConfirm: true
          };
        }
      }
      return null;
    };
  }


  /**
 * @param conditionnalField 
 * errors for validator: requiredIfNull
 */
  static requiredIfNull(nullFormControl) {
    return (control: AbstractControl) => {
      if (!!control && !!control.parent) {
        const value = control.parent.get(nullFormControl).value;
        if (!value) {
          return {
            requiredIfNull: true
          };
        }

      }
      return null;
    };
  }

  /**
   * 
   * @param names a list of forbidden strings
   * error: forbiddenName
   */
  static forbiddenNames(names: string[]) {
    return (control: AbstractControl) => {
      if (names.indexOf(control.value) >= 0) {
        return {
          forbiddenName: true
        }
      }
      return null;
    }
  }


  static uniqueName(functionToGetListOfForbbidenNames: Observable<any>) {
    return (control: AbstractControl) => {
      return new Promise(async (res, rej) => {
        
        if (!!control.value && control.value !== "" && (control.dirty || control.touched)) {
          const namesList: string[] = await functionToGetListOfForbbidenNames.toPromise();
          if (namesList.indexOf(control.value) >= 0) {
            res({
              uniqueName: true
            });
          }
        }
        res(null);
      });
    }
  }

  static passwordValidator(control: AbstractControl) {
    return !CommonValidators.passwordRegex.test(control.value) ? { pattern: true}: null;
  }


}
