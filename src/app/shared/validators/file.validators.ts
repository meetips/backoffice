import { AbstractControl } from "@angular/forms";
import { Observable } from "rxjs";
import { isNullOrUndefined } from "util";


export class FileValidator {

    static acceptedImageFormat = ['image/jpeg', 'image/png', 'image/jpg', 'image/gif'];

    static acceptedVideoFormat = [
        'video/mpeg',
        'video/ogg',
        'video/webm',
        'video/x-flv',
        'video/mp4'
    ];

    static maxSizeValidator(maxSize: number) {
        return (control: AbstractControl) => {
            const file = control.value as File;
            if (file.size > maxSize) {
                return { maxSize: true }
            }
            return null;
        };
    }

    /**
     * 
     * @param fileType "image" | "video"
     * @param acceptedFormat default are    
     *  acceptedVideoFormat = [
        'video/mpeg',
        'video/ogg',
        'video/webm',
        'video/x-flv',
        'video/mp4'
    ];
    and     acceptedImageFormat = ['image/jpeg', 'image/png', 'image/jpg', 'image/gif'];

     */
    static fileFormatValidator(fileType: "image" | "video", acceptedFormat = []) {
        return (control: AbstractControl) => {
            if (!isNullOrUndefined(control.value) && control.value !== "") {
                const file = control.value as File;
                if (!acceptedFormat && fileType === 'image' && !this.acceptedImageFormat.includes(file.type)) {
                    return {
                        fileFormat: true
                    };
                } else if (!acceptedFormat && fileType === 'video' && !this.acceptedVideoFormat.includes(file.type)) {
                    return {
                        fileFormat: true
                    };
                } else {
                    const fileFormat = file.name.split(".")[file.name.split(".").length - 1];
                    if (acceptedFormat.includes(fileFormat)) {
                        return {
                            fileFormat: true
                        };
                    }
                }
            }

            return null;
        };
    };

    ratioValidator(ratioRange: number[], nativeElement, fileType) {
        return (control: AbstractControl) => {
          return new Promise((resolve, rej) => {
            const min = ratioRange.sort((a, b) => (a > b ? b : a));
            const max = ratioRange.sort((a, b) => (a < b ? b : a));
            let ratio;
            let res = true;
            setTimeout(() => {
              if (!!control) {
                if (fileType === 'image') {
                  ratio =
                    nativeElement.naturalWidth /
                    nativeElement.naturalHeight;
                } else {
                  ratio =
                    nativeElement.videoWidth /
                    nativeElement.videoHeight;
                }
                if (
                  (ratio > min && ratio < max) ||
                  ratio === min ||
                  ratio === max
                ) {
                  res = false;
                }
                resolve({ ratio: res });
              }
            }, 1000);
          });
        };
      }
}
