import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nullToZero'
})
export class NullToZeroPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (!!value && value !== '') {
      return value;
    }
    return 0;
  }

}
