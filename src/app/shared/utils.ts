import { isNullOrUndefined, isArray, isObject, isString } from "util";
import { AbstractControl, FormGroup } from "@angular/forms";

export function hasError(formControl: AbstractControl, error) {
  if (!isNullOrUndefined(formControl)) {
    return formControl.errors[error];
  }
}

export function markControlsDirty(formGroup: FormGroup): void {
  for (let controlName in formGroup.controls) {
    const control = formGroup.get(controlName);
    if (control instanceof FormGroup) {
      markControlsDirty(control);
    } else {
      control.markAsDirty();
      control.markAsTouched();
      control.updateValueAndValidity();
    }
  }
  formGroup.updateValueAndValidity();
}

export function getFormData(toSend: any, pathToValue: string = ""): FormData {
  const formData = new FormData();
  for (const property of Object.keys(toSend)) {
    const value = toSend[property];

    if (!isNullOrUndefined(value)) {

      if (isArray(value)) {

        value.forEach((element, index) => {

          if (isObject(element) && Object.keys(element).length > 0 && !(element instanceof File)) {
            const fd = getFormData(element, pathToValue + property + "." + index);

            (fd as any).forEach((value, key) => {
              formData.append(key, value);
            });
            // formData.append(pathToValue + property + "." + index, element);
          }
          else {
            formData.append(pathToValue + property, element);
          }
        });

      } else if (isObject(value) && Object.keys(value).length > 0 && !(value instanceof File)) {

        const fd = getFormData(value, pathToValue + property + ".");

        (fd as any).forEach((value, key) => {
          formData.append(key, value);
        });
      } else {

        formData.append(pathToValue + property, value);

      }
    }
  }
  return formData;
}