import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategorySelectorComponent } from './components/category-selector/category-selector.component';
import { ChoicesButtonComponent } from './components/choices-button/choices-button.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { DemographicDataComponent } from './components/demographic-data/demographic-data.component';
import { ModalDialogComponent } from './components/modal-dialog/modal-dialog.component';
import { ModalRewardComponent } from '../modal-reward/modal-reward.component';
import { TreeNodeComponent } from './components/tree-node/tree-node.component';
import { MultipleValueSelectorComponent } from './components/multiple-value-selector/multiple-value-selector.component';
import { NullToZeroPipe } from './pipes/null-to-zero.pipe';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatDialogModule,
  MatTooltipModule,
  MatChipsModule,
  MatInputModule,
  MatMenuModule,
  MatCardModule,
  MatSelectModule,
  MatSlideToggleModule,
  MatExpansionModule,
  MatRadioModule,
  MatProgressSpinnerModule,
  MatFormFieldModule,
  MatListModule,
  MatTableModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatAutocompleteModule,
  MatStepperModule,
  MatTreeModule,
  MatCheckboxModule,
  MatProgressBarModule,
  MatPaginatorModule,
  GestureConfig,
  MatSliderModule
} from '@angular/material';
import { AuthService } from '../services/auth/auth.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { TokenInterceptor } from '../services/auth/token.interceptor';
import { HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DateInputComponent } from './components/date-input/date-input.component';
import { CustomPercentPipe } from './pipes/custom-percent.pipe';
import { CustomPricePipe } from './pipes/custom-price.pipe';
import { CustomCurrencyPipe } from './pipes/custom-currency.pipe';
import { FormFieldComponent } from './components/form-field/form-field.component';
import { ModelSelectorComponent } from './components/model-selector/model-selector.component';
import { PageContainerComponent } from './components/page-container/page-container.component';
import { LoadingSpinnerComponent } from './components/loading-spinner/loading-spinner.component';
import { AlertComponent } from './components/alert/alert.component';

const materialElements: any[] = [
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatDialogModule,
    MatTooltipModule,
    MatChipsModule,
    MatInputModule,
    MatMenuModule,
    MatCardModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatExpansionModule,
    MatRadioModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatListModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatAutocompleteModule,
    MatStepperModule,
    MatTreeModule,
    MatCheckboxModule,
    MatProgressBarModule,
    MatPaginatorModule,
    MatSlideToggleModule,
];

@NgModule({
  imports: [
    CommonModule,
    ...materialElements,
    FormsModule,
    HttpClientModule,
    MatSliderModule,
    ReactiveFormsModule,
    RouterModule
  ],
  declarations: [
    CategorySelectorComponent,
    ChoicesButtonComponent,
    FileUploadComponent,
    DemographicDataComponent,
    ModalDialogComponent,
    ModalRewardComponent,
    TreeNodeComponent,
    MultipleValueSelectorComponent,
    NullToZeroPipe,
    ConfirmationDialogComponent,
    DateInputComponent,
    CustomPercentPipe,
    CustomPricePipe,
    CustomCurrencyPipe,
    FormFieldComponent,
    ModelSelectorComponent,
    PageContainerComponent,
    LoadingSpinnerComponent,
    AlertComponent,
  ],
  exports: [
    CategorySelectorComponent,
    ChoicesButtonComponent,
    FileUploadComponent,
    DemographicDataComponent,
    ModalDialogComponent,
    ModalRewardComponent,
    TreeNodeComponent,
    MultipleValueSelectorComponent,
    ConfirmationDialogComponent,
    ...materialElements,
    // BrowserAnimationsModule,
    FormsModule,
    MatSliderModule,
    ReactiveFormsModule,
    NullToZeroPipe,
    DateInputComponent,
    FormFieldComponent,
    PageContainerComponent,
    LoadingSpinnerComponent,


  ],
  entryComponents: [ConfirmationDialogComponent],
  providers: [
    // AuthService,
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: TokenInterceptor,
    //   multi: true
    // },
    // {
    //   provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig
    // },
    // {provide: MAT_DATE_LOCALE, useValue: 'fr-FR'},
  ],
})
export class SharedModule { }
