import { Component, OnInit } from '@angular/core';
import { Campaign } from '../models/models';
import { CampaignService } from '../services/campaign/campaign.service';

@Component({
  selector: 'app-modal-reward',
  templateUrl: './modal-reward.component.html',
  styleUrls: ['./modal-reward.component.css']
})
export class ModalRewardComponent implements OnInit {

  constructor(private campaignService: CampaignService) { }

  campaign: Campaign;

  ngOnInit() {
    this.campaign =  this.campaignService.modalCampaign;
  }

}
