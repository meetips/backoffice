import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalRewardComponent } from './modal-reward.component';

describe('ModalRewardComponent', () => {
  let component: ModalRewardComponent;
  let fixture: ComponentFixture<ModalRewardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalRewardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRewardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
